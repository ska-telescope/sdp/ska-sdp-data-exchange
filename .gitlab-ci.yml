# GitLab CI in conjunction with GitLab Runner can use Docker Engine to test and build any application.
# Docker, when used with GitLab CI, runs each job in a separate and isolated container using the predefined image that is set up in .gitlab-ci.yml.
# In this case we use the latest python docker image to build and test this project.
# We store the artefact.skao.int/ska-k8s-tools/docker-builder:x.x.x as a group level variable for convenience
image: $SKA_K8S_TOOLS_BUILD_DEPLOY

# cache is used to specify a list of files and directories which should be cached between jobs. You can only use paths that are within the project workspace.
# If cache is defined outside the scope of jobs, it means it is set globally and all jobs will use that definition
# cache:
#   paths:
#     - build

# The YAML file defines a set of jobs with constraints stating when they should be run.
# You can specify an unlimited number of jobs which are defined as top-level elements with an arbitrary name and always have to contain at least the script clause.
# In this case we have only the test job which produce an artifacts (it must be placed into a directory called "public")
# It is also specified that only the master branch will be subject of this job.


stages:
  - build
  - test
  - lint
  - pages
  - publish
  - scan
  - release

# Standardised included jobs
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  CI_POETRY_VERSION: 1.8.2

# Include CI templates
include:
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/rules.gitlab-ci.yml'
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/oci-image.gitlab-ci.yml'
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/python.gitlab-ci.yml'
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/docs.gitlab-ci.yml'
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/finaliser.gitlab-ci.yml'
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/release.gitlab-ci.yml'

python-test:
  services:
    # Keep in sync with tests/services.docker-compose.yml
    - name: quay.io/coreos/etcd:v3.5.9
      alias: etcd
      command:
        - /usr/local/bin/etcd
        - "--advertise-client-urls=http://0.0.0.0:2379"
        - "--listen-client-urls=http://0.0.0.0:2379"
        - "--initial-advertise-peer-urls=http://0.0.0.0:2380"
        - "--listen-peer-urls=http://0.0.0.0:2380"
        - "--initial-cluster=default=http://0.0.0.0:2380"
    - name: bitnami/kafka:3.5.0-debian-11-r1
      alias: kafka
      variables:
        ALLOW_PLAINTEXT_LISTENER: "yes"
        KAFKA_CFG_ADVERTISED_LISTENERS: PLAINTEXT://localhost:9092
        KAFKA_CFG_CONTROLLER_QUORUM_VOTERS: 0@kafka:9093
        KAFKA_CFG_CONTROLLER_LISTENER_NAMES: CONTROLLER
        KAFKA_CFG_LISTENERS: PLAINTEXT://:9092,CONTROLLER://:9093
        KAFKA_CFG_LISTENERS_SECURITY_PROTOCOL_MAP: CONTROLLER:PLAINTEXT,PLAINTEXT:PLAINTEXT
        KAFKA_CFG_NODE_ID: 0
        KAFKA_CFG_PROCESS_ROLES: controller,broker
        KAFKA_CFG_AUTO_CREATE_TOPICS_ENABLE: "true"
        KAFKA_CFG_MESSAGE_MAX_BYTES: 134217728
  variables:
    SDP_TEST_HOST: etcd
    KAFKA_CFG_MESSAGE_MAX_BYTES: 134217728
    # Needed to allow kafka & zookeeper to talk to each other
    # See https://docs.gitlab.com/runner/configuration/feature-flags.html
    FF_NETWORK_PER_BUILD: "true"
    # To debug the above services, uncomment the below line. For more info see
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/2119#note_1142144587
    # CI_DEBUG_SERVICES: trace

# Run both the html and doctest builders in the docs-build job
# to ensure code examples are correct
docs-build:
  variables:
    CI_SPHINX_BUILDERS: html
