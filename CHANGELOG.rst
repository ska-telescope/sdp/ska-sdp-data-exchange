###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

[Development]
*************

[5.3.1]
*******

Fixed
-----

* Fixed unexpected `ConsumerStoppedError` when using `DataQueue` flows caused by stopping the source while stream iteration is suspended.

[5.3.0]
*******

Added
-----

* Added DataQueue source and sink implementation and connected to DataQueue flows.

Fixed
-----

* Close ``Config`` object at device shutdown.

Changed
-------

* Updated documentation theme.


[5.2.1]
*******

Fixed
-----

* Fixed device fault when invalid Tango atrribute URL is detected in flows.

Changed
-------

* Updated dependency on `ska-sdp-config` to ``^0.10.3``.

[5.2.0]
*******

Added
-----

* Added ``subarray_id`` Tango property for filtering configurations.
* Added support for unknown datamodels by defaulting to `object[]` model.

[5.1.1]
*******

Fixed
-----

* Fixed support for flow sources with `None` function value.
* Fixed ``StateModel`` compliance with other SDP components.

[5.1.0]
*******

Added
-----

* Added support for ``TangoAttributeMap`` in flow configs.

Changed
-------

* Updated dependency on `ska-sdp-config` to ``^0.9.1``.

[5.0.1]
*******

Changed
-------

* Updated dependency on `ska-sdp-config` to ``0.9.0``.

[5.0.0]
*******

Added
-----

* Added support for update LMC dish pointing layout using arbitrarily sized floating point array.
* Added experimental support for flows entities from the configuration database via the `experimental_flow` device property.
* Added thread-safe async stream utilities.
* Enabled more warnings as errors in tests.

Changed
-------

* **BREAKING** Changed default config database entity to `Flow` via default `experimental_flow=True`.
* **BREAKING** Renamed ``enconding`` to ``format`` in descriptors refering to entire serialized file formats.
* **BREAKING** Renamed config watchers.
* **BREAKING** Renamed ``Exchange.stream`` to ``Exchange.run``.
* Updated dependency on `ska-sdp-config` to ``^0.8.0``.
* Updated dependency on `pydantic` to ``^2.6.4``.
* Updated dependency on `pytest` to ``^8.1.0``.

Deprecated
----------

* Deprecated `create_sdp_config_watcher` in favor of `create_descriptor_watcher`.

Removed
-------

* **BREAKING** Removed support for pushing state to config paths at `"/component/lmc-queueconnector"``

[4.1.0]
*******

Added
-----

* Added optional ``antenna_name`` to ``TangoPointingSubscriptionSource``.
* Added support for ``NaN`` and ``Infinite`` floating point literal in JSON descriptors.

Changed
-------

* Device now transitions a new group to ``FAULT`` state on a bad configuration in the database. Resetting will occur when the bad configuration is changed.
* Updated to pydantic ``^2.6.2``.

Fixed
-----

* Restored automatic fault recovery behaviour. Clearing the database configuration for a faulted processing block will now remove the exchange group from the device and free up tango attribute names.
* Fixed attribute dimension order of 2D attributes in ``TangoObjectScatterAttributeSink``.

[4.0.4]
*******

Fixed
-----

* Fixed dimensionality of 2D attributes

[4.0.3]
*******

Fixed
-----

* ``TangoArrayScatterSink`` now correctly creating value attributes.

[4.0.2]
*******

Fixed
-----

* Device now automatically leaves INIT state when monitoring is configured.

[4.0.1]
*******

Fixed
-----

* Python logging setup now correctly follows the ``-v`` command line option.

[4.0.0]
*******

Changed
-------

* **BREAKING** Tango attributes representing exchange sinks are always read-only. The ``access`` option that allowed them to become read-write has been removed.
* ``TangoObjectScatterAttributeSink`` can now process arrays.

Removed
-------

* **BREAKING** Removed all device commands
* **BREAKING** Removed `exchanges_json` tango property
* **BREAKING** Removed ``poll_period``, ``abs_change`` and ``rel_change`` parameters on ``TangoLocalAttributeSink`` as polled Tango attributes are unable to be removed from the device.

[3.0.1]
*******

Changed
-------

* Changed configuration database state format to JSON dictionary
* Changed configuration database fault format to JSON dictionary

[3.0.0] [YANKED]
****************

Added
-----

* Added exchange groups for supporting multiple configs
* Added configuration database watching from multiple keys
* Added separate state machine per exchange group
* Added forwarding of exchange group state and exceptions to configuration database

Changed
-------

* **BREAKING** Changed `Configure()` to no longer accept a SDP configuration path. Use `Monitor()` instead.
* **BREAKING** Changed config monitoring to monitor only direct child paths of `exchanges_config_path`.

[2.0.0]
*******

Added
-----

* Added `TimestampOptions` to `KafkaProducerSink`
* Added `python` encoding option
* Added `DataPipe` interface with `DefaultPipe` and `BufferWithTimePipe`
* Added `msgpack_numpy` to the list of available kakfa encodings
* Added Getting Started section to online documentation

Changed
-------

* **BREAKING** Changed `TangoJsonScatterAttributeSink` to `TangoObjectScatterAttributeSink`
* **BREAKING** Changed to `python` as default encoding for `KafkaProducerSink` and `KafkaConsumerSource`
* Updated ``ska-sdp-config`` dependency from ``0.5.1`` to ``0.5.2`` for improved etcd performance. This removes the dependency on `git`.

Removed
-------

* **BREAKING** Removed `"bytearray"` as an alias for `"bytes"` dtype
* **BREAKING** Removed `json` as a supported dtype. Use `object` with encoding `json`
* **BREAKING** Removed `"dict"` and `"list"` as supported dtypes. Use `object` instead.

[1.2.0]
*******

Added
-----

* Added `json` encoding type

Changed
-------

* Updated ``ska-sdp-config`` dependency to ``0.5.1`` for improved etcd performance. This raises the minimum support version of etcd from ``3.3`` to ``3.4``.

Fixed
-----

* Fixed `TangoJsonScatterAttributeSink` default filter behaviour

[1.1.1]
*******

Fixed
-----

* Fixed default string values on `TangoJsonScatterAttributeSink`

[1.1.0]
*******

Added
-----

* Add limited support for numpy structured arrays when using InMemory and Kafka sources and sinks by specifying the structured dtype in the ``exchanges[].dtype`` field
* Added JSON/Dictionary types to exchanges
* Added TangoJsonScatterAttributeSink
* Added changelog to sphinx docs

Deprecated
----------

* Deprecated `TangoSplitAttributeSink` in favor of `TangoArrayScatterAttributeSink`

Removed
-------

* Removed ska-tango-base dependency

[1.0.0]
*******

Added
-----

* Added TangoSplitAttributeSink
* Added etcd3 path monitoring
* Added IsMonitoringDB command
* Added Tango command docstrings
* Added thread-safe SubscribeEventConditionContext

Changed
-------

* Set correct server logging format
* Set default server logging level to INFO
* Updated Configure command behaviour
* Renamed "ExchangeDescriptorList" to "QueueConnectorDescriptor"

Fixed
-----

* Bugfix for docs not displaying correctly

[0.2.2]
*******

Added
-----

* Support manual pushing of change events when polling not defined

Fixed
-----

* Kafka consumer source performance improvements

[0.2.1]
*******

Fixed
-----

* Entrypoint module path bugfix
* Only log warning if a kafka sink is written to after being stopped

[0.2.0]
*******

Added
------

* Extended documentation around configuration and data types

Changed
-------

* Renamed project from ska-sdp-data-exchange to ska-sdp-lmc-queue-connector
* Renamed device class from DynamicExchangeDevice to SDPQueueConnector
* Renamed entrypoint from SDPExchange to SDPQueueConnector

Removed
-------

* Removed "Data" and "Descriptor" from config type descriminators
* Removed "Data" from long class names

Fixed
-----

* Bugfix for when stopping a kafka sink whilst it is sending

[0.1.0]
*******

Added
-----

* string, bytes, primitive and nparray data type support
* Tango subscription source and tango attribute sink
* Kafka Consumer source and Kafka Producer sink
* In-memory source and sink
* Dynamic Exchange Device with configuration via a Tango property or using the ``Configure()`` command providing JSON or a SDP Config path
* Empty Python project directory structure
