# Include base Makefile functionality
include .make/base.mk
include .make/python.mk
include .make/oci.mk

DOCKER_COMPOSE = docker compose
DOCS_SPHINXOPTS = -W --keep-going
PYTHON_LINE_LENGTH = 99

# GitlabCI services used in CI, docker compose for local testing only
ifndef GITLAB_CI
SERVICES_UP=docker-compose-up
SERVICES_DOWN=docker-compose-down
endif

python-pre-test: ${SERVICES_UP}

python-post-test: ${SERVICES_DOWN}

# rebuild generated api docs
docs-pre-build:
	rm -rf docs/build/doctrees/*.doctree

docs-serve:
	sphinx-autobuild docs/src docs/build/

docker-compose-up:
	$(DOCKER_COMPOSE) --file tests/services.docker-compose.yml up --detach --wait

docker-compose-down:
	$(DOCKER_COMPOSE) --file tests/services.docker-compose.yml down
