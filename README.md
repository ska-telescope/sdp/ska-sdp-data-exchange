# Developer guide

## Get Started

### Install dependencies

You will need:
* [Python](https://www.python.org/downloads/) >=3.10
* [Poetry](https://python-poetry.org/docs/) >=1.2.2
* [Docker](https://docs.docker.com/get-docker/)

Before running `poetry install` to install the Python
dependencies you will need a system tango library installed
on your system (which is required by `pytango`).

For Debian/Ubuntu:
```
$ sudo apt update
$ sudo apt install -y curl git build-essential libboost-python-dev libtango-dev
```

Please note that:
* The `libtango-dev` will install an old version of
    the TANGO-controls framework (9.2.5);
* The best way to get the latest version of the framework
    is compiling it (instructions can be found
    [here](https://gitlab.com/tango-controls/cppTango/-/blob/main/INSTALL.md))
* MacOS is not supported
* Windows users will need to use WSL
* The above script has been tested with Ubuntu 22.04.

*During this step, `libtango-dev` installation might ask for the
Tango Server IP:PORT. Just accept the default proposed value.*

Once you have that available you can install the python dependencies.
Note that on some systems, you may need to explicitly provide the
path to the tango C++ headers:
```
CPPFLAGS=-I/usr/include/tango poetry install
```

### Run linting and testing

Since this project supports interfacing with Kafka,
we need to spin up a instance for testing. For this
we use Docker Compose so you will need to install
[docker engine](https://docs.docker.com/engine/install/),
and [docker compose](https://docs.docker.com/compose/install/).

When these are available you can run the tests using
```
$ poetry run make python-test
```

Linting can be run in a similar way:
```
$ poetry run make python-lint
```

## Other

### Makefile targets

This project contains a Makefile which acts as a UI for building Docker images, testing images, and for launching interactive developer environments.
For the documentation of the Makefile run ``make help``.


### TANGO References
* https://pytango.readthedocs.io/en/stable/contents.html
* https://pytango.readthedocs.io/en/stable/green_modes/green_modes_server.html
* https://pytango.readthedocs.io/en/stable/testing.html
* https://pytango.readthedocs.io/en/stable/client_api/index.html
* https://pytango.readthedocs.io/en/stable/server_api/server.html