.. _API:

API
===

SDP Queue Connector Device
--------------------------

.. autoclass:: ska_sdp_lmc_queue_connector.sdp_queue_connector.SDPQueueConnector
   :members:
.. autoclass:: ska_sdp_lmc_queue_connector.sdp_queue_connector.QueueConnectorDescriptor
   :members:

Exchange Interfaces
-------------------

.. autoclass:: ska_sdp_lmc_queue_connector.exchange.ExchangeDescriptor
   :members:
.. autoclass:: ska_sdp_lmc_queue_connector.exchange.Exchange
   :members:


Data Source, Sink and Pipe Interfaces
-------------------------------------

.. autoclass:: ska_sdp_lmc_queue_connector.sourcesink.DataSource
   :members:
.. autoclass:: ska_sdp_lmc_queue_connector.sourcesink.DataSink
   :members:
.. autoclass:: ska_sdp_lmc_queue_connector.sourcesink.DataPipe
   :members:

.. _api:tango:

Tango
-----

TangoSubscriptionSource
^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: ska_sdp_lmc_queue_connector.tango_sourcesink.TangoSubscriptionSourceDescriptor
   :members:
.. autoclass:: ska_sdp_lmc_queue_connector.tango_sourcesink.TangoSubscriptionSource
   :members:

TangoPointingSubscriptionSource
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: ska_sdp_lmc_queue_connector.tango_pointing_source.TangoPointingSubscriptionSourceDescriptor
   :members:
.. autoclass:: ska_sdp_lmc_queue_connector.tango_pointing_source.TangoPointingSubscriptionSource
   :members:

TangoAttributeSink
^^^^^^^^^^^^^^^^^^
.. autoclass:: ska_sdp_lmc_queue_connector.tango_sourcesink.TangoLocalAttributeSinkDescriptor
   :members:
.. autoclass:: ska_sdp_lmc_queue_connector.tango_sourcesink.TangoLocalAttributeSink
   :members:

TangoArrayScatterAttributeSink
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: ska_sdp_lmc_queue_connector.tango_array_scatter_sink.TangoArrayScatterAttributeSinkDescriptor
   :members:
.. autoclass:: ska_sdp_lmc_queue_connector.tango_array_scatter_sink.TangoArrayScatterAttributeSink
   :members:

TangoObjectScatterAttributeSink
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: ska_sdp_lmc_queue_connector.tango_object_scatter_sink.TangoObjectScatterAttributeSinkDescriptor
   :members:
.. autoclass:: ska_sdp_lmc_queue_connector.tango_object_scatter_sink.TangoAttributeDescriptor
   :members:
.. autoclass:: ska_sdp_lmc_queue_connector.tango_object_scatter_sink.TangoObjectScatterAttributeSink
   :members:

.. _api:kafka:

Kafka
-----

KafkaConsumerSource
^^^^^^^^^^^^^^^^^^^
.. autoclass:: ska_sdp_lmc_queue_connector.kafka_sourcesink.KafkaConsumerSourceDescriptor
   :members:
.. autoclass:: ska_sdp_lmc_queue_connector.kafka_sourcesink.KafkaConsumerSource
   :members:

KafkaProducerSink
^^^^^^^^^^^^^^^^^
.. autoclass:: ska_sdp_lmc_queue_connector.kafka_sourcesink.KafkaProducerSinkDescriptor
   :members:
.. autoclass:: ska_sdp_lmc_queue_connector.kafka_sourcesink.KafkaProducerSink
   :members:

.. _api:in-memory:

In-Memory
---------

InMemorySource
^^^^^^^^^^^^^^
.. autoclass:: ska_sdp_lmc_queue_connector.in_memory_sourcesink.InMemorySourceDescriptor
   :members:
.. autoclass:: ska_sdp_lmc_queue_connector.in_memory_sourcesink.InMemorySource
   :members:

InMemorySink
^^^^^^^^^^^^
.. autoclass:: ska_sdp_lmc_queue_connector.in_memory_sourcesink.InMemorySinkDescriptor
   :members:
.. autoclass:: ska_sdp_lmc_queue_connector.in_memory_sourcesink.InMemorySink
   :members:

Pipes
-----

.. autoclass:: ska_sdp_lmc_queue_connector.pipe.default_pipe.DefaultPipe
    :members:
.. autoclass:: ska_sdp_lmc_queue_connector.pipe.buffer_pipe.BufferWithTimePipe
    :members:

Format
------

.. autoclass:: ska_sdp_lmc_queue_connector.serialization.Format
   :members: