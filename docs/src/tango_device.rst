.. _Tango Device:

Tango Device
============

Properties
----------

======================== ======== =============== ==============================================
Property                  Type     Default Value   Example Value                               
======================== ======== =============== ==============================================
|exchanges_config_path|   String   ``""``          ``'/component/lmc-queueconnector-01/owner'`` 
------------------------ -------- --------------- ----------------------------------------------
|subarray_id|             String   ``""``         ``"01"``
------------------------ -------- --------------- ----------------------------------------------
|experimental_flow|       Bool     ``True``       ``True``
------------------------ -------- --------------- ----------------------------------------------
|experimental_kafka|      Bool     ``False``      ``True``
======================== ======== =============== ==============================================

.. _State Model:

State Model
-----------

The present implementation utilizes the following native Tango operational states that can be read using the ``state`` attribute.

* **INIT**: the device is currently initialising.
* **FAULT**: the device has experienced an unexpected error from which it needs to be reset.
* **STANDBY**: the device is not configured.
* **OFF**: the device is configured, but not currently in use.
* **OPEN**: the device is opening stream connections.
* **ON**: the device is streaming data.
* **CLOSE**: the device is closing stream connections.

.. This diagram can be edited by opening the file directly in https://app.diagrams.net
.. figure:: _static/img/state-model.drawio.svg

.. note::
    Transition arrows are triggered by a corresponding tango command, with the exception of underlined transitions, which are triggered automatically.


Configuration
-------------

The Queue Connector Tango Device only streams data when it has been configured at runtime. This can be performed via multiple approaches.

Configure by Flows
''''''''''''''''''

To configure via descriptors, set |experimental_flow| to ``True`` (default) and |subarray_id| to filter for flows with processing blocks assigned to that subarray.


Configure by Descriptors (deprecated)
'''''''''''''''''''''''''''''''''''''

To configure via descriptors, set both |experimental_flow| to ``False`` and the |exchanges_config_path| property in the Tango database and the device will configure on initialization. The device will fetch the JSON value at the given path parsed as a |QueueConnectorDescriptor|.


.. |QueueConnectorDescriptor| replace:: :class:`~ska_sdp_lmc_queue_connector.sdp_queue_connector.QueueConnectorDescriptor`
.. |exchanges_config_path| replace:: :attr:`~ska_sdp_lmc_queue_connector.sdp_queue_connector.SDPQueueConnector.exchanges_config_path`
.. |subarray_id| replace:: :attr:`~ska_sdp_lmc_queue_connector.sdp_queue_connector.SDPQueueConnector.subarray_id`
.. |experimental_flow| replace:: :attr:`~ska_sdp_lmc_queue_connector.sdp_queue_connector.SDPQueueConnector.experimental_flow`
.. |experimental_kafka| replace:: :attr:`~ska_sdp_lmc_queue_connector.sdp_queue_connector.SDPQueueConnector.experimental_kafka`
.. |Configure()| replace:: :meth:`~ska_sdp_lmc_queue_connector.sdp_queue_connector.SDPQueueConnector.Configure`
.. |IsMonitoringDB()| replace:: :meth:`~ska_sdp_lmc_queue_connector.sdp_queue_connector.SDPQueueConnector.IsMonitoringDB`
.. |Reset()| replace:: :meth:`~ska_sdp_lmc_queue_connector.sdp_queue_connector.SDPQueueConnector.Reset`