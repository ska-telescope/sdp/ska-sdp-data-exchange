import asyncio
from typing import AsyncGenerator, AsyncIterator, SupportsIndex, TypeVar

_T = TypeVar("_T")


async def merge(*iterators: AsyncIterator[_T]) -> AsyncGenerator[_T, None]:
    """
    Merge multiple AsyncIterator instances, yielding items in the order they appear.
    The merge continues until all iterators are exhausted.

    Args:
        *iterators: Arbitrary number of async iterators.

    Yields:
        Items from all iterators in the order they appear.
    """
    results = asyncio.Queue()
    tasks: list[asyncio.Task] = []
    stop_sentinel = object()

    async def read_from_iterator(iterator: AsyncIterator[_T]):
        """Continuously read from an iterator and put results in the queue."""
        try:
            async for item in iterator:
                await results.put(item)
        finally:
            # Indicate this iterator is finished
            await results.put(stop_sentinel)

    try:  # in python3.11 use TaskGroup
        for iterator in iterators:
            tasks.append(asyncio.create_task(read_from_iterator(iterator)))

        active_tasks = len(tasks)

        while active_tasks > 0:
            # Get the next available item from the queue
            item = await results.get()
            if item is stop_sentinel:
                active_tasks -= 1  # One iterator has completed
            else:
                yield item
    except asyncio.CancelledError:
        for task in tasks:
            task.cancel()
        raise

    # Wait for all tasks to finish (cleanup)
    await asyncio.gather(*tasks, return_exceptions=True)


async def timeout(source: AsyncIterator[_T], duration: float) -> AsyncIterator[_T]:
    """Raise a time-out if an element of the asynchronous sequence
    takes too long to arrive.

    Note: the timeout is not for the entire iterator, but specific to each step of
    the iteration.
    """
    while True:
        try:
            item = await asyncio.wait_for(anext(source), duration)
        except StopAsyncIteration:
            break
        else:
            yield item


async def take(source: AsyncIterator[_T], n: int) -> AsyncIterator[_T]:
    """Forward the first ``n`` elements from an asynchronous sequence.

    If ``n`` is non-positive, it simply terminates before iterating the source.
    """
    if n <= 0:
        return
    i = 0
    async for item in source:
        yield item
        i += 1
        if i >= n:
            return


async def arange(*args: SupportsIndex) -> AsyncIterator[int]:
    """Generate values from a regular iterable."""
    for item in range(*args):
        await asyncio.sleep(0)
        yield item


async def spaceout(source: AsyncIterator[_T], interval: float) -> AsyncIterator[_T]:
    """Make sure the elements of an asynchronous sequence are separated
    in time by at least the given interval.
    """
    target_time = 0.0
    loop = asyncio.get_event_loop()
    async for item in source:
        delta = target_time - loop.time()
        delay = delta if delta > 0 else 0.0
        await asyncio.sleep(delay)
        yield item
        target_time = loop.time() + interval
