import logging

from ska_sdp_config import Config
from tango import DevState

from ska_sdp_lmc_queue_connector.sdp_queue_connector_descriptor import StateModel
from ska_sdp_lmc_queue_connector.watcher.descriptor.flow_adapter import is_function_tagged

__logger = logging.getLogger(__name__)


def flow_status(state: DevState):
    match state:
        case DevState.FAULT:
            return "FAILED"
        case DevState.ON:
            return "FLOWING"
        case DevState.OFF | DevState.OPEN | DevState.CLOSE:
            return "WAITING"
        case _:
            return "UNKNOWN"


def create_flow_state_by_pb_id(config: Config, pb_id: str, state: DevState):
    """"""
    for txn in config.txn():
        for flow_key in txn.flow.list_keys(pb_id=pb_id):
            # TODO: should cache and use a set[Flow]
            tagged = any([is_function_tagged(source) for source in txn.flow.get(flow_key).sources])
            if tagged:
                if not txn.flow.state(flow_key).exists():
                    txn.flow.state(flow_key).create(
                        StateModel(status=flow_status(state)).model_dump()
                    )
                else:
                    __logger.warning(
                        "group state at %s is already created, overwriting",
                        flow_key,
                    )
                    txn.flow.state(flow_key).update(
                        StateModel(status=flow_status(state)).model_dump()
                    )


def update_flow_state_by_pb_id(config: Config, pb_id: str, state):
    for txn in config.txn():
        for flow_key in txn.flow.list_keys(pb_id=pb_id):
            # TODO: should cache and use a set[Flow]
            tagged = any([is_function_tagged(source) for source in txn.flow.get(flow_key).sources])
            if tagged:
                if txn.flow.state(flow_key).exists():
                    txn.flow.state(flow_key).update(
                        StateModel(status=flow_status(state)).model_dump()
                    )
                else:
                    __logger.warning("group state at %s is missing, recreating", flow_key)
                    txn.flow.state(flow_key).create(
                        StateModel(status=flow_status(state)).model_dump()
                    )


def delete_flow_state_by_pb_id(config: Config, pb_id: str):
    for txn in config.txn():
        for flow_key in txn.flow.list_keys(pb_id=pb_id):
            # TODO: should cache and use a set[Flow]
            tagged = any([is_function_tagged(source) for source in txn.flow.get(flow_key).sources])
            if tagged:
                if txn.flow.state(flow_key).exists():
                    txn.flow.state(flow_key).delete()
                else:
                    __logger.warning(
                        "group state at %s is already deleted, ignoring",
                        flow_key,
                    )
