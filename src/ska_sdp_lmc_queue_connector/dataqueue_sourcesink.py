import asyncio
import logging
from typing import Literal

import numpy as np
from overrides import override
from ska_sdp_dataqueues import DataQueueConsumer, DataQueueProducer

from ska_sdp_lmc_queue_connector.pydantic import SkaBaseModel
from ska_sdp_lmc_queue_connector.serialization import Format
from ska_sdp_lmc_queue_connector.sourcesink import DataSink, DataSource, DataType

logger = logging.getLogger(__name__)


class DataQueueConsumerSourceDescriptor(SkaBaseModel):
    type: Literal["DataQueueConsumerSource"] = "DataQueueConsumerSource"
    servers: str | list[str]
    """The Kafka broker(s) to query for metadata and setup the connection"""
    topic: str
    """The Kafka topic to read from"""
    format: Format = "msgpack_numpy"
    """Format of the message bytes"""


class DataQueueConsumerSource(DataSource):
    """A DataSource which consumes messages from a Kafka topic"""

    def __init__(
        self,
        descriptor: DataQueueConsumerSourceDescriptor,
        dtype: np.dtype,  # pylint: disable=unused-argument
        shape: list[int],  # pylint: disable=unused-argument
    ) -> None:
        self.__consumer_description = f"{descriptor.servers}/{descriptor.topic}"
        self.__consumer = DataQueueConsumer(
            server=descriptor.servers,  # type: ignore
            topics=[descriptor.topic],
            encoding=descriptor.format,
            auto_offset_reset="latest",
        )
        self.__desc = descriptor
        self.__running = False

    @override
    async def start(self):
        assert not self.__running, f"consumer to {self.__consumer_description} is already started"
        if self.__consumer is None:
            # consumer cannot be restarted once stopped, recreate instead
            self.__consumer = DataQueueConsumer(
                server=self.__desc.servers,  # type: ignore
                topics=[self.__desc.topic],
                encoding=self.__desc.format,
                auto_offset_reset="latest",
            )
        logger.debug("Started Kafka consumer from %s", self.__consumer_description)
        self.__consumer = await self.__consumer.__aenter__()
        self.__iterator = self.__consumer.__aiter__()
        self.__running = True

    async def stop(self):
        assert self.__running, f"consumer to {self.__consumer_description} is already stopped"
        self.__running = False
        self.__iterator = None
        assert self.__consumer
        await self.__consumer.__aexit__(None, None, None)
        logger.debug("Stopped Kafka consumer from %s", self.__consumer_description)
        self.__consumer = None

    @override
    async def __anext__(self) -> DataType:
        """
        Reads one message from the configured Kafka queue, blocking
        until one is received or the consumer is stopped.

        Raises:
            StopAsyncIteration
        """
        if not self.__running:
            raise StopAsyncIteration

        try:
            assert self.__consumer, "__consumer must be set when running"
            msg: tuple = await self.__iterator.__anext__()  # type: ignore
            _, value = msg
            return value
        except StopAsyncIteration:
            raise StopAsyncIteration


class DataQueueProducerSinkDescriptor(SkaBaseModel):
    type: Literal["DataQueueProducerSink"] = "DataQueueProducerSink"
    servers: str | list[str]
    """The data queue server for the connection"""
    topic: str
    """The data queue topic to write messages to"""
    format: Format = "msgpack_numpy"
    """The format of the data queue message."""
    message_max_bytes: int = 2**20
    """
    The max size of encoded messages in bytes.
    NOTE: The 1MiB default is recommended by Kafka, sending larger messages
    higher than this will linearly increase latency.
    """


class DataQueueProducerSink(DataSink):
    """A DataSink which produces messages for a Kafka topic"""

    def __init__(
        self,
        descriptor: DataQueueProducerSinkDescriptor,
        dtype: np.dtype,  # pylint: disable=unused-argument
        shape: list[int],  # pylint: disable=unused-argument
    ) -> None:
        self.__producer_description = f"{descriptor.servers}/{descriptor.topic}"
        self.__topic = descriptor.topic
        self.__desc = descriptor
        self.__producer = DataQueueProducer(
            server=descriptor.servers,  # type: ignore
            message_max_bytes=descriptor.message_max_bytes,
            topic=self.__topic,
            encoding=self.__desc.format,
        )

        # AIOKafkaProducer cannot be stopped during a send operation
        self.__lock = asyncio.Lock()
        self.__running = False

    @override
    async def start(self):
        assert not self.__running, f"producer to {self.__producer_description} is already started"
        logger.debug("Started producer to %s", self.__producer_description)
        if self.__producer is None:
            # producer cannot be restarted once stopped, recreate instead
            self.__producer = DataQueueProducer(
                server=self.__desc.servers,  # type: ignore
                message_max_bytes=self.__desc.message_max_bytes,
                topic=self.__topic,
                encoding=self.__desc.format,
            )
        await self.__producer.__aenter__()
        self.__running = True

    @override
    async def stop(self):
        assert self.__running, f"producer to {self.__producer_description} is already stopped"
        logger.debug("Stopped producer to %s", self.__producer_description)
        assert self.__producer
        async with self.__lock:
            await self.__producer.__aexit__(None, None, None)
            self.__producer = None
            self.__running = False

    @override
    async def awrite(self, value: DataType):
        async with self.__lock:
            if self.__running:
                assert self.__producer, "__producer must be set when running"
                await self.__producer.send(value)
            else:
                logger.warning(
                    "Producer to %s discarding data due to closed sink: %s",
                    self.__producer_description,
                    value,
                )
