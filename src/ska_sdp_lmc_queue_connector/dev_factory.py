import logging
from typing import Awaitable, Optional

from tango import DeviceProxy, GreenMode, get_device_proxy
from tango.green import get_executor
from tango.test_context import MultiDeviceTestContext


class DevFactory:
    """
    A factory class to construct (and cache) Tango device proxies.

    If used using testing, will seamlessly create device proxies that reference
    the test context rather than devices using the TANGO_HOST.

    More information on tango testing can be found at the following link:
    https://pytango.readthedocs.io/en/stable/testing.html

    """

    _test_context: Optional[MultiDeviceTestContext] = None

    def __init__(self, green_mode=GreenMode.Synchronous):
        self.dev_proxys = {}
        self.logger = logging.getLogger(__name__)
        self.default_green_mode = green_mode

    def get_sync_proxy(self, dev_name: str) -> DeviceProxy:
        """
        Creates or gets a cached DeviceProxy using green_mode=Synchronous

        Args:
            dev_name: device name or full object name to connect to. See
                https://tango-controls.readthedocs.io/en/latest/development/general-guidelines/naming.html
                for full format

        Returns:
            A device proxy using green_mode=Synchronous for the specified
            device
        """
        key = (GreenMode.Synchronous, dev_name)
        if key not in self.dev_proxys:
            self.dev_proxys[key] = self._create_proxy(dev_name, green_mode=GreenMode.Synchronous)

        return self.dev_proxys[key]

    async def get_async_proxy(self, dev_name: str) -> DeviceProxy:
        """
        Creates or gets a cached DeviceProxy using green_mode=Asyncio

        Args:
            dev_name: device name or full object name to connect to. See
                https://tango-controls.readthedocs.io/en/latest/development/general-guidelines/naming.html
                for full format

        Returns:
            A device proxy using green_mode=Asyncio for the specified device

        Raises:
            RuntimeError: If the proxy is attempted to be created from an
                invalid context (most likely using MultiDeviceTestContext)
        """
        key = (GreenMode.Asyncio, dev_name)
        if key not in self.dev_proxys:
            self.dev_proxys[key] = await self._create_proxy(dev_name, green_mode=GreenMode.Asyncio)

        return self.dev_proxys[key]

    def _create_proxy(
        self, dev_name: str, green_mode=None
    ) -> Awaitable[DeviceProxy] | DeviceProxy:
        """
        Creates a device proxy with the specified green mode, or the default
        mode used by the Factory. For non-synchronous green modes, the device
        proxy will be wrapped in the given green_mode's async type.
        e.g. GreenMode.Asyncio will wrap the device proxy in a co-routine.
        """

        if green_mode is None:
            green_mode = self.default_green_mode

        if DevFactory._test_context is not None:
            # If we're running this in a test, using MultiDeviceTestContext,
            # and we request an async DeviceProxy we hit a bug/limitation in
            # Tango where the DeviceProxy will happily report it is using the
            # async green_mode, but actually silently runs synchronously.
            # Here we replicate the check which causes this, but throw instead
            # so at least we'll be aware it's occurring!
            # See https://gitlab.com/tango-controls/pytango/-/issues/509 for
            # more details
            if green_mode != GreenMode.Synchronous:
                executor = get_executor(green_mode)
                if not executor.in_executor_context():
                    raise RuntimeError(
                        "Unable to create non-synchronous proxy"
                        " in different context/thread. If running"
                        " in test code, use "
                        " MultiDeviceTestContext.get_device()"
                    )

            # Not using MultiDeviceTestContext.get_device() as that always
            # returns a synchronous proxy
            dev_name = DevFactory._test_context.get_device_access(dev_name)

        # Use get_device_proxy instead of tango.DeviceProxy(...) so the proxy
        # creation is wrapped in the green_mode (i.e. this can be awaited
        # with green_mode=Asyncio)
        # This is preferential as there is a bunch of initialisation logic
        # over the network which we'd prefer not to block on
        return get_device_proxy(dev_name, green_mode=green_mode)
