import logging
from contextlib import AbstractContextManager, ExitStack, closing

import numpy as np

from ska_sdp_lmc_queue_connector.dataqueue_sourcesink import (
    DataQueueConsumerSource,
    DataQueueConsumerSourceDescriptor,
    DataQueueProducerSink,
    DataQueueProducerSinkDescriptor,
)
from ska_sdp_lmc_queue_connector.exchange import Exchange, ExchangeDescriptor
from ska_sdp_lmc_queue_connector.in_memory_sourcesink import (
    InMemorySink,
    InMemorySinkDescriptor,
    InMemorySource,
    InMemorySourceDescriptor,
)
from ska_sdp_lmc_queue_connector.kafka_sourcesink import (
    KafkaConsumerSource,
    KafkaConsumerSourceDescriptor,
    KafkaProducerSink,
    KafkaProducerSinkDescriptor,
)
from ska_sdp_lmc_queue_connector.pipe.buffer_pipe import (
    BufferWithTimePipe,
    BufferWithTimePipeDescriptor,
)
from ska_sdp_lmc_queue_connector.pipe.default_pipe import DefaultPipe, DefaultPipeDescriptor
from ska_sdp_lmc_queue_connector.sourcesink import DataPipe, DataSink, DataSource
from ska_sdp_lmc_queue_connector.tango_array_scatter_sink import (
    TangoArrayScatterAttributeSink,
    TangoArrayScatterAttributeSinkDescriptor,
)
from ska_sdp_lmc_queue_connector.tango_object_scatter_sink import (
    TangoObjectScatterAttributeSink,
    TangoObjectScatterAttributeSinkDescriptor,
)
from ska_sdp_lmc_queue_connector.tango_pointing_source import (
    TangoPointingSubscriptionSource,
    TangoPointingSubscriptionSourceDescriptor,
)
from ska_sdp_lmc_queue_connector.tango_sourcesink import (
    TangoLocalAttributeSink,
    TangoLocalAttributeSinkDescriptor,
    TangoSubscriptionSource,
    TangoSubscriptionSourceDescriptor,
)


def create_exchange_groups(
    device, group_descriptor: dict[str | None, list[ExchangeDescriptor]]
) -> dict[str | None, list[Exchange]]:
    """Factory method for creating one or more exchange groups from relevant descriptors.

    Args:
        device (SdpQueueConnector): tango device instance for tango sinks
        group_descriptor (dict[str | None, list[ExchangeDescriptor]]): Descriptor for one or
        more exchange groups.

    Returns:
        dict[str | None, list[Exchange]]: exchange group mapping.
    """
    group_exchanges: dict[str | None, list[Exchange]] = {}

    for group_name, exchanges in group_descriptor.items():
        exchange_list = []

        for exchange in exchanges:
            dtype = exchange.dtype
            shape = exchange.shape
            pipe = _create_pipe(exchange.pipe, dtype, shape)
            sources = [
                _create_source(device, source, dtype, shape)
                for source in _listify(exchange.source)
            ]
            sink = _create_sink(
                device,
                exchange.sink,
                pipe.output_dtype,
                pipe.output_shape,
            )

            exchange_list.append(Exchange(sources, sink, pipe))
        group_exchanges[group_name] = exchange_list

    return group_exchanges


def enter_exchange_contexts(
    group_exchanges: dict[str | None, list[Exchange]]
) -> dict[str | None, ExitStack]:
    """Given a mapping of exchange groups, any elements containing contexts are entered and placed
    in an ExitStack, particularly for cleaning up tango resources. Should entering fail for
    any exchange group, all exchanges will be cleaned up and the exception reraised.

    Args:
        group_exchanges (dict[str | None, list[Exchange]]): exchange group mapping.

    Returns:
        dict[str | None, ExitStack]: ExitStack mapping to be closed when a group is removed.
    """
    group_contexts: dict[str | None, ExitStack] = {}
    try:
        for group_name, exchanges in group_exchanges.items():
            stack = ExitStack()
            group_contexts[group_name] = stack

            for exchange in exchanges:
                if isinstance(exchange.sink, AbstractContextManager):
                    stack.enter_context(exchange.sink)
    except Exception:
        for context in group_contexts.values():
            with closing(context):
                logging.exception("Exception entering exchange group, exiting")
        raise
    return group_contexts


def _create_pipe(descriptor, dtype: np.dtype, shape: list[int]) -> DataPipe:
    match descriptor:
        case DefaultPipeDescriptor():
            pipe = DefaultPipe(dtype, shape)
        case BufferWithTimePipeDescriptor():
            pipe = BufferWithTimePipe(descriptor, dtype, shape)
        case _:
            raise NotImplementedError(type(descriptor))
    return pipe


def _create_source(device, descriptor, dtype: np.dtype, shape: list[int]) -> DataSource:
    match descriptor:
        case InMemorySourceDescriptor():
            source = InMemorySource(descriptor, dtype, shape)
        case TangoPointingSubscriptionSourceDescriptor():
            source = TangoPointingSubscriptionSource(descriptor, device, dtype, shape)
        case TangoSubscriptionSourceDescriptor():
            source = TangoSubscriptionSource(descriptor, device, dtype, shape)
        case KafkaConsumerSourceDescriptor():
            source = KafkaConsumerSource(descriptor, dtype, shape)
        case DataQueueConsumerSourceDescriptor():
            source = DataQueueConsumerSource(descriptor, dtype, shape)
        case _:
            raise NotImplementedError(type(descriptor))
    return source


def _create_sink(device, descriptor, dtype: np.dtype, shape: list[int]) -> DataSink:
    match descriptor:
        case InMemorySinkDescriptor():
            sink = InMemorySink(descriptor)
        case TangoLocalAttributeSinkDescriptor():
            sink = TangoLocalAttributeSink(descriptor, device, dtype, shape)
        case TangoArrayScatterAttributeSinkDescriptor():
            sink = TangoArrayScatterAttributeSink(descriptor, device, dtype, shape)
        case TangoObjectScatterAttributeSinkDescriptor():
            sink = TangoObjectScatterAttributeSink(descriptor, device, dtype, shape)
        case KafkaProducerSinkDescriptor():
            sink = KafkaProducerSink(descriptor, dtype, shape)
        case DataQueueProducerSinkDescriptor():
            sink = DataQueueProducerSink(descriptor, dtype, shape)
        case _:
            raise NotImplementedError(type(descriptor))
    return sink


def _listify(obj):
    return obj if isinstance(obj, list) else [obj]
