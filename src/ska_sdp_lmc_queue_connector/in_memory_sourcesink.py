import asyncio
import logging
from asyncio import Queue
from typing import Literal

import numpy as np
from numpy.lib.recfunctions import unstructured_to_structured
from overrides import override

from ska_sdp_lmc_queue_connector.numpy_helper import create_numpy_scalar
from ska_sdp_lmc_queue_connector.pydantic import SkaBaseModel
from ska_sdp_lmc_queue_connector.sourcesink import DataSink, DataSource, DataType, SchemaDataType

logger = logging.getLogger(__name__)


class InMemorySourceDescriptor(SkaBaseModel):
    """Descriptor for instantiating an InMemorySource"""

    type: Literal["InMemorySource"] = "InMemorySource"
    data: list[SchemaDataType]
    """Data values to be read from the source to a sink"""
    delay: float = 0
    """Time delay in seconds before the next data value is available to read"""


async def wait_for_condition(condition: asyncio.Condition, timeout: float):
    """
    Returns True once the condition is notified, otherwise
    returns False if the timeout period has elapsed.
    """
    async with condition:
        try:
            return await asyncio.wait_for(condition.wait(), timeout=timeout)
        except asyncio.TimeoutError:
            return False


class InMemorySource(DataSource):
    """
    An in-memory implementation of a DataSource for testing.
    """

    def __init__(
        self,
        desc: InMemorySourceDescriptor,
        dtype: np.dtype,
        shape: list[int] | None = None,
    ):
        if shape is None:
            shape = []
        self.__queue = Queue()
        self.__running = False
        self.__stop_condition = asyncio.Condition()
        self.__delay = desc.delay
        for v in desc.data:
            if dtype == np.bytes_:
                assert isinstance(v, tuple)
                assert len(v) == 2
                assert isinstance(v[0], str)
                assert isinstance(v[1], bytes)
                data = v
            elif dtype == np.object_:  # dict
                data = v
            elif np.dtype(dtype).names is not None:
                data = unstructured_to_structured(np.array(v), dtype=dtype)
                if shape:
                    data = np.full(shape, data)
            elif len(shape) == 0:
                data = create_numpy_scalar(v, dtype=dtype)
            else:
                data = np.full(
                    shape,
                    v,
                    # NOTE: np.str_ not compatible with np.full()
                    dtype=dtype if dtype != np.str_ else None,
                )
            self.__queue.put_nowait(data)

    @override
    async def start(self):
        self.__running = True

    @override
    async def stop(self):
        await self.__queue.put(None)
        self.__running = False
        async with self.__stop_condition:
            self.__stop_condition.notify_all()

    @override
    async def __anext__(self) -> DataType:
        if self.__running:
            await wait_for_condition(self.__stop_condition, self.__delay)

        if self.__running:
            v = await self.__queue.get()
            if v is not None:
                return v
            else:
                await self.__queue.put(None)
        raise StopAsyncIteration


class InMemorySinkDescriptor(SkaBaseModel):
    """Descriptor for instantiating an InMemorySink"""

    type: Literal["InMemorySink"] = "InMemorySink"
    key: str
    """Key for accessing the stored data queue via classmethod"""


class InMemorySink(DataSink):
    """
    An in-memory implementation of a DataSink for testing. Instances of
     sink data queues exist as class members referenced via a lookup key.
    """

    __queues = {}

    def __init__(self, desc: InMemorySinkDescriptor):
        self.__key = desc.key
        if self.__key not in InMemorySink.__queues:
            InMemorySink.__queues[self.__key] = Queue()

    @property
    def queue(self) -> Queue:
        return InMemorySink.__queues[self.__key]

    @classmethod
    def get_queue(cls, key: str) -> Queue:
        return cls.__queues[key]

    @classmethod
    def set_queue(cls, key: str, value: Queue):
        cls.__queues[key] = value

    @override
    async def start(self):
        pass

    @override
    async def stop(self):
        pass

    @override
    async def awrite(self, value: DataType):
        await self.queue.put(value)
