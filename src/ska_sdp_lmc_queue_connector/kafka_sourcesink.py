import asyncio
import logging
import warnings
from typing import Callable, Literal

import numpy as np
from aiokafka import AIOKafkaConsumer, AIOKafkaProducer, ConsumerStoppedError
from overrides import override

from ska_sdp_lmc_queue_connector.pydantic import SkaBaseModel, Slice
from ska_sdp_lmc_queue_connector.serialization import (
    Format,
    create_deserializer,
    create_serializer,
)
from ska_sdp_lmc_queue_connector.sourcesink import DataSink, DataSource, DataType

logger = logging.getLogger(__name__)


class KafkaConsumerSourceDescriptor(SkaBaseModel):
    type: Literal["KafkaConsumerSource"] = "KafkaConsumerSource"
    servers: str | list[str]
    """The Kafka broker(s) to query for metadata and setup the connection"""
    topic: str
    """The Kafka topic to read from"""
    format: Format = "python"
    """Format of the message bytes"""


class KafkaConsumerSource(DataSource):
    """A DataSource which consumes messages from a Kafka topic"""

    def __init__(
        self,
        descriptor: KafkaConsumerSourceDescriptor,
        dtype: np.dtype,
        shape: list[int],
    ) -> None:
        self.__consumer_description = f"{descriptor.servers}/{descriptor.topic}"
        self.__consumer = AIOKafkaConsumer(
            descriptor.topic,
            bootstrap_servers=descriptor.servers,  # type: ignore
        )
        self.__desc = descriptor
        self.__deserialize_lambda = create_deserializer(dtype, shape, descriptor.format)
        self.__running = False

    @override
    async def start(self):
        assert not self.__running, f"consumer to {self.__consumer_description} is already started"
        logger.debug("Started Kafka consumer from %s", self.__consumer_description)
        if self.__consumer is None:
            # consumer cannot be restarted once stopped, recreate instead
            self.__consumer = AIOKafkaConsumer(
                self.__desc.topic,
                bootstrap_servers=self.__desc.servers,  # type: ignore
            )
        await self.__consumer.start()
        self.__running = True

    async def stop(self):
        assert self.__running, f"consumer to {self.__consumer_description} is already stopped"
        self.__running = False
        assert self.__consumer
        await self.__consumer.stop()
        logger.debug("Stopped Kafka consumer from %s", self.__consumer_description)
        self.__consumer = None

    @override
    async def __anext__(self) -> DataType:
        """
        Reads one message from the configured Kafka queue, blocking
        until one is received or the consumer is stopped.

        Raises:
            StopAsyncIteration
        """
        if not self.__running:
            raise StopAsyncIteration

        try:
            assert self.__consumer, "__consumer must be set when running"
            value: bytes = (await self.__consumer.getone()).value  # type: ignore
            logger.debug(
                "Received message of size %s bytes from %s",
                f"{len(value):,}",
                self.__consumer_description,
            )
            return self.__deserialize_lambda(value)
        except ConsumerStoppedError:
            raise StopAsyncIteration


class KafkaProducerSinkDescriptor(SkaBaseModel):
    type: Literal["KafkaProducerSink"] = "KafkaProducerSink"
    servers: str | list[str]
    """The Kafka broker(s) to query for metadata and setup the connection"""
    topic: str
    """The Kafka topic to write messages to"""
    format: Format = "python"
    """The format of the Kafka message."""
    message_max_bytes: int = 2**20
    """
    The max size of encoded messages in bytes.
    NOTE: The 1MiB default is recommended by Kafka, sending larger messages
    higher than this will linearly increase latency.
    """

    class TimestampOptions(SkaBaseModel):
        """
        A set of kafka producer options related to overriding Kafka message
        timestamps from dynamic data instead of the system time when messages
        are produced. Timestamps in dynamic data must be one of:

        * An offset to unix epoch in milliseconds.
        * A numpy datetime64 on TAI scale.
        """

        slices: tuple[int | Slice, ...] = tuple()
        """
        Timestamp slice location for multidimensional data.
        Size must match number of dimensions.
        """
        key: str | None = None
        """
        Timestamp key for dictionary-like data types.
        """
        reducer: Literal["min", "max", "mean"] | None = None
        """
        Axes reduce operation for timestamps.
        """

    timestamp_options: TimestampOptions | None = None
    """An optional group of settings related to extracting Kafka
    timestamps from dynamic data. None results in using the current
    time.
    """


_REDUCER_MAP = {
    None: lambda x: x,
    "min": np.min,
    "max": np.max,
    "mean": np.mean,
}


class KafkaProducerSink(DataSink):
    """A DataSink which produces messages for a Kafka topic"""

    def __init__(
        self,
        descriptor: KafkaProducerSinkDescriptor,
        dtype: np.dtype,
        shape: list[int],
    ) -> None:
        if descriptor.timestamp_options is not None:
            if len(shape) != len(descriptor.timestamp_options.slices):
                raise RuntimeError(
                    f"slices dims mismatch, {shape} != {descriptor.timestamp_options.slices}"
                )
            if (getattr(dtype, "names", None) is not None) or dtype == np.object_:
                if descriptor.timestamp_options.key is None:
                    raise RuntimeError(f"timestamp key is required for dtype {dtype}")
            else:
                if descriptor.timestamp_options.key is not None:
                    warnings.warn(RuntimeWarning(f"timestamp key is not used for dtype {dtype}"))
            if any(isinstance(v, slice) for v in descriptor.timestamp_options.slices):
                if descriptor.timestamp_options.reducer is None:
                    raise RuntimeError("reducer required when using slices")

            self.__timestamp_slices = descriptor.timestamp_options.slices
            self.__timestamp_key = descriptor.timestamp_options.key

        self.__producer_description = f"{descriptor.servers}/{descriptor.topic}"
        self.__producer = AIOKafkaProducer(
            bootstrap_servers=descriptor.servers,  # type: ignore
            max_request_size=descriptor.message_max_bytes,
        )
        self.__topic = descriptor.topic
        self.__desc = descriptor
        self.__serialize_lambda = create_serializer(dtype, shape, descriptor.format)

        if (
            descriptor.timestamp_options is not None
            and descriptor.timestamp_options.reducer is not None
        ):
            reducer = _REDUCER_MAP[descriptor.timestamp_options.reducer]
        else:
            reducer = _REDUCER_MAP[None]
        self.__timestamp_lambda = self.__create_timestamp_lambda(dtype, shape, reducer)

        # AIOKafkaProducer cannot be stopped during a send operation
        self.__lock = asyncio.Lock()
        self.__running = False

    @override
    async def start(self):
        assert not self.__running, f"producer to {self.__producer_description} is already started"
        logger.debug("Started producer to %s", self.__producer_description)
        if self.__producer is None:
            # producer cannot be restarted once stopped, recreate instead
            self.__producer = AIOKafkaProducer(
                bootstrap_servers=self.__desc.servers,  # type: ignore
                max_request_size=self.__desc.message_max_bytes,
            )
        await self.__producer.start()
        self.__running = True

    @override
    async def stop(self):
        assert self.__running, f"producer to {self.__producer_description} is already stopped"
        logger.debug("Stopped producer to %s", self.__producer_description)
        assert self.__producer
        async with self.__lock:
            await self.__producer.stop()
            self.__producer = None
            self.__running = False

    @override
    async def awrite(self, value: DataType):
        async with self.__lock:
            if self.__running:
                data = self.__serialize_lambda(value)
                assert self.__producer, "__producer must be set when running"
                await self.__producer.send_and_wait(
                    self.__topic,
                    data,
                    timestamp_ms=self.__timestamp_lambda(value),
                )
            else:
                logger.warning(
                    "Producer to %s discarding data due to closed sink: %s",
                    self.__producer_description,
                    value,
                )

    def __create_timestamp_lambda(
        self, dtype: np.dtype, shape: list[int], reducer
    ) -> Callable[[DataType], None | int]:
        """Generates a function to extracts a Kafka timestamp from dynamic data
        of format specified by dtype and shape.

        Args:
            dtype (np.dtype): the numpy datatype
            shape (list[int]): the data shape

        Returns:
            Callable[[DataType], None | int]: function capable of extracting
            timestamps from its argument.
        """
        if self.__desc.timestamp_options is None:
            return lambda _: None
        elif getattr(dtype, "names", None) is not None:
            # Struct or record array
            return lambda data: KafkaProducerSink.__to_kafka_timestamp(
                reducer(data[self.__timestamp_slices][self.__timestamp_key])  # type: ignore
            )
        elif dtype == np.object_:
            # Mapping
            return lambda data: KafkaProducerSink.__to_kafka_timestamp(
                reducer(np.array(data[self.__timestamp_key]))  # type: ignore
            )
        elif len(shape) > 0:
            # Regular ndarray
            return lambda data: KafkaProducerSink.__to_kafka_timestamp(
                reducer(data[self.__timestamp_slices])  # type: ignore
            )
        else:
            # Scalar
            return lambda data: KafkaProducerSink.__to_kafka_timestamp(
                reducer(np.array(data))  # type: ignore
            )

    @staticmethod
    def __to_kafka_timestamp(value: np.generic) -> int:
        """
        Converts a numpy value to unix datetime in milliseconds required
        by kafka timestamps.
        """
        return value.astype("datetime64[ms]").astype(np.uint64)
