from typing import Any

import numpy as np


def create_numpy_scalar(value, dtype: np.dtype) -> Any:
    """
    Creates a numpy scalar for a given scalar value and dtype.

    Args:
        value (Any): scalar value object
        dtype (np.dtype): numpy dtype instance

    Raises:
        AssertionError: value argument is not scalar

    Returns:
        _type_: scalar numpy value
    """
    array = np.array(value, dtype)
    assert array.shape == ()
    return array[()]  # type: ignore
