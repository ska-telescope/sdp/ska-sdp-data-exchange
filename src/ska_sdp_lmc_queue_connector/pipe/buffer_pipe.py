import asyncio
from contextlib import suppress
from typing import AsyncGenerator, AsyncIterator, Literal, TypeVar

import numpy as np

from ska_sdp_lmc_queue_connector.pydantic import SkaBaseModel
from ska_sdp_lmc_queue_connector.sourcesink import DataPipe

_T = TypeVar("_T")


class BufferWithTimePipeDescriptor(SkaBaseModel):
    type: Literal["BufferWithTimePipe"] = "BufferWithTimePipe"
    timespan: float = 0
    """Timespan in seconds for the buffer to grow before flushing the stream"""
    flatten: bool = False
    """When enabled expands the existing dynamic dimension, otherwise adds
    a new dynamic dimension.
    """


class BufferWithTimePipe(DataPipe):
    """
    Buffers data with time window by dynamically by adding a dynamic
    dimension to the ouput shape and appending data until the time window
    closes.
    """

    def __init__(
        self,
        desc: BufferWithTimePipeDescriptor,
        dtype: np.dtype,
        shape: list[int],
    ):
        self.__dtype = dtype
        if not desc.flatten:
            self.__shape = [-1] + shape
        else:
            self.__shape = [-1] + shape[1:]
        self.__timespan = desc.timespan

    @property
    def output_dtype(self) -> np.dtype:
        return self.__dtype

    @property
    def output_shape(self) -> list[int]:
        return self.__shape

    def __call__(self, iterator: AsyncIterator) -> AsyncGenerator:
        return buffer_with_time(iterator, self.__timespan, shape=tuple(self.__shape))


async def buffer_with_time(
    iterator: AsyncIterator[_T],
    timespan: float,
    shape: tuple[int, ...] | None,
) -> AsyncGenerator[_T, None]:
    """Asynchronous pipe operator that performs time buffering
    over items in the stream by grouping into a list.

    Args:
        iterator (AsyncIterator[_T]): The input stream.
        timespan (float): maximum time span between the first and last
            item in a single buffer in the output stream.
        shape (tuple[int, ...]): optional shape to reshape final output with.

    Returns:
        AsyncIterator: The output stream.

    Yields:
        Iterator[AsyncIterator]: A list of buffered stream items.
    """
    buffer = []
    stop_event = asyncio.Event()

    async def buffer_stream() -> None:
        try:
            async for item in iterator:
                buffer.append(item)
        finally:
            stop_event.set()

    async def buffer_flush() -> AsyncGenerator[_T, None]:
        while not stop_event.is_set() or buffer:
            _, pending = await asyncio.wait(
                [
                    asyncio.create_task(stop_event.wait()),
                    asyncio.create_task(asyncio.sleep(timespan)),
                ],
                return_when=asyncio.FIRST_COMPLETED,
            )
            for task in pending:
                task.cancel()

            if buffer:
                yield buffer[:]
                buffer.clear()

    stream_task = asyncio.create_task(buffer_stream())
    try:
        async for chunk in buffer_flush():
            yield np.array(chunk).reshape(shape)
    except Exception:
        # Ensure task is cancelled if an exception occurs
        stream_task.cancel()
        with suppress(asyncio.CancelledError):
            await stream_task
        raise
    else:
        await stream_task
    finally:
        # Handle any remaining items in the buffer
        if buffer:
            yield np.array(buffer).reshape(shape)
