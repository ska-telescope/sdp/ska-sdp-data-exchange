from typing import AsyncIterator, Literal

import numpy as np

from ska_sdp_lmc_queue_connector.pydantic import SkaBaseModel
from ska_sdp_lmc_queue_connector.sourcesink import DataPipe


class DefaultPipeDescriptor(SkaBaseModel):
    type: Literal["DefaultPipe"] = "DefaultPipe"


class DefaultPipe(DataPipe):
    """
    Simple pipe that does not modify the data stream.
    """

    def __init__(self, dtype: np.dtype, shape: list[int]):
        self.__dtype = dtype
        self.__shape = shape

    @property
    def output_dtype(self) -> np.dtype:
        return self.__dtype

    @property
    def output_shape(self) -> list[int]:
        return self.__shape

    def __call__(self, iterator: AsyncIterator) -> AsyncIterator:
        return iterator
