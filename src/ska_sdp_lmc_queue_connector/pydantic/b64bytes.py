import base64
from typing import Annotated, Any

from pydantic import GetCoreSchemaHandler
from pydantic_core import core_schema


class _B64Bytes:
    @classmethod
    def __get_pydantic_core_schema__(
        cls,
        _source_type: Any,
        _handler: GetCoreSchemaHandler,
    ) -> core_schema.CoreSchema:
        def validate_from_python(value):
            if isinstance(value, (bytes, bytearray, memoryview)):
                return value
            else:
                return base64.b64decode(value)

        return core_schema.json_or_python_schema(
            json_schema=core_schema.no_info_after_validator_function(
                base64.b64decode,
                schema=core_schema.str_schema(),
            ),
            python_schema=core_schema.no_info_plain_validator_function(validate_from_python),
            serialization=core_schema.plain_serializer_function_ser_schema(base64.b64encode),
        )


B64Bytes = Annotated[bytes, _B64Bytes]
