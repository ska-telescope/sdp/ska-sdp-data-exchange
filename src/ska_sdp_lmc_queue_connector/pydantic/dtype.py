from typing import Annotated, Any

import numpy as np
from pydantic import GetCoreSchemaHandler
from pydantic_core import core_schema


class _DType:
    @classmethod
    def __get_pydantic_core_schema__(
        cls,
        _source_type: Any,
        _handler: GetCoreSchemaHandler,
    ) -> core_schema.CoreSchema:
        """Core schema for validating and serializing"""

        def validate_dtype(value: Any) -> Any:
            try:
                return np.dtype(value)
            except TypeError as e:
                raise ValueError(*e.args) from e

        def validate_from_json(value: Any) -> Any:
            if isinstance(value, str):
                return validate_dtype(value)
            elif isinstance(value, list):
                return validate_dtype([(pair[0], pair[1]) for pair in value])
            else:
                raise ValueError(f"Unknown dtype {value}")

        def serialize_from_python(value: Any) -> Any:
            """
            Adapts a python value to a json serializable value
            """
            if isinstance(value, np.dtype):
                if value.names is None:
                    return value.name
                else:
                    return value.descr
            elif isinstance(value, str):
                return value
            elif isinstance(value, list):
                res = []
                for item in value:
                    assert len(item) == 2
                    name = item[1].__name__ if isinstance(item[1], type) else item[1]
                    res.append([item[0], name])
                return res
            else:
                raise ValueError(f"Unknown dtype {value}")

        return core_schema.json_or_python_schema(
            json_schema=core_schema.union_schema(
                [
                    core_schema.no_info_after_validator_function(
                        validate_dtype,
                        schema=core_schema.str_schema(),
                    ),
                    core_schema.no_info_after_validator_function(
                        validate_from_json,
                        schema=core_schema.list_schema(
                            items_schema=core_schema.list_schema(
                                min_length=2,
                                max_length=3,
                            )
                        ),
                    ),
                ]
            ),
            python_schema=core_schema.no_info_after_validator_function(
                validate_dtype,
                schema=core_schema.any_schema(),
            ),
            serialization=core_schema.plain_serializer_function_ser_schema(serialize_from_python),
        )


DType = Annotated[np.dtype, _DType]
"""
A numpy dtype value that can validate from dtype-like python values and
serializes to human readable JSON values. Does not support structured
definitions with overlapping fields.
"""
