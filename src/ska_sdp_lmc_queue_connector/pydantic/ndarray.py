from typing import Annotated, Any

import numpy as np
from pydantic import GetCoreSchemaHandler
from pydantic_core import core_schema


class _NDArray:
    @classmethod
    def __get_pydantic_core_schema__(
        cls,
        _source_type: Any,
        _handler: GetCoreSchemaHandler,
    ) -> core_schema.CoreSchema:
        """Core schema for validating and serializing"""

        def validate_from_arraylike(value) -> np.ndarray:
            return np.array(value)

        def serialize_from_python(value) -> list:
            return value.tolist()

        return core_schema.json_or_python_schema(
            json_schema=core_schema.chain_schema(
                [
                    core_schema.list_schema(),
                    core_schema.no_info_plain_validator_function(validate_from_arraylike),
                ]
            ),
            python_schema=core_schema.no_info_plain_validator_function(validate_from_arraylike),
            serialization=core_schema.plain_serializer_function_ser_schema(serialize_from_python),
        )


NDArray = Annotated[np.ndarray, _NDArray]
