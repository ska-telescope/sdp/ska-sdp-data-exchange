from pydantic import BaseModel, ConfigDict


class SkaBaseModel(BaseModel):
    """Base Model configuration for SKA LMC related schema."""

    model_config = ConfigDict(
        strict=True,
        extra="forbid",
        arbitrary_types_allowed=False,
        frozen=True,
        ser_json_inf_nan="constants",  # NOTE: non-standard JSON
    )

    def model_dump_json_nodb(self):
        # BUG: tango test context in nodb/filedatabase mode doesn't escape
        # certain characters correctly in valid json strings provided as
        # device properties. This serializer avoids the runtime errors in
        # test contexts until the following tango fix is made available.
        # https://gitlab.com/tango-controls/cppTango/-/merge_requests/1152
        return self.model_dump_json() + " "
