from typing import Annotated, Any, Callable

from pydantic_core import core_schema


class _Slice:
    @classmethod
    def __get_pydantic_core_schema__(
        cls,
        _source_type: Any,
        _handler: Callable[[Any], core_schema.CoreSchema],
    ) -> core_schema.CoreSchema:
        """Core schema for validating and serializing"""

        def validate_from_list(value: list) -> slice:
            return slice(value[0], value[1], value[2])

        from_list_schema = core_schema.chain_schema(
            [
                core_schema.list_schema(
                    items_schema=core_schema.nullable_schema(core_schema.int_schema()),
                    min_length=3,
                    max_length=3,
                ),
                core_schema.no_info_plain_validator_function(validate_from_list),
            ]
        )
        return core_schema.json_or_python_schema(
            json_schema=from_list_schema,
            python_schema=core_schema.is_instance_schema(slice),
            serialization=core_schema.plain_serializer_function_ser_schema(
                lambda instance: [instance.start, instance.stop, instance.step]
            ),
        )


Slice = Annotated[slice, _Slice]
