from typing import Annotated, Any

from pydantic import GetCoreSchemaHandler
from pydantic_core import core_schema
from tango import AttrWriteType, DevState, EventType


class _DevState:
    @classmethod
    def __get_pydantic_core_schema__(
        cls,
        _source_type: Any,
        _handler: GetCoreSchemaHandler,
    ) -> core_schema.CoreSchema:
        def validate_from_python(value):
            if isinstance(value, DevState):
                return value
            else:
                return DevState.names[value]

        return core_schema.json_or_python_schema(
            json_schema=core_schema.no_info_after_validator_function(
                validate_from_python,
                schema=core_schema.str_schema(),
            ),
            python_schema=core_schema.no_info_after_validator_function(
                validate_from_python,
                schema=core_schema.any_schema(),
            ),
            serialization=core_schema.plain_serializer_function_ser_schema(str),
        )


SchemaDevStateType = Annotated[DevState, _DevState]
SchemaWriteType = Annotated[int, AttrWriteType]
SchemaEventType = Annotated[int, EventType]
