##
# SKA
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.


import argparse
import asyncio
import json
import logging
import traceback
import warnings
from contextlib import ExitStack, suppress
from typing import Annotated, Dict, List

import numpy as np
import ska_sdp_config
import ska_ser_logging
import tango
from deprecated import deprecated
from overrides import override
from tango import ArgType, DevState, GreenMode
from tango.server import Device, device_property, run

from ska_sdp_lmc_queue_connector.configdb_utils import (
    create_flow_state_by_pb_id,
    update_flow_state_by_pb_id,
)
from ska_sdp_lmc_queue_connector.dev_factory import DevFactory
from ska_sdp_lmc_queue_connector.exchange import Exchange
from ska_sdp_lmc_queue_connector.exchange_factory import (
    create_exchange_groups,
    enter_exchange_contexts,
)
from ska_sdp_lmc_queue_connector.sdp_queue_connector_descriptor import (
    FaultModel,
    QueueConnectorDescriptor,
    StateModel,
)
from ska_sdp_lmc_queue_connector.sourcesink import DataType
from ska_sdp_lmc_queue_connector.watcher import create_descriptor_watcher


class SDPQueueConnector(Device):  # type: ignore
    """
    A dynamically configured tango device for sending and receiving
    data to and from data services.
    """

    green_mode = GreenMode.Asyncio

    __initialized: bool = False

    # -----------------
    # Device Properties
    # -----------------

    exchanges_config_path: str = device_property(dtype=ArgType.DevString, default_value="")
    """Configuration Database template parent path for exchange configs.

    Brace templates are substituted with the device subarray_id.

    Examples:
    * /component/lmc-queueconnector-01/owner
    * /component/lmc-queueconnector-{}/owner
    """

    experimental_flow: Annotated[bool, deprecated(version="5.3.0")] = device_property(
        dtype=ArgType.DevBoolean, default_value=True
    )
    """Enable to automatically configure using configuration database flow configs."""

    subarray_id: str = device_property(dtype=ArgType.DevString, default_value="")
    """Subarray ID used when filtering configuration database configs."""

    experimental_kafka: bool = device_property(dtype=ArgType.DevBoolean, default_value=False)
    """Backwards compatibility flag to select Kafka specialized exchanges."""

    # ------------------
    # Dynamic Attributes
    # ------------------

    def generic_read(self, attr: tango.Attr):
        """Generic method for reading the internal device values
        into the output attribute argument.

        Args:
            attr (tango.Attr): output tango attribute
        """
        value = self.__values[attr.get_name()]
        if isinstance(value, tuple):
            attr.set_value(value[0], value[1])
        else:
            attr.set_value(value)

    def set_attr(self, name: str, value: DataType, push_event=True):
        """
        Sets the internal attribute and optionally pushes a change event.

        Args:
            name (str): name of attribute to set
            value (DataType): value to set
            push_event (bool | None, optional): Whether to push an event.
            Setting to None will push if polling is not active. Defaults to
            None.
        """
        # convert any scalar ndarrays to scalar for tango
        if isinstance(value, np.ndarray):
            value = value[()]

        # Using json string to support object types in tango
        if isinstance(value, dict):
            value = json.dumps(value)

        self.__values[name] = value

        if push_event:
            if isinstance(value, tuple):
                self.push_change_event(name, *value)
            else:
                self.push_change_event(name, value)

    def __db_create_group_state(self, pb_id: str | None, state: DevState):
        assert pb_id not in self._group_states
        self._group_states[pb_id] = state
        if self.exchanges_config_path is not None or self.experimental_flow:
            self.logger.debug("creating state %s as %s", pb_id, state)
            create_flow_state_by_pb_id(self.__dbconfig, pb_id, state)
        self.set_state(self.__determine_state())

    def __db_update_group_state(self, pb_id: str | None, state: DevState):
        assert pb_id in self._group_states
        self._group_states[pb_id] = state
        if self.exchanges_config_path is not None or self.experimental_flow:
            self.logger.debug("updating state %s to %s", pb_id, state)
            update_flow_state_by_pb_id(self.__dbconfig, pb_id, state)
        self.set_state(self.__determine_state())

    def __db_delete_group_state(self, pb_id: str | None):
        self._group_states.pop(pb_id)
        if self.exchanges_config_path is not None or self.experimental_flow:
            self.logger.debug("ignoring flow state deletion %s", pb_id)
            # delete_flow_state_by_pb_id(self.__dbconfig, pb_id)
        self.set_state(self.__determine_state())

    def __db_set_group_exception(self, pb_id: str | None, exception: Exception):
        try:
            if self.exchanges_config_path is not None or self.experimental_flow:
                self.logger.info("writing exception for %s", pb_id)
                for txn in self.__dbconfig.txn():
                    for flow_key in txn.flow.list_keys(pb_id=pb_id):
                        exists = txn.flow.state(flow_key).exists()
                        if exists:
                            create_or_update = txn.flow.state(flow_key).update
                        else:
                            create_or_update = txn.flow.state(flow_key).create

                        create_or_update(
                            StateModel(
                                status="FAILED",
                                exception=FaultModel(
                                    message=str(exception),
                                    stacktrace="".join(traceback.format_exception(exception)),
                                ),
                            ).model_dump()
                        )
                        logging.info("added exception state %s", flow_key)
        except Exception:
            logging.exception("Failed to push db exception for %s", pb_id)

    def __determine_state(self):
        """
        Determines the device state by aggregating all group states.
        Backward compatibility for workflows that do not utilize
        exchange groups.
        """
        states = set(self._group_states.values())
        if DevState.FAULT in states:
            return DevState.FAULT
        elif DevState.ON in states:
            return DevState.ON
        elif DevState.CLOSE in states:
            return DevState.CLOSE
        elif DevState.OPEN in states:
            return DevState.OPEN
        elif DevState.OFF in states:
            return DevState.OFF
        return DevState.STANDBY

    # --------
    # Methods
    # --------

    def is_Standby_allowed(self, group_name: str | None = None):
        return group_name in self._group_states and self._group_states[group_name] == DevState.OFF

    async def group_standby(self, group_name: str | None):
        if group_name not in self.__group_exchanges:
            raise ValueError("{group_name} is not a registered exchange group")

        # Remove attributes on device
        self.__group_contexts.pop(group_name).__exit__(None, None, None)
        # Remove exchanges
        self.__group_exchanges.pop(group_name)
        # Remove state
        self.__db_delete_group_state(group_name)
        self.set_state(self.__determine_state())

    def is_Abort_allowed(self, group_name: str | None = None):
        return group_name in self._group_states and self._group_states[group_name] in (
            DevState.OPEN,
            DevState.CLOSE,
        )

    async def group_abort(self, group_name: str | None):
        stream_task = self.__group_stream_tasks.get(group_name, None)
        if stream_task is not None:
            if not stream_task.done():
                stream_task.cancel()
            with suppress(asyncio.CancelledError):
                await stream_task

        self.__db_delete_group_state(group_name)

    def is_Reset_allowed(self, group_name: str | None = None):
        return (
            group_name in self._group_states and self._group_states[group_name] == DevState.FAULT
        )

    async def start_db_monitor(self):
        """
        Starts the database monitoring in a background task.
        """
        # Restart the monitoring task
        if self.db_monitor_task is not None:
            self.logger.info(
                "restarting monitor for %s", self.exchanges_config_path.format(self.subarray_id)
            )
            self.db_monitor_task.cancel()
            with suppress(asyncio.CancelledError):
                await self.db_monitor_task
        self.db_monitor_task = asyncio.create_task(self.watch_database_and_reconfigure())

    async def group_reset(self, group_name: str | None):
        stream_task = self.__group_stream_tasks[group_name]
        if not stream_task.done():
            stream_task.cancel()
        try:
            with suppress(asyncio.CancelledError):
                await stream_task
        except Exception:
            self.logger.exception("Reset retreived an exception")
        self.__db_update_group_state(group_name, DevState.OFF)

    def is_Configure_allowed(self, group_name: str | None = None):
        return group_name not in self._group_states

    async def group_configure(self, configuration: str, group_name_override: str | None = None):
        """Commands the device to load exchanges using a provided config
        json string.

        Args:
            configuration (str | None): QueueConnectorDescriptor json. None
            value is considered as an empty config.
            group_name_override (str | None): When provided, overrides the
            group name inside the QueueConnectorDescriptor.
        """
        # Load configuration document
        descriptor = QueueConnectorDescriptor.model_validate_json(configuration)

        group_keys: list[str | None] = [None]
        if isinstance(descriptor.exchanges, dict):
            group_keys = list(descriptor.exchanges.keys())

        if group_name_override is not None:
            group_keys = [group_name_override]
            self._register_exchanges(descriptor, group_name_override)
        else:
            self._register_exchanges(descriptor)

        for group_key in group_keys:
            if group_key in self.__group_exchanges and len(self.__group_exchanges[group_key]) != 0:
                self.__db_create_group_state(group_key, DevState.OFF)
        self.set_state(self.__determine_state())

    async def watch_database_and_reconfigure(self):
        """Long running awaitable that watches the configuration database
        and automatically tries reconfiguring and the device.
        """
        if self.experimental_flow:
            self.logger.info("Device started watching flows")
        else:
            self.logger.info("Device started watching descriptors")
            assert self.exchanges_config_path.startswith("/")

        if not self.subarray_id:
            logging.warning("queue connector not using subarray_id")
        async with create_descriptor_watcher(
            self.__dbconfig,
            subarray_id=self.subarray_id if self.subarray_id else None,
            use_flow=self.experimental_flow,
            descriptor_path=self.exchanges_config_path,
            use_kafka=self.experimental_kafka,
        ) as watcher:
            async for watch_key, config_value in watcher:
                try:
                    if self.experimental_flow:
                        group_name = watch_key  # pb_id
                    else:
                        warnings.warn(
                            DeprecationWarning("watching component config is deprecated")
                        )
                        group_name = watch_key[
                            slice(
                                1
                                + len(self.exchanges_config_path)
                                + watch_key.find(self.exchanges_config_path),
                                None,
                            )
                        ]

                    if isinstance(config_value, Exception):
                        raise config_value
                    if config_value is not None:
                        self.logger.info("config updated for %s", watch_key)
                    else:  # config deleted
                        self.logger.info("config deleted for %s", watch_key)

                    await self.reconfigure(group_name, config_value)
                except Exception as e:
                    self.logger.exception("reconfigure loop encountered an error")
                    self.__db_set_group_exception(group_name, e)
                    if group_name in self._group_states:
                        self.__db_update_group_state(group_name, DevState.FAULT)

    async def reconfigure(self, group_name: str, config_value: str | None):
        """Tries commanding to stop, reconfigure and
        start the device depending on the current state.

        Args:
            config (str | None): the new configuration json string.
        """
        if self.is_Reset_allowed(group_name):
            await self.group_reset(group_name)
        if self.is_Stop_allowed(group_name):
            await self.group_stop(group_name)
        if self.is_Standby_allowed(group_name):
            await self.group_standby(group_name)
        if config_value is not None and self.is_Configure_allowed(group_name):
            await self.group_configure(config_value, group_name)
        if self.is_Start_allowed(group_name):
            await self.group_start(group_name)

    def is_Start_allowed(self, group_name: str | None = None):
        return group_name in self._group_states and self._group_states[group_name] == DevState.OFF

    async def group_start(self, group_name: str | None):
        # NOTE: tango subscribing in a non OFF state will trigger
        # an initial attribute event
        self.__db_update_group_state(group_name, DevState.OPEN)

        await asyncio.gather(
            *[exchange.start() for exchange in self.__group_exchanges[group_name]]
        )

        self.__group_stream_tasks.update(
            {group_name: asyncio.create_task(self._stream(group_name))}
        )
        self.__db_update_group_state(group_name, DevState.ON)

    def is_Stop_allowed(self, group_name: str | None = None):
        return group_name in self._group_states and self._group_states[group_name] == DevState.ON

    async def group_stop(self, group_name: str | None):
        self.__db_update_group_state(group_name, DevState.CLOSE)
        await asyncio.gather(*[exchange.stop() for exchange in self.__group_exchanges[group_name]])

        # stop should set condition for exchanges to finish
        stream_task = self.__group_stream_tasks[group_name]
        assert stream_task is not None
        await stream_task

        self.__db_update_group_state(group_name, DevState.OFF)

    # ---------------
    # General methods
    # ---------------

    @override
    def set_state(self, state):
        super().set_state(state)
        self.push_change_event("state", state)

    @override
    async def init_device(self):  # pylint: disable=invalid-overridden-method
        await super().init_device()  # type: ignore
        if not self.__initialized:
            self.set_state(DevState.INIT)
            self.logger = logging.getLogger(__name__)
            self.__group_exchanges: Dict[str | None, List[Exchange]] = {}
            self._group_states: Dict[str | None, DevState] = {}
            self.__group_contexts: Dict[str | None, ExitStack] = {}
            self.__group_stream_tasks: Dict[str | None, asyncio.Task] = {}

            self.__values: Dict[str, DataType] = {}
            self._dev_factory = DevFactory(green_mode=self.green_mode)
            self.__dbconfig = ska_sdp_config.Config()
            self.db_monitor_task = None
            self.set_change_event("state", True, False)

            # Configure automatically if config properties are set
            try:
                self.set_state(DevState.STANDBY)
                await self._configure_from_property()
            except Exception:
                self.set_state(DevState.FAULT)
                self.logger.exception("Failed to configure device")

            if self.exchanges_config_path is not None:
                warnings.warn(PendingDeprecationWarning("exchanges_config_path"))

            self.__initialized = True

    async def _configure_from_property(self):
        if self.exchanges_config_path or self.experimental_flow:
            await self.start_db_monitor()

    @override
    async def delete_device(self):  # pylint: disable=invalid-overridden-method
        # cancel streaming if still active
        for stream_task in self.__group_stream_tasks.values():
            if not stream_task.done():
                stream_task.cancel()
                with suppress(asyncio.CancelledError):
                    await stream_task

        # cancel monitoring task if monitoring is active
        if self.db_monitor_task and not self.db_monitor_task.done():
            self.db_monitor_task.cancel()
            with suppress(asyncio.CancelledError):
                await self.db_monitor_task

        self.__dbconfig.close()
        super().delete_device()

    async def _stream(self, group_name: str | None):
        tasks = [
            asyncio.create_task(exchange.run()) for exchange in self.__group_exchanges[group_name]
        ]
        try:
            await asyncio.gather(*tasks)
        except Exception:
            self.__db_update_group_state(group_name, DevState.FAULT)
            self.logger.exception("Error during streaming")
            raise

    def _register_exchanges(
        self,
        descriptor: QueueConnectorDescriptor,
        group_name_override: str | None = None,
    ):
        if descriptor.exchanges is not None:
            if isinstance(descriptor.exchanges, dict):
                for group_name in descriptor.exchanges.keys():
                    if group_name_override is not None and group_name_override != group_name:
                        logging.warning(
                            "overriding config group name: %s -> %s",
                            group_name,
                            group_name_override,
                        )

            # NOTE: backwards compatibility with 2.0 descriptors
            if isinstance(descriptor.exchanges, list):
                exchange_desc_groups = {group_name_override: descriptor.exchanges}
            else:
                exchange_desc_groups = descriptor.exchanges

            group_exchanges = create_exchange_groups(self, exchange_desc_groups)
            group_contexts = enter_exchange_contexts(group_exchanges)
            self.__group_exchanges.update(group_exchanges)
            self.__group_contexts.update(group_contexts)


# ----------
# Run server
# ----------

_TANGO_TO_PYTHON = {
    tango.LogLevel.LOG_FATAL: logging.CRITICAL,
    tango.LogLevel.LOG_ERROR: logging.ERROR,
    tango.LogLevel.LOG_WARN: logging.WARNING,
    tango.LogLevel.LOG_INFO: logging.INFO,
    tango.LogLevel.LOG_DEBUG: logging.DEBUG,
    tango.LogLevel.LOG_OFF: logging.NOTSET,
}


def main():
    """Main function of the SDPQueueConnector module."""

    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument("-v", dest="verbose", nargs="?", type=int)
    log_level = parser.parse_known_args()[0].verbose
    ska_ser_logging.configure_logging(level=_TANGO_TO_PYTHON.get(log_level, logging.INFO))
    run(classes=(SDPQueueConnector,))


if __name__ == "__main__":
    main()
