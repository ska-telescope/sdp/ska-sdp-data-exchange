from __future__ import annotations

from ska_sdp_lmc_queue_connector.exchange import ExchangeDescriptor
from ska_sdp_lmc_queue_connector.pydantic import SkaBaseModel


class QueueConnectorDescriptor(SkaBaseModel):
    """
    Primary JSON serializable descriptor for configuring a queue connector device.
    """

    exchanges: dict[str | None, list[ExchangeDescriptor]] | list[ExchangeDescriptor] | None = None
    """Mapping or list of exchanges for a queue connector to process when running."""


class StateModel(SkaBaseModel):
    """
    JSON serializable model that describes an exchange group state within the
    configuration database.
    """

    status: str
    exception: FaultModel | None = None


class FaultModel(SkaBaseModel):
    """
    JSON serializable model that provides details for an exchange group fault within
    the configuration database.
    """

    message: str
    stacktrace: str
