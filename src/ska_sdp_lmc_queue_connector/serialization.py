"""Serialization module"""
import ast
import json
import logging
from io import BytesIO
from json import JSONEncoder
from typing import Callable, Literal

import msgpack
import msgpack_numpy
import numpy as np
from overrides import override

from ska_sdp_lmc_queue_connector.sourcesink import DataType

logger = logging.getLogger(__name__)


Format = Literal["utf-8", "ascii", "python", "json", "msgpack_numpy", "npy", "carray"]
"""
Supported serialized format of structured data.

* utf-8: string data format using python __str__ method encoded as utf-8
* ascii: string data format using python __str__ method encoded as ascii
* python: python literal from python __repr__ method encoded as utf-8
* json: json format using utf-8 format
* msgpack_numpy: binary format for JSON-like data combined with numpy support
* npy: binary format for numpy arrays containing type and shape
* carray: raw data buffer in C/row major order
"""


def create_serializer(
    dtype: np.dtype, shape: list[int], data_format: str
) -> Callable[[DataType], bytes | memoryview]:
    """Returns a serialization function using predetermined
    data type, shape and format.

    Args:
        dtype (np.dtype): dtype of function data
        shape (list[int]): shape of function data
        data_format (str): format for output bytes representation

    Raises:
        ValueError: Unsupported combination of arguments

    Returns:
        Callable[[DataType], bytes | memoryview]: Specialized function for
        serializing data of the specified dtype, shape and format
    """

    def serialize_bytes(value: bytes):
        assert value[0] == data_format
        return value[1]

    def serialize_text(value: DataType):
        return str(value).encode(data_format)

    if dtype == np.bytes_:  # bytes tuple
        if shape != []:
            raise ValueError("unsupported bytes shape", shape)
        serializer = serialize_bytes
    else:  # strings, dicts, primitives
        match data_format:
            case "carray":
                if dtype in (np.str_, np.object_):
                    raise ValueError("carray does not support str or dict types")
                serializer = _serialize_carray
            case "npy":
                serializer = _serialize_npy
            case "json":
                serializer = _serialize_json
            case "msgpack_numpy" | "xarray":
                packer = msgpack.Packer(default=msgpack_numpy.encode)
                serializer = packer.pack
            case "python":
                serializer = _serialize_python
            case "utf-8" | "ascii":
                serializer = serialize_text
            case _:
                raise ValueError("unsupported data_format", data_format)
    return serializer


class NDArrayEncoder(JSONEncoder):
    """
    A JSON encoder extension for detecting and converting numpy
    primitives and arrays to JSON types.
    """

    @override
    def default(self, o):
        if isinstance(o, np.ndarray):
            return o.tolist()
        if np.issubdtype(o, np.integer):
            return int(o)
        if np.issubdtype(o, np.floating):
            return float(o)
        return JSONEncoder.default(self, o)


def _serialize_carray(value: np.ndarray) -> memoryview:
    return value.data


def _serialize_npy(value: DataType) -> memoryview:
    bio = BytesIO()
    np.save(bio, value)
    return bio.getbuffer()


def _serialize_json(value: DataType) -> bytes:
    return json.dumps(value, cls=NDArrayEncoder).encode("utf-8")


def _serialize_python(value: DataType) -> bytes:
    return repr(value).encode("utf-8")


def create_deserializer(
    dtype: np.dtype, shape: list[int], data_format: str
) -> Callable[[bytes], DataType]:
    """Returns a deserialization function using predetermined
    data type, shape and format.

    Args:
        dtype (np.dtype): dtype of function data
        shape (list[int]): shape of function data
        data_format (str): format for output bytes representation

    Raises:
        ValueError: Unsupported combination of arguments

    Returns:
        Callable[[DataType], bytes | memoryview]: Specialized function for
        deserializing data of the specified dtype, shape and format
    """

    def deserialize_bytes(value):
        return (data_format, value)

    def deserialize_carray(value):
        return np.frombuffer(value, dtype=dtype).reshape(shape)

    def deserialize_npy(value):
        return np.load(BytesIO(value))

    def deserialize_json(value):
        return np.array(json.loads(value), dtype=dtype)

    def deserialize_msgpack_numpy(value):
        return np.array(
            msgpack.unpackb(value, object_hook=msgpack_numpy.decode),
            dtype=dtype,
        )

    def deserialize_python(value):
        return np.array(ast.literal_eval(value.decode("utf-8")), dtype=dtype)

    def deserialize_text(value):
        return np.array(value.decode(data_format), dtype=dtype)

    if dtype == np.bytes_:  # tango bytes tuple
        if shape != []:
            raise ValueError("unsupported bytes shape", shape)
        deserializer = deserialize_bytes
    else:  # strings, dicts, primitives
        match data_format:
            case "carray":
                if dtype in (np.str_, np.object_):
                    raise ValueError("carray does not support str or dict types")
                deserializer = deserialize_carray
            case "npy":
                deserializer = deserialize_npy
            case "json":
                deserializer = deserialize_json
            case "msgpack_numpy" | "xarray":
                deserializer = deserialize_msgpack_numpy
            case "python":
                deserializer = deserialize_python
            case "utf-8" | "ascii":
                deserializer = deserialize_text
            case _:
                raise ValueError(f"unknown format {data_format} for dtype {dtype}")

    return deserializer
