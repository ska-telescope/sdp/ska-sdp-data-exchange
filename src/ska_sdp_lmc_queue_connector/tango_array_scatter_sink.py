from __future__ import annotations

import warnings
from contextlib import AbstractContextManager
from typing import TYPE_CHECKING

from ska_sdp_lmc_queue_connector.tango_dtype_helper import dtype_to_tango

if TYPE_CHECKING:
    from ska_sdp_lmc_queue_connector.sdp_queue_connector import (
        SDPQueueConnector,
    )

import logging
from typing import Literal

import numpy as np
from overrides import override
from tango import AttrDataFormat, AttrWriteType
from tango.server import attribute

from ska_sdp_lmc_queue_connector.pydantic import SkaBaseModel
from ska_sdp_lmc_queue_connector.sourcesink import DataSink, DataType, SchemaDataType

logger = logging.getLogger(__name__)


class TangoArrayScatterAttributeSinkDescriptor(SkaBaseModel):
    """
    A Tango Attribute Sink for splitting and scattering of ndarray data.
    """

    type: Literal[
        "TangoArrayScatterAttributeSink", "TangoSplitAttributeSink"
    ] = "TangoArrayScatterAttributeSink"
    attribute_names: list[str]
    """Ordered attributute names to dynamically add the SDPQueueConnector"""
    axis: int = 0
    """Axis to split on. Non-zero values may reduce performance."""
    default_value: SchemaDataType = ""
    """Starting value of the dynamic attribute before a source is read"""
    attribute_shape_names: list[str] | None = None
    """Optional attribute shapes names"""
    indices: list[int] | None = None
    """Optional ordered indexes of the split boundaries"""

    def __init__(self, **data):
        if data.get("type", "") == "TangoSplitAttributeSink":
            warnings.warn(
                "TangoSplitAttributeSink deprecated, use TangoArrayScatterAttributeSink",
                category=DeprecationWarning,
            )
        super().__init__(**data)


class TangoArrayScatterAttributeSink(DataSink, AbstractContextManager):
    def __init__(
        self,
        desc: TangoArrayScatterAttributeSinkDescriptor,
        device: SDPQueueConnector,
        dtype: np.dtype,
        shape: list[int],
    ):
        """
        A Tango Attribute sink capable of scattering a tensor to multiple tango
        attributes.
        """

        # Input Validation
        assert len(shape) > 0, "Tango split attribute only supports array data"
        if desc.attribute_shape_names:
            assert len(desc.attribute_names) == len(desc.attribute_shape_names)
            attribute_shape_names = desc.attribute_shape_names
        else:
            attribute_shape_names = []

        # Members
        self.__started = False
        self.__device = device
        self.__attribute_names = desc.attribute_names
        self.__indices = desc.indices if desc.indices else len(desc.attribute_names)
        self.__tensor_shape = shape
        self.__tensor_axis = desc.axis

        # Construction Variables
        split_defaults = np.array_split(
            np.full(
                self.__tensor_shape,
                desc.default_value,
                dtype=dtype if dtype != np.str_ else None,
            ),
            self.__indices,
            self.__tensor_axis,
        )
        split_shapes = [v.shape for v in split_defaults]
        split_defaults = [v.flatten() for v in split_defaults]
        split_flat_shapes = [np.array(v.shape) for v in split_defaults]

        match len(shape):
            case 0:
                tdformat = AttrDataFormat.SCALAR
            case 1:
                tdformat = AttrDataFormat.SPECTRUM
            case 2:
                tdformat = AttrDataFormat.IMAGE
            case _:
                tdformat = AttrDataFormat.SPECTRUM

        # Data Attributes
        self.__attrs = []
        for index, attr_name in enumerate(desc.attribute_names):
            self.__device.set_attr(attr_name, split_defaults[index], False)
            self.__attrs.append(
                attribute(
                    name=attr_name,
                    dtype=dtype_to_tango(dtype),
                    dformat=tdformat,
                    access=AttrWriteType.READ,
                    max_dim_x=split_flat_shapes[index][0]
                    if len(split_flat_shapes[index]) > 0
                    else 0,
                    max_dim_y=split_flat_shapes[index][1]
                    if len(split_flat_shapes[index]) > 1
                    else 0,
                    fread=device.generic_read,
                )
            )

        # Shape Attributes
        for index, attr_shape_name in enumerate(attribute_shape_names):
            self.__device.set_attr(attr_shape_name, np.array(split_shapes[index]), False)
            self.__attrs.append(
                attribute(
                    name=attr_shape_name,
                    dtype=np.uint32,
                    dformat=tdformat,
                    access=AttrWriteType.READ,
                    max_dim_x=len(split_shapes[index]),
                    fread=device.generic_read,
                )
            )

    @override
    def __enter__(self):
        for attr in self.__attrs:
            self.__device.add_attribute(attr)
            self.__device.set_change_event(attr.name, True, False)
        return self

    @override
    def __exit__(self, exc, value, tb):
        for attr in self.__attrs:
            self.__device.remove_attribute(attr.name)

    @override
    async def start(self):
        self.__started = True

    @override
    async def stop(self):
        pass

    @override
    async def awrite(self, value: DataType):
        assert isinstance(value, np.ndarray)
        if self.__started:
            tensor = value.reshape(self.__tensor_shape)
            arrays = np.array_split(tensor, self.__indices, axis=self.__tensor_axis)
            for index, array in enumerate(arrays):
                self.__device.set_attr(
                    self.__attribute_names[index],
                    array if len(self.__tensor_shape) < 3 else array.flatten(),
                )
