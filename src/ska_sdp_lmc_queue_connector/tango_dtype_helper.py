import warnings

import numpy as np
from tango import AttrDataFormat, CmdArgType

from ska_sdp_lmc_queue_connector.numpy_helper import create_numpy_scalar

# Make sure to keep any changes to this dict consistent with
# `data_types_and_shapes.rst`
_DTYPE_TO_TANGO = dict[np.dtype, CmdArgType](
    {
        np.dtype(np.bytes_): CmdArgType.DevEncoded,
        np.dtype(np.object_): CmdArgType.DevString,
        np.dtype(np.str_): CmdArgType.DevString,
        np.dtype(np.bool_): CmdArgType.DevBoolean,
        np.dtype(np.uint8): CmdArgType.DevUChar,
        np.dtype(np.uint16): CmdArgType.DevUShort,
        np.dtype(np.uint32): CmdArgType.DevULong,
        np.dtype(np.uint64): CmdArgType.DevULong64,
        np.dtype(np.int16): CmdArgType.DevShort,
        np.dtype(np.int32): CmdArgType.DevLong,
        np.dtype(np.int64): CmdArgType.DevLong64,
        np.dtype(np.float32): CmdArgType.DevFloat,
        np.dtype(np.float64): CmdArgType.DevDouble,
    }
)

_TANGO_TO_DTYPE = dict[CmdArgType, np.dtype](
    {
        CmdArgType.DevEncoded: np.dtype(np.bytes_),
        CmdArgType.DevString: np.dtype(np.str_),
        CmdArgType.DevBoolean: np.dtype(np.bool_),
        CmdArgType.DevUChar: np.dtype(np.uint8),
        CmdArgType.DevUShort: np.dtype(np.uint16),
        CmdArgType.DevULong: np.dtype(np.uint32),
        CmdArgType.DevULong64: np.dtype(np.uint64),
        CmdArgType.DevShort: np.dtype(np.int16),
        CmdArgType.DevLong: np.dtype(np.int32),
        CmdArgType.DevLong64: np.dtype(np.int64),
        CmdArgType.DevFloat: np.dtype(np.float32),
        CmdArgType.DevDouble: np.dtype(np.float64),
    }
)

_TANGO_TO_DTYPES = dict[CmdArgType, set[np.dtype]](
    {
        CmdArgType.DevEncoded: {np.dtype(np.bytes_)},
        CmdArgType.DevString: {np.dtype(np.str_), np.dtype(np.object_)},
        CmdArgType.DevBoolean: {np.dtype(np.bool_)},
        CmdArgType.DevUChar: {np.dtype(np.uint8)},
        CmdArgType.DevUShort: {np.dtype(np.uint16)},
        CmdArgType.DevULong: {np.dtype(np.uint32)},
        CmdArgType.DevULong64: {np.dtype(np.uint64)},
        CmdArgType.DevShort: {np.dtype(np.int16)},
        CmdArgType.DevLong: {np.dtype(np.int32)},
        CmdArgType.DevLong64: {np.dtype(np.int64)},
        CmdArgType.DevFloat: {np.dtype(np.float32)},
        CmdArgType.DevDouble: {np.dtype(np.float64)},
    }
)


def dtype_to_tango(dtype: np.dtype) -> CmdArgType:
    """Converts a numpy datatype to the respective tango type

    Returns:
        type: type that is a subdtype of tango.CmdArgType
    """
    return _DTYPE_TO_TANGO[dtype]


def tango_to_dtype(argtype: CmdArgType) -> np.dtype:
    """Converts tango datatype to a numpy dtype

    Returns:
        type: type that is a subdtype of numpy.generic
    """
    return _TANGO_TO_DTYPE[argtype]


def tango_to_dtypes(argtype: CmdArgType) -> set[np.dtype]:
    """Converts tango datatype to a set of compatible numpy dtypes

    Args:
        argtype (CmdArgType): _description_

    Returns:
        set[np.dtype]: set of types that are subset of numpy.generic
    """
    warnings.warn("Deprecated, avoid using np.object_ with pytango", DeprecationWarning)
    return _TANGO_TO_DTYPES[argtype]


def tango_format_to_shape(dformat: AttrDataFormat, xdim: int, ydim: int) -> list[int]:
    """
    Converts a tango dformat with dimensions to a numpy C order shape.
    """
    match dformat:
        case AttrDataFormat.SCALAR:
            return []
        case AttrDataFormat.SPECTRUM:
            return [xdim]
        case AttrDataFormat.IMAGE:
            # Tango internal dimensions uses F order instead of the numpy
            # default C order used by pytango attribute proxies.
            return [ydim, xdim]
        case _:
            raise ValueError("invalid format", dformat)


def cast_tango_default_value(value, dtype: np.dtype, shape: list[int]):
    """
    Converter for Pydantic model default value to tango
    compatible value.
    """
    if dtype == np.bytes_:
        # Encoded value (strict format)
        assert len(shape) == 0, "encoded attribute must be scalar"
        default_value = value
    elif len(shape) == 0:
        # Scalar value
        default_value = create_numpy_scalar(value, dtype)
    else:
        # NDArray value
        default_value = np.full(
            shape,
            value,
            # NOTE: np.str_ not compatible with np.full()
            dtype=dtype if dtype != np.str_ else None,
        )
    return default_value
