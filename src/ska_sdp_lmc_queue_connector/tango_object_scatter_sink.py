from __future__ import annotations

import logging
from contextlib import AbstractContextManager
from typing import TYPE_CHECKING, Literal

import jmespath
import numpy as np
from overrides import override
from tango import AttrDataFormat, AttrWriteType
from tango.server import attribute as Attribute

from ska_sdp_lmc_queue_connector.pydantic import DType, SkaBaseModel
from ska_sdp_lmc_queue_connector.sourcesink import DataSink, DataType, SchemaDataType
from ska_sdp_lmc_queue_connector.tango_dtype_helper import cast_tango_default_value, dtype_to_tango

if TYPE_CHECKING:
    from ska_sdp_lmc_queue_connector.sdp_queue_connector import SDPQueueConnector

logger = logging.getLogger(__name__)


class TangoAttributeDescriptor(SkaBaseModel):
    """
    An attribute descriptor describing a filter and path to
    to a value in JSON.
    """

    attribute_name: str
    """Attribute name to dynamically add to the SDPQueueConnector"""
    dtype: DType
    """Python primitive, numpy dtype or tango dtype of the dynamic attribute"""
    shape: list[int] = []
    """Maximum shape of the dynamic attribute"""
    path: str = "@"
    """JMESPath expression for the location of the data to extract"""
    filter: str | None = None
    """JMESPath predicate expression for whether to update the attribute.

    For testing on https://play.jmespath.org, the deserialized message
    can be converted to the equivalent JSON using the following:

    >>> import json, numpy
    >>> class NumpyArrayJSONEncoder(json.JSONEncoder):
    ...     def default(self, obj):
    ...         if isinstance(obj, numpy.ndarray):
    ...             return obj.tolist()
    ...         return json.JSONEncoder.default(self, obj)
    ...
    >>> example = numpy.array([("a", 1)], dtype=[("name", object),("value", int)])
    >>> json.dumps(example, cls=NumpyArrayJSONEncoder)
    '[["a", 1]]'
    """
    default_value: SchemaDataType = ""
    """Starting value of the dynamic attribute before a source is read"""


class TangoObjectScatterAttributeSinkDescriptor(SkaBaseModel):
    """
    A Tango Attribute Sink for splitting and scattering object heirarchy
    data.
    """

    type: Literal[
        "TangoObjectScatterAttributeSink",
    ] = "TangoObjectScatterAttributeSink"
    attributes: list[TangoAttributeDescriptor]
    """Attribute names to dynamically add the SDPQueueConnector"""


class TangoObjectScatterAttributeSink(DataSink, AbstractContextManager):
    def __init__(
        self,
        desc: TangoObjectScatterAttributeSinkDescriptor,
        device: SDPQueueConnector,
        # pylint: disable-next=unused-argument
        dtype: np.dtype,
        # pylint: disable-next=unused-argument
        shape: list[int],
    ):
        self.__device = device
        self.__attrs = []
        self.__attributes = [_JSONTangoAttribute(att) for att in desc.attributes]

        for attr_desc in desc.attributes:
            attr_dtype = attr_desc.dtype
            default_value = cast_tango_default_value(
                attr_desc.default_value, attr_dtype, attr_desc.shape
            )
            self.__device.set_attr(attr_desc.attribute_name, default_value, False)
            n_dims = len(attr_desc.shape)
            match n_dims:
                case 0:
                    tdformat = AttrDataFormat.SCALAR
                case 1:
                    tdformat = AttrDataFormat.SPECTRUM
                case 2:
                    tdformat = AttrDataFormat.IMAGE
                case _:
                    raise ValueError("invalid attribute shape", shape)

            self.__attrs.append(
                Attribute(
                    name=attr_desc.attribute_name,
                    dtype=dtype_to_tango(attr_dtype),
                    dformat=tdformat,
                    access=AttrWriteType.READ,
                    # NOTE: Tango internal dimensions uses F order instead of the numpy
                    # default C order used by pytango attribute proxies.
                    max_dim_x=attr_desc.shape[-1] if n_dims > 0 else 0,
                    max_dim_y=attr_desc.shape[-2] if n_dims > 1 else 0,
                    fread=device.generic_read,
                )
            )

    @override
    def __enter__(self):
        for attr in self.__attrs:
            self.__device.add_attribute(attr)
            self.__device.set_change_event(attr.name, True, False)
        return self

    @override
    def __exit__(self, exc, value, tb):
        for attr in self.__attrs:
            self.__device.remove_attribute(attr.name)

    @override
    async def start(self):
        self.__started = True

    @override
    async def stop(self):
        pass

    @override
    async def awrite(self, value: DataType):
        if isinstance(value, np.ndarray):
            # convert to scalar when possible
            value = value[()]
        if self.__started:
            logging.debug("scattering %s", value)
            for attribute in self.__attributes:
                if attribute.filter is None or attribute.filter.search(value):
                    self.__device.set_attr(attribute.name, attribute.path.search(value))


class _JSONTangoAttribute:
    """Contains cached JSON path information for a given Tango attribute"""

    def __init__(self, desc: TangoAttributeDescriptor):
        self.name = desc.attribute_name
        self.filter = jmespath.compile(desc.filter) if desc.filter else None
        self.path = jmespath.compile(desc.path)
