from __future__ import annotations

from typing import TYPE_CHECKING, Literal

import numpy as np
from astropy.time import Time, TimeDelta
from overrides import override
from tango import AttributeInfoEx

from ska_sdp_lmc_queue_connector.sourcesink import DataType
from ska_sdp_lmc_queue_connector.tango_dtype_helper import tango_format_to_shape
from ska_sdp_lmc_queue_connector.tango_sourcesink import (
    TangoSubscriptionSource,
    TangoSubscriptionSourceDescriptor,
)

if TYPE_CHECKING:
    from ska_sdp_lmc_queue_connector.sdp_queue_connector import SDPQueueConnector


class TangoPointingSubscriptionSourceDescriptor(TangoSubscriptionSourceDescriptor):
    type: Literal["TangoPointingSubscriptionSource"] = "TangoPointingSubscriptionSource"
    antenna_name: str | None = None
    """Antenna name to prepend to the data."""


def get_astropy_time(value: float) -> Time:
    """Get the astropy Time from an SKAO seconds since Y2000 timestamp."""
    return TangoPointingSubscriptionSource.INPUT_EPOCH + TimeDelta(
        value,
        format="sec",
    )


class TangoPointingSubscriptionSource(TangoSubscriptionSource):
    """
    A specialized tango attribute source for converting
    pointings to a structured type.
    """

    INPUT_EPOCH = Time("2000-01-01", format="isot", scale="tai")

    def __init__(
        self,
        desc: TangoPointingSubscriptionSourceDescriptor,
        device: SDPQueueConnector,
        dtype: np.dtype,
        shape: list[int],
    ):
        if desc.antenna_name is not None:
            self.__antenna_name = desc.antenna_name
        else:
            self.__antenna_name = desc.device_name.rsplit("/")[-1]
        super().__init__(desc, device, dtype, shape)

    @override
    def validate(self, attr: AttributeInfoEx):
        assert np.dtype(self._dtype).names is not None
        # last dimension of will be reduced due to converting a dimension
        # into a structure.
        output_shape = [-1] + tango_format_to_shape(
            attr.data_format, attr.max_dim_x, attr.max_dim_y
        )[:-1]
        if self._shape != output_shape:
            raise RuntimeError(
                f"configured shape: {self._shape} != tango attribute shape: {output_shape}"
            )

    @override
    async def __anext__(self) -> DataType:
        spectrum: np.ndarray = await super().__anext__()  # type: ignore
        dish_pointing_table = spectrum.reshape(-1, 3)
        return np.array(
            [
                (
                    self.__antenna_name,
                    get_astropy_time(dish_pointing[0]).isot,
                    dish_pointing[1],
                    dish_pointing[2],
                )
                for dish_pointing in dish_pointing_table
            ],
            dtype=self._dtype,
        )
