from __future__ import annotations

import asyncio
import logging
from contextlib import AbstractContextManager
from typing import TYPE_CHECKING, Literal

import numpy as np
from overrides import override
from tango import AttrDataFormat, AttributeInfoEx, AttrWriteType, DeviceProxy, DevState, EventType
from tango.server import attribute

from ska_sdp_lmc_queue_connector.pydantic import SchemaEventType, SkaBaseModel
from ska_sdp_lmc_queue_connector.sourcesink import DataSink, DataSource, DataType, SchemaDataType
from ska_sdp_lmc_queue_connector.tango_dtype_helper import (
    cast_tango_default_value,
    dtype_to_tango,
    tango_format_to_shape,
    tango_to_dtypes,
)

if TYPE_CHECKING:
    from ska_sdp_lmc_queue_connector.sdp_queue_connector import SDPQueueConnector

logger = logging.getLogger(__name__)


class TangoSubscriptionSourceDescriptor(SkaBaseModel):
    type: Literal["TangoSubscriptionSource"] = "TangoSubscriptionSource"
    device_name: str
    """Device name containing the attribute the subscription is to."""
    attribute_name: str
    """Attribute name the subscription is to."""
    etype: SchemaEventType = 0
    """The type of attribute event to listen for."""
    stateless: bool = True
    """
    When True will retry subscribing every 10 seconds if failed subscription,
    otherwise will raise an exception on start if False.
    """


class TangoSubscriptionSource(DataSource):
    """
    A DataSource populated from a subscription to tango attribute events.
    """

    def __init__(
        self,
        desc: TangoSubscriptionSourceDescriptor,
        device: SDPQueueConnector,
        dtype: np.dtype,
        shape: list[int],
    ):
        self.__desc = desc
        self.__device = device
        self._dtype = dtype
        self._shape = shape
        self.__event_id: int | None = None
        self.__event_queue = asyncio.Queue()

    @override
    async def start(self):
        self.__proxy: DeviceProxy = await self.__device._dev_factory.get_async_proxy(
            self.__desc.device_name
        )

        # Check that proxy attribute is what the descriptor describes
        attr = (await self.__proxy.get_attribute_config_ex(self.__desc.attribute_name))[0]
        self.validate(attr)

        self.__event_id = await self.__proxy.subscribe_event(
            self.__desc.attribute_name,
            EventType(self.__desc.etype),
            self.handle_event,
            stateless=self.__desc.stateless,
        )

    async def handle_event(self, event):
        try:  # NOTE: exceptions here are otherwise silent
            if event.err:
                error = event.errors[0]
                logging.info("%s %s", error.reason, error.desc)
                logging.error("%s", error.origin)
                return

            if (
                self.__device.get_state() != DevState.OFF
                and TangoSubscriptionSource.is_valid_event(event)
            ):
                await self.__event_queue.put(event.attr_value.value)
        except Exception:
            logging.exception("uncaught tango subscription source error")

    def validate(self, attr: AttributeInfoEx):
        assert self._dtype in tango_to_dtypes(
            attr.data_type
        ), f"{self._dtype} not in {tango_to_dtypes(attr.data_type)}"
        assert (
            tango_format_to_shape(attr.data_format, attr.max_dim_x, attr.max_dim_y) == self._shape
        )

    @override
    async def stop(self):
        await self.__proxy.unsubscribe_event(self.__event_id)
        await self.__event_queue.put(None)

    @override
    async def __anext__(self) -> DataType:
        if self.__event_id:
            v = await self.__event_queue.get()
            if v is not None:
                return v
        raise StopAsyncIteration

    @staticmethod
    def is_valid_event(event):
        return event and event.attr_value and event.attr_value.value is not None


class TangoLocalAttributeSinkDescriptor(SkaBaseModel):
    type: Literal["TangoLocalAttributeSink"] = "TangoLocalAttributeSink"
    attribute_name: str
    """Attribute name to dynamically add to the SDPQueueConnector"""
    default_value: SchemaDataType = ""
    """Starting value of the dynamic attribute before a source is read"""


class TangoLocalAttributeSink(DataSink, AbstractContextManager):
    """
    A DataSink that publishes data to a tango attribute on the
    Tango Device member.
    """

    def __init__(
        self,
        desc: TangoLocalAttributeSinkDescriptor,
        device: SDPQueueConnector,
        dtype: np.dtype,
        shape: list[int],
    ):
        self.__device = device
        self.__started = False
        n_dims = len(shape)
        match n_dims:
            case 0:
                tdformat = AttrDataFormat.SCALAR
            case 1:
                tdformat = AttrDataFormat.SPECTRUM
            case 2:
                tdformat = AttrDataFormat.IMAGE
            case _:
                raise ValueError("invalid attribute shape", shape)
        default_value = cast_tango_default_value(desc.default_value, dtype, shape)

        self.__device.set_attr(desc.attribute_name, default_value, False)
        self.__attr = attribute(
            name=desc.attribute_name,
            dtype=dtype_to_tango(dtype),
            dformat=tdformat,
            access=AttrWriteType.READ,
            # NOTE: Tango internal dimensions uses F order instead of the numpy
            # default C order used by pytango attribute proxies.
            max_dim_x=shape[-1] if n_dims > 0 else 0,
            max_dim_y=shape[-2] if n_dims > 1 else 0,
            fread=device.generic_read,
        )

    @override
    def __enter__(self):
        self.__device.add_attribute(self.__attr)
        self.__device.set_change_event(self.__attr.name, True, False)
        return self

    @override
    def __exit__(self, exc, value, tb):
        # Note: Tango 9.4.2 does not support removing polled attributes
        # due to race conditions with the Tango polling thread.
        assert not self.__device.is_attribute_polled(self.__attr.name)
        self.__device.remove_attribute(self.__attr.name)

    @override
    async def start(self):
        self.__started = True

    @override
    async def stop(self):
        pass

    @override
    async def awrite(self, value: DataType):
        if self.__started:
            self.__device.set_attr(
                self.__attr.name,
                value,
            )


class TangoRemoteAttributeSinkDescriptor(SkaBaseModel):
    type: Literal["TangoRemoteAttributeSink"] = "TangoRemoteAttributeSink"
    device_name: str
    attribute_name: str
