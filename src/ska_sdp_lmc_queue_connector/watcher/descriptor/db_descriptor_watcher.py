from __future__ import annotations

import logging
import threading
from typing import Generator

import athreading
from overrides import override
from ska_sdp_config import Config
from ska_sdp_config.backend import Etcd3Backend

from .descriptor_watcher import DescriptorWatcher


class DbDescriptorWatcher(DescriptorWatcher):
    """
    An asynchronous sdp config database watcher that provides anasync
    iterable interface for when the specified database path(s) updates.
    """

    def __init__(self, config: Config, config_path: str, use_prefix=False):
        """Creates an asynchronous sdp configuration database watcher.

        Args:
            config (Config): Configuration database client
            key (str): Configuration database path to watch
            use_prefix (bool): When true watches multiple paths using :py:key as the
            prefix. Defaults to False.

        Returns:
            SdpConfigWatcher: watcher instance
        """
        self.__config = config
        self.__config_path = config_path
        self.__config_registry = {}
        self.__use_prefix = use_prefix
        self.__context = self.__awatch()

    @override
    async def __aenter__(self):
        self.__aiter = await self.__context.__aenter__()  # pylint: disable=no-member
        self.__trigger = lambda: None
        self.__stopped = threading.Event()
        return self

    @override
    async def __aexit__(self, exc_type, exc_value, traceback):
        self.__stopped.set()
        self.__trigger()
        await self.__context.__aexit__(exc_type, exc_value, traceback)  # pylint: disable=no-member

    @override
    async def __anext__(self) -> tuple[str, str | None]:
        return await self.__aiter.__anext__()

    def __has_keyvalue_changed(self, key: str, value: str | None):
        return key not in self.__config_registry or self.__config_registry[key] != value

    @athreading.iterate
    def __awatch(self) -> Generator[tuple[str, str | None], None, None]:
        backend = self.__config.backend
        assert isinstance(backend, Etcd3Backend)
        for watcher in backend.watcher():
            if self.__stopped.is_set():
                break
            # keep member reference of watcher to allow
            # interuption via aexit
            self.__trigger = watcher.trigger
            for txn in watcher.txn():
                try:
                    if self.__use_prefix:
                        # watch prefix to get first trigger if no
                        # keys are listed yet
                        _ = txn.get(self.__config_path)

                        keys_raw = txn.list_keys(self.__config_path, 1)

                        # TODO(yan-1549) watcher in some circumstances
                        # does not decode keys and/or values
                        keys = [
                            key.decode("utf-8") if isinstance(key, bytes) else key
                            for key in keys_raw
                            if key != self.__config_path
                        ]
                        # yield changed events
                        for key in keys:
                            raw_value = txn.get(key)
                            value = (
                                raw_value.decode("utf-8")
                                if isinstance(raw_value, bytes)
                                else raw_value
                            )
                            if self.__has_keyvalue_changed(key, value):
                                self.__config_registry.update({key: value})
                                yield (key, value)

                        # yield deleted events
                        for key in list(self.__config_registry.keys()):
                            if key not in keys:
                                self.__config_registry.pop(key)
                                yield (key, None)

                    else:
                        # yield all events
                        key = self.__config_path
                        value = txn.get(self.__config_path)
                        if self.__has_keyvalue_changed(key, value):
                            self.__config_registry.update({key: value})
                            yield (key, value)
                except BaseException:
                    logging.exception(
                        "Unexpected error while watching %s",
                        self.__config_path,
                    )
