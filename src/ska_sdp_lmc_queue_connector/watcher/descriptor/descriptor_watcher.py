"""DescriptorWatcher interface module."""

from __future__ import annotations

from abc import ABCMeta
from contextlib import AbstractAsyncContextManager
from typing import AsyncIterator


class DescriptorWatcher(  # pylint: disable=too-few-public-methods
    AbstractAsyncContextManager["DescriptorWatcher"],
    AsyncIterator[tuple[str, str | Exception | None]],
    metaclass=ABCMeta,
):
    """An asynchronous descriptor watcher interface intended for fast queue connector
    configuration."""
