from ska_sdp_config import Config

from ska_sdp_lmc_queue_connector.watcher.descriptor import DbDescriptorWatcher
from ska_sdp_lmc_queue_connector.watcher.descriptor.descriptor_watcher_flow_adapter import (
    DescriptorWatcherFlowAdapter,
)
from ska_sdp_lmc_queue_connector.watcher.flow import DbFlowWatcher

from .descriptor_watcher import DescriptorWatcher
from .etcd3_db_descriptor_watcher import Etcd3DbDescriptorWatcher


def create_descriptor_watcher(
    config: Config,
    subarray_id: str | None = None,
    pb_id: str | None = None,
    use_flow: bool = False,
    use_etcd3: bool = False,
    descriptor_path: str = "/component/lmc-queueconnector-01/owner",
    use_kafka: bool = True,
) -> DescriptorWatcher:
    """Creates an asynchronous sdp configuration database watcher.

    Args:
        config (Config): Configuration database client.
        subarray_id (str | None, optional): Subarray ID of database entries to watch. Defaults
        to all.
        pb_id (str | None, optional): ProcessingBlock ID of database entries to watch. Defaults
        to all.
        use_flow (bool, optional): Whether to use database flow entries instead of queue
        connector component entries. Defaults to False.
        use_etcd3 (bool, optional): Use native etcd3 watcher optimization. Defaults to False.
        descriptor_path (str, optional): Legacy db descriptor JSON path.

    Returns:
        DescriptorWatcher: async iterable watcher instance.
    """
    if use_flow:
        return DescriptorWatcherFlowAdapter(
            DbFlowWatcher(config, subarray_id=subarray_id, pb_id=pb_id),
            config,
            use_kafka=use_kafka,
        )
    else:
        if use_etcd3:
            if pb_id is None:
                return Etcd3DbDescriptorWatcher(config, descriptor_path, True)
            else:
                return Etcd3DbDescriptorWatcher(
                    config, f"{descriptor_path}/{pb_id}".format(subarray_id), False
                )
        else:
            if pb_id is None:
                return DbDescriptorWatcher(config, descriptor_path, True)
            else:
                return DbDescriptorWatcher(
                    config, f"{descriptor_path}/{pb_id}".format(subarray_id), False
                )
