import logging
from types import TracebackType

from overrides import override
from ska_sdp_config import Config

from ska_sdp_lmc_queue_connector.exchange import ExchangeDescriptor
from ska_sdp_lmc_queue_connector.sdp_queue_connector_descriptor import QueueConnectorDescriptor
from ska_sdp_lmc_queue_connector.watcher.descriptor.descriptor_watcher import DescriptorWatcher
from ska_sdp_lmc_queue_connector.watcher.descriptor.flow_adapter import (
    try_adapt_flow_and_take_ownership,
)
from ska_sdp_lmc_queue_connector.watcher.flow import FlowWatcher


class DescriptorWatcherFlowAdapter(DescriptorWatcher):
    """A DescriptorWatcher that adapts a FlowWatcher."""

    def __init__(self, watcher: FlowWatcher, config: Config, use_kafka: bool):
        self.__watcher = watcher
        self.__config = config
        self.__use_kafka = use_kafka
        self.logger = logging.getLogger(__name__)

    @override
    async def __aenter__(self) -> DescriptorWatcher:
        await self.__watcher.__aenter__()
        return self

    @override
    async def __aexit__(
        self,
        __exc_type: type[BaseException] | None,
        __exc_value: BaseException | None,
        __traceback: TracebackType | None,
    ) -> bool | None:
        await self.__watcher.__aexit__(__exc_type, __exc_value, __traceback)

    @override
    async def __anext__(self) -> tuple[str, str | None]:
        pb_id, flows = await self.__watcher.__anext__()
        self.logger.info("detected flows from %s : %s", pb_id, flows)
        exchanges: list[ExchangeDescriptor] = []
        try:
            for flow in flows:
                exchange = try_adapt_flow_and_take_ownership(
                    self.__config, flow, use_kafka=self.__use_kafka
                )
                if exchange:
                    exchanges.append(exchange)
        except Exception as e:
            return (pb_id, e)
        config = QueueConnectorDescriptor(exchanges=exchanges).model_dump_json()
        self.logger.info("adapted flows from %s : %s", pb_id, config)
        return (pb_id, config)
