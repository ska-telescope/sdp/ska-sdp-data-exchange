from overrides import override
from ska_sdp_config import Config
from ska_sdp_config.backend import Etcd3Backend
from ska_sdp_config.backend.common import depth_of_path
from ska_sdp_config.backend.etcd3 import _tag_depth, _untag_depth

from .descriptor_watcher import DescriptorWatcher
from .etcd3_watcher import Etcd3Watcher


class Etcd3DbDescriptorWatcher(DescriptorWatcher):
    """An asynchronous SDP configuration database watcher implemented using
    etcd3 watchers.
    """

    def __init__(self, config: Config, key: str, use_prefix=False):
        """Creates an asynchronous sdp configuration database watcher.

        Args:
            config (Config): Configuration database client
            key (str): Configuration database path to watch
            use_prefix (bool): When true watches multiple paths using :py:key as the
            prefix. Defaults to False.
        """
        assert isinstance(config.backend, Etcd3Backend)
        depth = depth_of_path(key)
        self.__watcher = Etcd3Watcher(
            config.backend._client,
            (f"{depth+1}{key}".encode() if use_prefix else _tag_depth(key)),
            use_prefix,
            get_existing=True,
        )

    @override
    async def __aenter__(self):
        self.__watcher = self.__watcher
        await self.__watcher.__aenter__()
        return self

    @override
    async def __aexit__(self, exc_type, exc_value, traceback):
        await self.__watcher.__aexit__(exc_type, exc_value, traceback)

    @override
    async def __anext__(self) -> tuple[str, str | None]:
        kv = await self.__watcher.__anext__()
        value = kv[1] if kv[1] is None else kv[1].decode("utf-8")
        return (_untag_depth(kv[0]), value)
