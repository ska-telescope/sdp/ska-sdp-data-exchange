from collections.abc import AsyncIterator
from contextlib import AbstractAsyncContextManager
from typing import Generator, Union

import athreading
from etcd3 import Etcd3Client, MultiEndpointEtcd3Client
from etcd3.events import DeleteEvent
from overrides import override


class Etcd3Watcher(
    AbstractAsyncContextManager["Etcd3Watcher"], AsyncIterator[tuple[bytes, bytes | None]]
):
    """
    A watcher that provides an asynchronous iterable interface
    for watching etcd3 value updates.
    """

    def __init__(
        self,
        client: Union[Etcd3Client, MultiEndpointEtcd3Client],
        key: bytes,
        use_prefix=False,
        get_existing=True,
    ):
        """An asynchronous etcd3 watcher that yields key-value pairs of watched
        database path(s).

        Args:
            client (Etcd3Client | MultiEndpointEtcd3Client): An etcd3 client
            key (bytes): etcd3 path or path prefix to watch
            use_prefix (bool): When true uses prefix watching of all child keys. Defaults to False.
            get_existing (bool): When true yields the current values at the key at the start
            of the stream. Defaults to True.
        """
        self.__client = client
        self.__key = key
        self.__use_prefix = use_prefix
        self.__get_existing = get_existing
        self.__context = self.__awatch()

    @override
    async def __aenter__(self):
        self.__aiter = await self.__context.__aenter__()  # pylint: disable=no-member
        self.__cancel = lambda: None
        return self

    @override
    async def __aexit__(self, exc_type, exc_value, traceback):
        self.__cancel()
        await self.__context.__aexit__(exc_type, exc_value, traceback)  # pylint: disable=no-member

    @override
    async def __anext__(self) -> tuple[bytes, bytes | None]:
        return await self.__aiter.__anext__()

    @athreading.iterate
    def __awatch(self) -> Generator[tuple[bytes, bytes | None], None, None]:
        if self.__get_existing:
            if self.__use_prefix:
                responses = self.__client.get_prefix(self.__key)
                for event in responses:
                    if event[0] is not None:
                        yield (event[1].key, event[0])
            else:
                event = self.__client.get(self.__key)
                yield (self.__key, event[0])

        event_generator, self.__cancel = (
            self.__client.watch_prefix(self.__key)
            if self.__use_prefix
            else self.__client.watch(self.__key)
        )
        for event in event_generator:
            value = None if isinstance(event, DeleteEvent) else event.value
            key = event.key
            yield (key, value)
