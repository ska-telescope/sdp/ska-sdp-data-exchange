import ast
import logging
import re

import numpy as np
from ska_sdp_config import Config
from ska_sdp_config.entity.common.tango import TANGO_ATTRIBUTE_RE
from ska_sdp_config.entity.flow import (
    DataQueue,
    Flow,
    FlowSource,
    TangoAttribute,
    TangoAttributeMap,
    TangoAttributeUrl,
)
from tango import CmdArgType

from ska_sdp_lmc_queue_connector.dataqueue_sourcesink import (
    DataQueueConsumerSourceDescriptor,
    DataQueueProducerSinkDescriptor,
)
from ska_sdp_lmc_queue_connector.exchange import (
    BufferWithTimePipeDescriptor,
    DefaultPipeDescriptor,
    ExchangeDescriptor,
    KafkaConsumerSourceDescriptor,
    KafkaProducerSinkDescriptor,
    PipeDescriptor,
    SourceDescriptor,
    TangoLocalAttributeSinkDescriptor,
    TangoPointingSubscriptionSourceDescriptor,
    TangoSubscriptionSourceDescriptor,
)
from ska_sdp_lmc_queue_connector.tango_dtype_helper import tango_to_dtype
from ska_sdp_lmc_queue_connector.tango_object_scatter_sink import (
    TangoAttributeDescriptor,
    TangoObjectScatterAttributeSinkDescriptor,
)

DATA_MODEL_RE = re.compile(r"([A-Za-z]+)(\[[0-9\s\,]*\])")
"""
Dtype and Shape regex to allow using data model for this information
(when the provided datamodel is otherwise unknown).

examples:
* float[3] -> { dtype: float, shape: [3] }
* int[] -> { dtype: int, shape: [] }
"""


__logger = logging.getLogger(__name__)


def is_function_tagged(source: FlowSource):
    return source.function is not None and "ska-sdp-lmc-queue-connector" in source.function


def adapt_flow_dtype(flow: Flow) -> np.dtype:
    data_model = flow.data_model

    if data_model == "PointingTable":
        return np.dtype(
            [
                ("antenna_name", "<U6"),
                ("ts", "datetime64[ns]"),
                ("az", "float64"),
                ("el", "float64"),
            ]
        )

    res = DATA_MODEL_RE.match(data_model)
    if res:
        return res[1]
    else:
        return object


def adapt_flow_shape(flow: Flow) -> list:
    data_model = flow.data_model

    if data_model == "PointingTable":
        return [-1]

    res = DATA_MODEL_RE.match(flow.data_model)
    if res:
        return ast.literal_eval(res[2])
    else:
        return []


def adapt_flow_pipe(flow: Flow) -> PipeDescriptor:
    if flow.data_model == "PointingTable":
        return BufferWithTimePipeDescriptor(timespan=1.0, flatten=True)
    return DefaultPipeDescriptor()


def adapt_flow_source(flow_source: Flow, use_kafka) -> SourceDescriptor:
    match flow_source.sink:
        case TangoAttributeMap():
            source = TangoObjectScatterAttributeSinkDescriptor(
                attributes=[
                    TangoAttributeDescriptor(
                        attribute_name=attribute.attribute_url.attribute_name,
                        dtype=tango_to_dtype(CmdArgType.names[attribute.dtype]),
                        shape=np.trim_zeros([attribute.max_dim_y, attribute.max_dim_x]),
                        path=query.select,
                        filter=query.when,
                        default_value=attribute.default_value,
                    )
                    for attribute, query in flow_source.sink.attributes
                ]
            )
        case TangoAttribute():
            url = flow_source.sink.attribute_url
            source = TangoSubscriptionSourceDescriptor(
                device_name=url.device_name,
                attribute_name=url.attribute_name,
            )
        case DataQueue():
            if isinstance(flow_source.sink.topics, str):
                topic = flow_source.sink.topics
            else:
                raise NotImplementedError(flow_source.sink.topics)
            dsns = flow_source.sink.host
            if isinstance(dsns, list):
                bootstrap_servers = [f"{dsn.host}:{dsn.port}" for dsn in dsns]
            else:
                bootstrap_servers = f"{dsns.host}:{dsns.port}"
            if use_kafka:
                source = KafkaConsumerSourceDescriptor(
                    servers=bootstrap_servers, topic=topic, format=flow_source.sink.format
                )
            else:
                source = DataQueueConsumerSourceDescriptor(
                    servers=bootstrap_servers, topic=topic, format=flow_source.sink.format
                )
        case _:
            raise ValueError(flow_source.sink)
    return source


def try_take_flow_ownership(config: Config, flow: Flow) -> bool:
    """Try setting the flow owner to this process.

    Args:
        config (Config): sdp config client.
        flow (Flow): flow to take ownership of.

    Returns:
        bool: True if taking ownership was successful
    """
    success = False
    for txn in config.txn():
        # take ownership if nothing owns the flow. In some cases
        # another process may have already claimed the flow so
        # ownership must be checked in the same transaction to safely
        # take ownership
        if not txn.flow.ownership(flow.key).exists():
            txn.flow.ownership(flow.key).take()
        is_owned_by_this_process = txn.flow.ownership(flow.key).is_owned_by_this_process()

    if is_owned_by_this_process:
        # log any flow source issues
        for source in flow.sources:
            if not is_function_tagged(source):
                __logger.warning(
                    "flow %s processed by queue connector contains ignored source %s",
                    flow.key,
                    source,
                )
        success = True
    return success


def try_adapt_flow_and_take_ownership(
    config: Config, flow: Flow, use_kafka: bool
) -> ExchangeDescriptor | None:
    """Adapts the newer Flow config model to the internal ExchangeDescriptor config model.

    NOTE: flow sources without a function member containing
    "ska-sdp-lmc-queue-connector" are ignored by design. Should some sources
    contain this and others not for the same flow warnings will be logged.

    Args:
        config (Config): configuration database for querying sources.
        flow (Flow): flow to adapt to an exchange descriptor
        use_kafka (bool): use specialized Kafka implementation for message queue flows.

    Raises:
        NotImplementedError: Flow contains values not supported yet.
        ValueError: Flow contains invalid value.

    Returns:
        ExchangeDescriptor | None: Exchange descriptor value to process or
        None if the flow should be ignored.
    """
    for txn in config.txn():
        if (
            txn.flow.ownership(flow.key).exists()
            and not txn.flow.ownership(flow.key).is_owned_by_this_process()
        ):
            # Flow is already owned by a seperate process, skip adapting
            __logger.info("Flow already owned")
            return None

    source_descriptors: list[SourceDescriptor] = []
    adapted = None

    for source in flow.sources:
        if not is_function_tagged(source):
            break
        match source.uri:
            case Flow.Key():
                flow_source = None
                for txn in config.txn():
                    flow_source = txn.flow.get(source.uri)
                if flow_source is None:
                    __logger.error("flow source %s does not exist in config database", source.uri)
                    continue
                source_descriptor = adapt_flow_source(flow_source, use_kafka)
                source_descriptors.append(source_descriptor)
            case TangoAttributeUrl():
                url = source.uri
                # depending on the data model, may require a specialized source
                match flow.data_model:
                    case "PointingTable":
                        source_descriptor = TangoPointingSubscriptionSourceDescriptor(
                            device_name=url.device_name,
                            attribute_name=url.attribute_name,
                            antenna_name=TANGO_ATTRIBUTE_RE.match(url.unicode_string())[4],
                        )
                    case _:
                        source_descriptor = TangoSubscriptionSourceDescriptor(
                            device_name=url.device_name,
                            attribute_name=url.attribute_name,
                        )
                source_descriptors.append(source_descriptor)
            case _:
                __logger.error(
                    "flow source %s of type %s not supported", source.uri, type(source.uri)
                )
                continue

    if len(source_descriptors) != 0:
        pipe = adapt_flow_pipe(flow)
        dtype = adapt_flow_dtype(flow)
        shape = adapt_flow_shape(flow)
        match flow.sink:
            case TangoAttributeMap():
                sink_descriptor = TangoObjectScatterAttributeSinkDescriptor(
                    attributes=[
                        TangoAttributeDescriptor(
                            attribute_name=attribute.attribute_url.attribute_name,
                            dtype=tango_to_dtype(CmdArgType.names[attribute.dtype]),
                            shape=np.trim_zeros([attribute.max_dim_y, attribute.max_dim_x]),
                            path=query.select,
                            filter=query.when,
                            default_value=attribute.default_value,
                        )
                        for attribute, query in flow.sink.attributes
                    ]
                )
            case TangoAttribute():
                dtype = tango_to_dtype(CmdArgType.names[flow.sink.dtype])
                shape = np.trim_zeros([flow.sink.max_dim_y, flow.sink.max_dim_x])
                sink_descriptor = TangoLocalAttributeSinkDescriptor(
                    attribute_name=flow.sink.attribute_url.attribute_name,
                    default_value=flow.sink.default_value,
                )
            case DataQueue():
                dsns = flow.sink.host
                if isinstance(dsns, list):
                    bootstrap_servers = [f"{dsn.host}:{dsn.port}" for dsn in dsns]
                else:
                    bootstrap_servers = f"{dsns.host}:{dsns.port}"
                topics = flow.sink.topics

                if isinstance(topics, str):
                    bootstrap_servers = f"{dsns.host}:{dsns.port}"
                else:
                    raise NotImplementedError(dsns)
                if use_kafka:
                    sink_descriptor = KafkaProducerSinkDescriptor(
                        servers=bootstrap_servers,
                        topic=flow.sink.topics,
                        format=flow.sink.format,
                    )
                else:
                    sink_descriptor = DataQueueProducerSinkDescriptor(
                        servers=bootstrap_servers,
                        topic=flow.sink.topics,
                        format=flow.sink.format,
                    )
            case _:
                raise ValueError(flow.sink)
        adapted = ExchangeDescriptor(
            dtype=dtype, shape=shape, sink=sink_descriptor, source=source_descriptors, pipe=pipe
        )

    # take ownership if adapting successful
    if adapted is None or not try_take_flow_ownership(config, flow):
        adapted = None
    return adapted
