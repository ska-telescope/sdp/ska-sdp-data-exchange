# pylint: disable=no-member

from __future__ import annotations

import logging
import threading
from collections import defaultdict
from types import TracebackType
from typing import Generator, Hashable, Iterable, TypeVar

import athreading
from overrides import override
from ska_sdp_config import Config
from ska_sdp_config.config import Transaction
from ska_sdp_config.entity.flow import DataQueue, Flow, TangoAttribute, TangoAttributeMap

from ska_sdp_lmc_queue_connector.watcher.flow.flow_watcher import FlowWatcher

_KeyT = TypeVar("_KeyT", bound=Hashable)
_ValueT = TypeVar("_ValueT")

_logger = logging.getLogger(__name__)


def _dict_symmetric_difference_deep(
    left: dict[_KeyT, _ValueT], right: dict[_KeyT, _ValueT]
) -> Iterable[_KeyT]:
    """
    Performs a deep symmetric difference comparison across dictionary
    key-values and returns a set of keys that do not match.

    Args:
        left (dict[_KeyT, _ValueT]): first dictionary to compare
        right (dict[_KeyT, _ValueT]): second dictionary to compare

    Returns:
        Iterable[_KeyT]: unique set of keys that do not match
    """
    # a hashable ValueT could use symmetric difference operator e.g.
    # return dict(set(a.items()) ^ set(b.items())).keys()
    all_keys = set(left.keys() | right.keys())
    different_keys = []
    for key in all_keys:
        if key not in left or key not in right or left[key] != right[key]:
            different_keys.append(key)
    different_keys.sort()
    return different_keys


class DbFlowWatcher(FlowWatcher):
    """
    An asynchronous sdp config watcher that provides an async
    iterable interface for when the specified configuration
    database path(s) value updates.
    """

    __SOURCE_DISCRIMINATOR = "kind"
    __KINDS = [
        # pylint: disable-next=unsubscriptable-object
        DataQueue.model_fields[__SOURCE_DISCRIMINATOR].default,
        # pylint: disable-next=unsubscriptable-object
        TangoAttribute.model_fields[__SOURCE_DISCRIMINATOR].default,
        # pylint: disable-next=unsubscriptable-object
        TangoAttributeMap.model_fields[__SOURCE_DISCRIMINATOR].default,
    ]

    def __init__(
        self,
        config: Config,
        subarray_id: str | None = None,
        pb_id: str | None = None,
        kind: str | None = None,
        name: str | None = None,
        state: bool = False,
    ):
        """Creates an asynchronous sdp configuration database watcher.

        Args:
            config (Config): Configuration database client
            subarrray_id (str, optional): Flow subarray_id to filter
            pb_id (str, optional): Flow pb_id to filter
            kind (str, optional): Flow kind to filter
            name (str, optional): Flow name to filter
            state (bool, optional): Mark whether to watch flow or it's state entry.

        Returns:
            SdpConfigWatcher: watcher instance
        """
        self.__config = config
        self.__subarray_id = subarray_id
        self.__pb_id = pb_id
        self.__kinds = [kind] if kind is not None else self.__KINDS
        self.__name = name
        self.__state = state
        self.__config_cache: dict[str, list[Flow]] = defaultdict(list)
        self._trigger = lambda: None
        self.__aiter = self.__awatch()

    @override
    async def __aenter__(self):
        await self.__aiter.__aenter__()
        self.__stopped = threading.Event()
        return self

    async def __aexit__(
        self,
        __exc_type: type[BaseException] | None,
        __exc_value: BaseException | None,
        __traceback: TracebackType | None,
    ) -> bool | None:
        self._trigger()
        self.__stopped.set()
        await self.__aiter.__aexit__(__exc_type, __exc_value, __traceback)

    @override
    async def __anext__(self) -> tuple[str, list[Flow]]:
        return await self.__aiter.__anext__()

    def __get_flow_keys(self, txn: Transaction) -> list[Flow.Key]:
        """Query all flow keys belonging to the subarray acive execution block."""
        flow_keys: list[Flow.Key] = []
        if self.__subarray_id is not None:
            subarray: dict = txn.component(f"lmc-subarray-{self.__subarray_id}").get()
            if subarray is None:
                _logger.warning("subarray %s does not exist.", self.__subarray_id)
            else:
                eb_id = subarray.get("eb_id", None)
                if eb_id is not None:
                    eb = txn.execution_block.get(eb_id)
                    if eb is None:
                        _logger.info("subarray eb_id %s does not exist.", eb_id)
                    else:
                        pb_ids = eb.pb_realtime + eb.pb_batch
                        for pb_id in pb_ids:
                            for kind in self.__kinds:
                                flow_keys.extend(
                                    txn.flow.query_keys(
                                        pb_id=pb_id,
                                        kind=kind,
                                        name=self.__name,
                                    )
                                )
        else:
            for kind in self.__kinds:
                flow_keys.extend(
                    txn.flow.query_keys(
                        pb_id=self.__pb_id,
                        kind=kind,
                        name=self.__name,
                    )
                )
        return flow_keys

    @athreading.iterate
    def __awatch(self) -> Generator[tuple[str, list[Flow]], None, None]:
        watcher_generator = self.__config.watcher()
        for watcher in watcher_generator:
            if self.__stopped.is_set():
                break

            # keep member reference of watcher to allow
            # interuption via aexit
            self._trigger = watcher.trigger
            for txn in watcher.txn():
                try:
                    flow_keys = self.__get_flow_keys(txn)

                    # recreate registry
                    new_config_cache: dict[str, list[Flow]] = defaultdict(list)

                    for key in flow_keys:
                        if self.__state:
                            value = txn.flow.state(key).get()
                        else:
                            value = txn.flow.get(index=key)
                        if value is not None:
                            new_config_cache[key.pb_id].append(value)

                    # yield changed pb_ids
                    for pb_id in _dict_symmetric_difference_deep(
                        new_config_cache, self.__config_cache
                    ):
                        yield (pb_id, new_config_cache.get(pb_id, []))

                    # cache previous registry
                    self.__config_cache = new_config_cache

                except Exception:
                    _logger.exception(
                        "Unexpected error while watching flows, subarray_id: %s, pb_id: %s",
                        self.__subarray_id,
                        self.__pb_id,
                    )
