from __future__ import annotations

from abc import ABCMeta
from contextlib import AbstractAsyncContextManager
from typing import AsyncIterator

from ska_sdp_config.entity.flow import Flow


class FlowWatcher(
    AbstractAsyncContextManager["FlowWatcher"],
    AsyncIterator[tuple[str, list[Flow]]],
    metaclass=ABCMeta,
):
    """An asynchronous SDP configuration database watcher interface."""
