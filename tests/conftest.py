import contextlib

import pytest
import tango
from tango.test_context import MultiDeviceTestContext

from ska_sdp_lmc_queue_connector.dev_factory import DevFactory
from tests.manual_sdp_queue_connector import ManualSDPQueueConnector


# pylint: disable-next=unused-argument
def pytest_sessionstart(session):
    """
    Pytest hook; prints info about tango version.
    :param session: a pytest Session object
    :type session: :py:class:`pytest.Session`
    """
    print(tango.utils.info())


def multi_device_test_context(devices_to_load):
    context = MultiDeviceTestContext(devices_to_load, process=False)
    DevFactory._test_context = context
    return context


@pytest.fixture
def tango_context(devices_to_load):
    with multi_device_test_context(devices_to_load) as context:
        yield context


_DEVICE_NAME = "test/sdp/1"


@contextlib.contextmanager
def _create_manual_qc_device(device_properties):
    devices_to_load = (
        {
            "class": ManualSDPQueueConnector,
            "devices": [
                {
                    "name": _DEVICE_NAME,
                    "properties": device_properties,
                },
            ],
        },
    )
    with multi_device_test_context(devices_to_load) as context:
        yield context.get_device(_DEVICE_NAME)


@pytest.fixture(name="create_manual_qc")
def create_manual_qc_fixture():
    return _create_manual_qc_device


@pytest.fixture(name="manual_qc")
def manual_qc_fixture(device_properties):
    with _create_manual_qc_device(device_properties) as proxy:
        yield proxy
