import asyncio
import logging
import queue
from contextlib import AbstractAsyncContextManager, suppress
from typing import TypeVar

from pydantic import BaseModel
from ska_sdp_config import Config

from ska_sdp_lmc_queue_connector.sdp_queue_connector_descriptor import StateModel
from ska_sdp_lmc_queue_connector.watcher.flow.db_flow_watcher import DbFlowWatcher

Model = TypeVar("Model")


class DbFlowWaiter(AbstractAsyncContextManager):
    """
    A database test helper that can wait for a specific database value
    with timeout.
    """

    def __init__(self, config: Config, pb_id: str | None, state: bool = False):
        self.__pb_id = pb_id
        self.__watcher = DbFlowWatcher(config, pb_id=pb_id, state=state)
        self.__condition = asyncio.Condition()
        self.__queue: queue.Queue[list[dict]] = queue.Queue()
        self.__task = None

    async def __aenter__(self):
        await self.__watcher.__aenter__()
        self.__task = asyncio.create_task(self.__read_and_notify())
        return self

    async def __aexit__(self, __typ, __val, __tb):
        if self.__task is not None:
            self.__task.cancel()
            with suppress(asyncio.CancelledError):
                await self.__task
        await self.__watcher.__aexit__(__typ, __val, __tb)

    async def __read_and_notify(self):
        async for _, values in self.__watcher:
            # TODO value is list[dict] from flow.state().get()
            self.__queue.put_nowait([StateModel.model_validate(value) for value in values])
            async with self.__condition:
                self.__condition.notify_all()

    async def wait_for(self, value: BaseModel | None, timeout: float | None):
        """Waits for the specified value to appear in database at
        the given key from construction. Stream values are queued
        from when the class context is entered to avoid data racing.

        Args:
            value (str): the database value to wait for
            timeout (float | None): timeout in seconds before raising
            asyncio.TimeoutError
        """

        def predicate():
            result = False
            while not result and not self.__queue.empty():
                raw = self.__queue.get_nowait()
                result = (
                    value is None if len(raw) == 0 else all(map(lambda item: value == item, raw))
                )
                logging.debug(
                    "%s wait predicate(%s) on %s == %s", self.__pb_id, value, raw, result
                )
            return result

        async with self.__condition:
            try:
                await asyncio.wait_for(
                    self.__condition.wait_for(predicate),
                    timeout=timeout,
                )
            except asyncio.TimeoutError:
                raise asyncio.TimeoutError(f"timeout waiting for {value}")
