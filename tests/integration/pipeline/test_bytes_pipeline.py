import asyncio

import numpy as np
import pytest

from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    InMemorySinkDescriptor,
    InMemorySourceDescriptor,
    KafkaConsumerSourceDescriptor,
    KafkaProducerSinkDescriptor,
    QueueConnectorDescriptor,
    TangoLocalAttributeSinkDescriptor,
    TangoSubscriptionSourceDescriptor,
)
from ska_sdp_lmc_queue_connector.in_memory_sourcesink import InMemorySink

KAFKA_HOST = "localhost:9092"
TEST_TOPIC = "kafka-data-test-events"

DTYPE = np.dtype(bytes)
SHAPE = []


@pytest.fixture
def device_properties():
    return {
        "exchanges_json": QueueConnectorDescriptor(
            exchanges=[
                ExchangeDescriptor(
                    dtype=DTYPE,
                    shape=SHAPE,
                    source=InMemorySourceDescriptor(
                        data=[
                            ("ascii", b"hello"),
                            (
                                "ascii",
                                bytes([119, 111, 114, 108, 100]),
                            ),
                        ],
                        delay=0.01,
                    ),
                    sink=KafkaProducerSinkDescriptor(
                        servers=KAFKA_HOST,
                        topic=TEST_TOPIC,
                        format="ascii",
                    ),
                ),
                ExchangeDescriptor(
                    dtype=DTYPE,
                    shape=SHAPE,
                    source=KafkaConsumerSourceDescriptor(
                        servers=KAFKA_HOST,
                        topic=TEST_TOPIC,
                        format="ascii",
                    ),
                    sink=TangoLocalAttributeSinkDescriptor(
                        attribute_name="testattr",
                        default_value=("ascii", b""),
                    ),
                ),
                ExchangeDescriptor(
                    dtype=DTYPE,
                    shape=SHAPE,
                    source=TangoSubscriptionSourceDescriptor(
                        device_name="test/sdp/1",
                        attribute_name="testattr",
                    ),
                    sink=InMemorySinkDescriptor(key="1"),
                ),
            ]
        ).model_dump_json_nodb(),
    }


@pytest.mark.integration_test
@pytest.mark.asyncio
async def test_bytes_pipeline(manual_qc):
    manual_qc.Start()
    await asyncio.sleep(0.5)
    manual_qc.Stop()

    q = InMemorySink.get_queue(key="1")
    final_value = tuple()
    while not q.empty():
        final_value = q.get_nowait()

    assert isinstance(final_value, tuple)
    assert final_value[0] == "ascii"
    assert final_value[1] == bytes([119, 111, 114, 108, 100])
