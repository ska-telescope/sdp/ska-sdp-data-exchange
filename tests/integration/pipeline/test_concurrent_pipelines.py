import asyncio
import json
import logging

import pytest
from aiokafka import AIOKafkaConsumer, AIOKafkaProducer
from pydantic_core import Url
from ska_sdp_config import Config, ExecutionBlock
from ska_sdp_config.entity.flow import DataQueue, Flow, FlowSource, TangoAttribute
from tango import DeviceProxy

import ska_sdp_lmc_queue_connector.astream_utils as astream
from ska_sdp_lmc_queue_connector.sdp_queue_connector import SDPQueueConnector
from ska_sdp_lmc_queue_connector.sdp_queue_connector_descriptor import StateModel
from tests.db_flow_waiter import DbFlowWaiter

SUBARRAY_ID = "03"
DEVICE_NAME = f"test/queueconnector/{SUBARRAY_ID}"
KAFKA_HOST = Url("kafka://localhost:9092")

EB_ID = "eb-0-00000000-realtime"
PB_ID1 = "pb-0-00000000-pipeline1"
PB_ID2 = "pb-0-00000000-pipeline2"

TEST_TOPIC1IN = "test1in"
TEST_TOPIC2IN = "test2in"
TEST_TOPIC1OUT = "test1out"
TEST_TOPIC2OUT = "test2out"


PIPELINE1 = []
PIPELINE1.append(
    Flow(
        key=Flow.Key(pb_id=PB_ID1, name="flow1-source"),
        data_model="ushort[]",
        sources=[],
        sink=DataQueue(topics=TEST_TOPIC1IN, host=KAFKA_HOST.unicode_string(), format="json"),
    )
)
PIPELINE1.append(
    Flow(
        key=Flow.Key(pb_id=PB_ID1, name="flow1-in"),
        data_model="ushort[]",
        sources=[
            FlowSource(uri=PIPELINE1[0].key, function="ska-sdp-lmc-queue-connector:exchange")
        ],
        sink=TangoAttribute(
            attribute_url=f"tango://{DEVICE_NAME}/test_attr1",
            dtype="DevUShort",
            max_dim_x=0,
            max_dim_y=0,
            default_value="0",
        ),
    )
)
PIPELINE1.append(
    Flow(
        key=Flow.Key(pb_id=PB_ID1, name="flow1-out"),
        data_model="ushort[]",
        sources=[
            FlowSource(uri=PIPELINE1[1].key, function="ska-sdp-lmc-queue-connector:exchange")
        ],
        sink=DataQueue(topics=TEST_TOPIC1OUT, host=KAFKA_HOST, format="json"),
    )
)

PIPELINE2 = []
PIPELINE2.append(
    Flow(
        key=Flow.Key(pb_id=PB_ID2, name="flow2-source"),
        data_model="ushort[]",
        sources=[],
        sink=DataQueue(topics=TEST_TOPIC2IN, host=KAFKA_HOST.unicode_string(), format="json"),
    )
)
PIPELINE2.append(
    Flow(
        key=Flow.Key(pb_id=PB_ID2, name="flow2-in"),
        data_model="ushort[]",
        sources=[
            FlowSource(uri=PIPELINE2[0].key, function="ska-sdp-lmc-queue-connector:exchange")
        ],
        sink=TangoAttribute(
            attribute_url=f"tango://{DEVICE_NAME}/test_attr2",
            dtype="DevUShort",
            max_dim_x=0,
            max_dim_y=0,
            default_value="0",
        ),
    )
)
PIPELINE2.append(
    Flow(
        key=Flow.Key(pb_id=PB_ID2, name="flow2-out"),
        data_model="ushort[]",
        sources=[
            FlowSource(uri=PIPELINE2[1].key, function="ska-sdp-lmc-queue-connector:exchange")
        ],
        sink=DataQueue(topics=TEST_TOPIC2OUT, host=KAFKA_HOST, format="json"),
    )
)


def kafka_bootstrap(url: Url):
    return f"{url.host}:{url.port}"


@pytest.fixture()
def devices_to_load(use_kafka: bool):
    return (
        {
            "class": SDPQueueConnector,
            "devices": [
                {
                    "name": DEVICE_NAME,
                    "properties": {
                        "experimental_flow": True,
                        "subarray_id": SUBARRAY_ID,
                        "experimental_kafka": use_kafka,
                    },
                },
            ],
        },
    )


@pytest.fixture(name="device_proxy")
def sdp_proxy(tango_context):
    return tango_context.get_device(DEVICE_NAME)


@pytest.fixture(name="config")
def fixture_config():
    config = Config(backend="etcd3")
    # must delete both entities and subentities
    for txn in config.txn():
        txn.execution_block.create_or_update(
            ExecutionBlock(
                key=EB_ID,
                beams=[],
                channels=[],
                context={},
                fields=[],
                max_length=None,
                pb_batch=[],
                pb_realtime=[PB_ID1, PB_ID2],
                polarisations=[],
                resources={},
                scan_types=[],
                subarray_id=SUBARRAY_ID,
            )
        )
        txn.component(f"lmc-subarray-{SUBARRAY_ID}").create_or_update({"eb_id": EB_ID})
        for path in txn.raw.list_keys("/flow", recurse=2):
            logging.warning("deleting: %s", path)
            txn.raw.delete(path, must_exist=False)
    yield config
    for txn in config.txn():
        for path in txn.raw.list_keys("/flow", recurse=2):
            logging.warning("deleting: %s", path)
            txn.raw.delete(path, must_exist=False)
        txn.component(f"lmc-subarray-{SUBARRAY_ID}").delete()
        txn.execution_block.delete(EB_ID)


@pytest.mark.integration_test
@pytest.mark.asyncio
@pytest.mark.parametrize(
    "use_kafka,configure_timeout",
    [
        (False, 5.0),
        (True, 3.0),
    ],
)
async def test_concurrent_pipelines_simple(
    config: Config, device_proxy: DeviceProxy, configure_timeout: float
):
    async with (
        DbFlowWaiter(config, PB_ID1, state=True) as config_waiter1,
        DbFlowWaiter(config, PB_ID2, state=True) as config_waiter2,
    ):
        for txn in config.txn():
            txn.flow.create(PIPELINE1[0])
            txn.flow.create(PIPELINE1[1])
            txn.flow.create(PIPELINE1[2])
            txn.flow.create(PIPELINE2[0])
            txn.flow.create(PIPELINE2[1])
            txn.flow.create(PIPELINE2[2])
        await config_waiter1.wait_for(StateModel(status="FLOWING"), configure_timeout)
        await config_waiter2.wait_for(StateModel(status="FLOWING"), configure_timeout)

        # test data
        async with (
            AIOKafkaProducer(bootstrap_servers=kafka_bootstrap(KAFKA_HOST)) as q1i,
            AIOKafkaConsumer(TEST_TOPIC1OUT, bootstrap_servers=kafka_bootstrap(KAFKA_HOST)) as q1,
        ):
            await q1i.send(TEST_TOPIC1IN, b"1")
            await q1i.send(TEST_TOPIC1IN, b"2")
            await q1i.send(TEST_TOPIC1IN, b"3")
            stream = astream.timeout(q1, 1.0)
            for expected in range(1, 4):
                actual = json.loads((await anext(stream)).value)
                while actual == 0:
                    actual = json.loads((await anext(stream)).value)
                assert expected == actual
        async with (
            AIOKafkaProducer(bootstrap_servers=kafka_bootstrap(KAFKA_HOST)) as q2i,
            AIOKafkaConsumer(TEST_TOPIC2OUT, bootstrap_servers=kafka_bootstrap(KAFKA_HOST)) as q2,
        ):
            await q2i.send(TEST_TOPIC2IN, b"4")
            await q2i.send(TEST_TOPIC2IN, b"5")
            await q2i.send(TEST_TOPIC2IN, b"6")
            stream = astream.timeout(q2, 1.0)
            for expected in range(4, 7):
                actual = json.loads((await anext(stream)).value)
                while actual == 0:
                    actual = json.loads((await anext(stream)).value)
                assert expected == actual

        # test attributes and cleanup
        assert {"test_attr1", "test_attr2"}.issubset(device_proxy.get_attribute_list())

        for txn in config.txn():
            txn.flow.delete(PIPELINE1[0], recurse=False)
            txn.flow.delete(PIPELINE1[1], recurse=False)
            txn.flow.delete(PIPELINE1[2], recurse=False)
            txn.flow.delete(PIPELINE2[0], recurse=False)
            txn.flow.delete(PIPELINE2[1], recurse=False)
            txn.flow.delete(PIPELINE2[2], recurse=False)
        await config_waiter1.wait_for(None, configure_timeout)
        await config_waiter2.wait_for(None, configure_timeout)

        # TODO(YAN-1775): sdp-config list_keys does not provide a way
        # to lists states, state no longer gets observed by the waiter
        # once the entity disappears. Could:
        # * Cache list of owned flow keys
        # * Extend SDP config
        await asyncio.sleep(6.0)
        assert {"test_attr1", "test_attr2"}.intersection(
            device_proxy.get_attribute_list()
        ) == set()


@pytest.mark.integration_test
@pytest.mark.asyncio
@pytest.mark.parametrize(
    "use_kafka,configure_timeout",
    [
        (False, 10.0),
        (True, 3.0),
    ],
)
async def test_concurrent_pipelines_cancel(
    config: Config, device_proxy: DeviceProxy, configure_timeout: float
):
    async with (
        DbFlowWaiter(config, PB_ID1, state=True) as config_waiter1,
        DbFlowWaiter(config, PB_ID2, state=True) as config_waiter2,
    ):
        for txn in config.txn():
            txn.flow.create(PIPELINE1[0])
            txn.flow.create(PIPELINE1[1])
            txn.flow.create(PIPELINE1[2])
            txn.flow.create(PIPELINE2[0])
            txn.flow.create(PIPELINE2[1])
            txn.flow.create(PIPELINE2[2])
        await config_waiter2.wait_for(StateModel(status="FLOWING"), configure_timeout)
        for txn in config.txn():
            txn.flow.delete(PIPELINE2[0])
            txn.flow.delete(PIPELINE2[1])
            txn.flow.delete(PIPELINE2[2])

        # wait for steady state
        await config_waiter1.wait_for(StateModel(status="FLOWING"), configure_timeout)
        await config_waiter2.wait_for(None, configure_timeout)

        # pipeline1 should run as normal
        async with (
            AIOKafkaProducer(bootstrap_servers=kafka_bootstrap(KAFKA_HOST)) as q1i,
            AIOKafkaConsumer(TEST_TOPIC1OUT, bootstrap_servers=kafka_bootstrap(KAFKA_HOST)) as q1,
        ):
            await q1i.send(TEST_TOPIC1IN, b"1")
            await q1i.send(TEST_TOPIC1IN, b"2")
            await q1i.send(TEST_TOPIC1IN, b"3")
            stream = astream.timeout(q1, 1.0)
            for expected in range(1, 4):
                actual = json.loads((await anext(stream)).value)
                while actual == 0:
                    actual = json.loads((await anext(stream)).value)
                assert expected == actual

        # 2nd pipeline should have been cancelled
        async with (
            AIOKafkaProducer(bootstrap_servers=kafka_bootstrap(KAFKA_HOST)) as q2i,
            AIOKafkaConsumer(TEST_TOPIC2OUT, bootstrap_servers=kafka_bootstrap(KAFKA_HOST)) as q2,
        ):
            await q2i.send(TEST_TOPIC2IN, b"4")
            await q2i.send(TEST_TOPIC2IN, b"5")
            await q2i.send(TEST_TOPIC2IN, b"6")
            stream = astream.timeout(q2, 1.0)
            with pytest.raises(asyncio.TimeoutError):
                actual = json.loads((await anext(stream)).value)
                assert False, actual

        # test attributes and cleanup
        assert {"test_attr1"}.issubset(device_proxy.get_attribute_list())
        for txn in config.txn():
            txn.flow.delete(PIPELINE1[0])
            txn.flow.delete(PIPELINE1[1])
            txn.flow.delete(PIPELINE1[2])

        await config_waiter1.wait_for(None, 3.0)


@pytest.mark.integration_test
@pytest.mark.asyncio
@pytest.mark.parametrize(
    "use_kafka,configure_timeout",
    [
        (False, 6.0),
        (True, 3.0),
    ],
)
async def test_concurrent_pipelines_fault(
    config: Config, device_proxy: DeviceProxy, configure_timeout: float
):
    badflow2out = Flow(
        key=Flow.Key(pb_id=PB_ID2, name="flow2-out"),
        data_model="unknown",
        sources=[
            FlowSource(uri=PIPELINE1[0].key, function="ska-sdp-lmc-queue-connector:exchange")
        ],
        sink=DataQueue(topics=TEST_TOPIC2OUT, host=KAFKA_HOST, format="npz"),
    )

    async with (
        DbFlowWaiter(config, PB_ID1, state=True) as config_waiter1,
        DbFlowWaiter(config, PB_ID2, state=True) as config_waiter2,
    ):
        for txn in config.txn():
            txn.flow.create(PIPELINE1[0])
            txn.flow.create(PIPELINE1[1])
            txn.flow.create(PIPELINE1[2])
            txn.flow.create(PIPELINE2[0])
            txn.flow.create(PIPELINE2[1])
            txn.flow.create(badflow2out)

        # 1st pipeline should run as per normal
        await config_waiter1.wait_for(StateModel(status="FLOWING"), configure_timeout)

        # 2nd pipeline never reaches OFF state
        with pytest.raises(asyncio.TimeoutError):
            await config_waiter2.wait_for(StateModel(status="WAITING"), configure_timeout)

        state = None
        for txn in config.txn():
            state = StateModel.model_validate(txn.flow.state(badflow2out).get())
        assert state is not None
        assert state.exception is not None
        expected_message = "validation error"
        assert expected_message in state.exception.message
        assert expected_message in state.exception.stacktrace

        # pipeline1 should run as normal
        async with (
            AIOKafkaProducer(bootstrap_servers=kafka_bootstrap(KAFKA_HOST)) as q1i,
            AIOKafkaConsumer(TEST_TOPIC1OUT, bootstrap_servers=kafka_bootstrap(KAFKA_HOST)) as q1,
        ):
            await q1i.send(TEST_TOPIC1IN, b"1")
            await q1i.send(TEST_TOPIC1IN, b"2")
            await q1i.send(TEST_TOPIC1IN, b"3")
            stream = astream.timeout(q1, 1.0)
            for expected in range(1, 4):
                actual = json.loads((await anext(stream)).value)
                while actual == 0:
                    actual = json.loads((await anext(stream)).value)
                assert expected == actual

        # test attributes and cleanup
        assert {"test_attr1"}.issubset(device_proxy.get_attribute_list())
        for txn in config.txn():
            txn.flow.delete(PIPELINE1[0], recurse=False)
            txn.flow.delete(PIPELINE1[1], recurse=False)
            txn.flow.delete(PIPELINE1[2], recurse=False)
            txn.flow.delete(PIPELINE2[0], recurse=False)
            txn.flow.delete(PIPELINE2[1], recurse=False)
            txn.flow.delete(badflow2out, recurse=False)

        # wait for standby
        await config_waiter1.wait_for(None, configure_timeout)

        # TODO(YAN-1775): sdp-config list_keys does not provide a way
        # to lists states, state no longer gets observed by the waiter
        # once the entity disappears. Could:
        # * Cache list of owned flow keys
        # * Extend SDP config
        await asyncio.sleep(3.0)
        assert {"test_attr1", "test_attr2"}.intersection(
            device_proxy.get_attribute_list()
        ) == set()
