import asyncio

import aiokafka
import numpy as np
import pytest
from ska_sdp_datamodels.calibration import PointingTable, import_pointingtable_from_hdf5
from ska_sdp_datamodels.utilities import encode
from tango import EventType

from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    KafkaConsumerSourceDescriptor,
    QueueConnectorDescriptor,
)
from ska_sdp_lmc_queue_connector.dataqueue_sourcesink import DataQueueConsumerSourceDescriptor
from ska_sdp_lmc_queue_connector.tango_object_scatter_sink import (
    TangoAttributeDescriptor,
    TangoObjectScatterAttributeSinkDescriptor,
)
from tests.tango_helper import SubscribeEventValueContext
from tests.test_utils import untar

KAFKA_HOST = "localhost:9092"
POINTING_OFFSET_TOPIC = "pointing-offsets"
FORMAT = "msgpack_numpy"


@pytest.fixture
def device_properties(data_queue_consumer_impl: type):
    return {
        "exchanges_json": QueueConnectorDescriptor(
            exchanges=[
                ExchangeDescriptor(
                    dtype=object,
                    shape=[],
                    source=data_queue_consumer_impl(
                        servers=KAFKA_HOST,
                        topic=POINTING_OFFSET_TOPIC,
                        format=FORMAT,
                    ),
                    sink=TangoObjectScatterAttributeSinkDescriptor(
                        attributes=[
                            TangoAttributeDescriptor(
                                attribute_name="time",
                                shape=[1],
                                dtype=np.float64,
                                path="coords.time.data",
                                default_value=np.array(
                                    [0],
                                ),
                            ),
                            TangoAttributeDescriptor(
                                attribute_name="antenna",
                                shape=[4],
                                dtype=np.int64,
                                path="coords.antenna.data",
                                default_value=np.array([0, 0, 0, 0]),
                            ),
                            TangoAttributeDescriptor(
                                attribute_name="pointing",
                                shape=[2],
                                dtype=np.float64,
                                path="data_vars.pointing.data[0][0][0][0][:]",
                                default_value=0.0,
                            ),
                        ]
                    ),
                )
            ]
        ).model_dump_json_nodb(),
    }


@pytest.mark.integration_test
@pytest.mark.asyncio
@pytest.mark.parametrize(
    "data_queue_consumer_impl", [DataQueueConsumerSourceDescriptor, KafkaConsumerSourceDescriptor]
)
async def test_pointing_offset_pipeline(manual_qc):
    table = import_pointingtable_from_hdf5(
        untar(
            "tests/data/global-8arcmin-azel/"
            "global_p_ra_293.7131809005612_dec_-63.70184096161769/"
            "SKA_MID-AA0.5_SIM_custom_B2_random_pointing_nchan32_pt_actual.tar.gz"
        )
    )
    assert isinstance(table, PointingTable)

    times = []
    antennas = []
    pointings = []
    async with (aiokafka.AIOKafkaProducer(bootstrap_servers=KAFKA_HOST)) as producer:
        with (
            SubscribeEventValueContext(manual_qc, "time", EventType.CHANGE_EVENT, times.append),
            SubscribeEventValueContext(
                manual_qc, "antenna", EventType.CHANGE_EVENT, antennas.append
            ),
            SubscribeEventValueContext(
                manual_qc, "pointing", EventType.CHANGE_EVENT, pointings.append
            ),
        ):
            manual_qc.Start()
            await producer.send_and_wait(POINTING_OFFSET_TOPIC, encode(table))
            await asyncio.sleep(1)
            manual_qc.Stop()

    assert len(times) == 2
    np.set_printoptions(precision=None, formatter={"float_kind": str})
    np.testing.assert_array_equal(times[0], [0])
    np.testing.assert_array_equal(times[1], [4453425598.207603])

    assert len(antennas) == 2
    np.testing.assert_array_equal(antennas[0], [0, 0, 0, 0])
    np.testing.assert_array_equal(antennas[1], [0, 1, 2, 3])

    assert len(pointings) == 2
    np.testing.assert_array_equal(pointings[0], 0.0)
    np.testing.assert_array_equal(pointings[1], [0.002327204099604347, 0.0023270214564525965])
