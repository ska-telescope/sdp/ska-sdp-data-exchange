import asyncio
import datetime
import logging
from io import BytesIO

import aiokafka
import numpy as np
import pytest
from tango import DevState

import ska_sdp_lmc_queue_connector.astream_utils as astream
from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    InMemorySourceDescriptor,
    KafkaProducerSinkDescriptor,
    QueueConnectorDescriptor,
    TangoLocalAttributeSinkDescriptor,
    TangoSubscriptionSourceDescriptor,
)

KAFKA_HOST = "localhost:9092"
COMMANDED_TOPIC = "commanded-pointings"
ACTUAL_TOPIC = "actual-pointings"

FORMAT = "npy"
FREQ_HZ = 10.0
DISHES = 64  # AA2 Mid

# number of pointings samples to mock for each attribute
SAMPLES = 10

# allowed time in seconds for all data to appear in Kafka
TIMEOUT = 10.0


class Pointing:
    DTYPE = [
        ("dish", "int64"),
        ("timestamp", "datetime64[ns]"),
        ("az", np.double),
        ("el", np.double),
    ]

    @staticmethod
    def create(antenna_id: int):
        return np.array(
            [(antenna_id, datetime.datetime.now(), 0.5, -0.5)],
            dtype=Pointing.DTYPE,
        )

    @staticmethod
    def encode(data: np.ndarray, encoding: str) -> tuple[str, bytes]:
        match encoding:
            case "carray":
                return (
                    FORMAT,
                    data.tobytes(),
                )
            case "npy":
                bio = BytesIO()
                np.save(bio, data)
                return (FORMAT, bio.getbuffer())
            case _:
                raise ValueError(encoding)

    @staticmethod
    def decode(data: bytes, encoding: str) -> np.ndarray:
        match encoding:
            case "carray":
                return np.frombuffer(data, dtype=Pointing.DTYPE)
            case "npy":
                return np.load(BytesIO(data))
            case _:
                raise ValueError(data)


@pytest.fixture
def device_properties():
    DTYPE = np.dtype("bytes")
    SHAPE = []
    return {
        "exchanges_json": QueueConnectorDescriptor(
            exchanges=[
                # Mock Data
                ExchangeDescriptor(
                    dtype=DTYPE,
                    shape=SHAPE,
                    source=InMemorySourceDescriptor(
                        data=[Pointing.encode(Pointing.create(d), FORMAT) for i in range(SAMPLES)],
                        delay=1.0 / FREQ_HZ,
                    ),
                    sink=TangoLocalAttributeSinkDescriptor(
                        attribute_name=f"mock_pointing_{d}",
                        default_value=(FORMAT, b""),
                    ),
                )
                for d in range(DISHES)
            ]
            + [
                # Commanded Pointings
                ExchangeDescriptor(
                    dtype=DTYPE,
                    shape=SHAPE,
                    source=TangoSubscriptionSourceDescriptor(
                        device_name="test/sdp/1",
                        attribute_name=f"mock_pointing_{d}",
                    ),
                    sink=KafkaProducerSinkDescriptor(
                        servers=KAFKA_HOST,
                        topic=COMMANDED_TOPIC,
                        format=FORMAT,
                    ),
                )
                for d in range(DISHES)
            ]
            + [
                # Actual Pointings
                ExchangeDescriptor(
                    dtype=DTYPE,
                    shape=SHAPE,
                    source=TangoSubscriptionSourceDescriptor(
                        device_name="test/sdp/1",
                        attribute_name=f"mock_pointing_{d}",
                    ),
                    sink=KafkaProducerSinkDescriptor(
                        servers=KAFKA_HOST,
                        topic=ACTUAL_TOPIC,
                        format=FORMAT,
                    ),
                )
                for d in range(DISHES)
            ]
        ).model_dump_json_nodb(),
    }


@pytest.mark.integration_test
@pytest.mark.asyncio
async def test_pointing_pipeline(manual_qc):
    expected_pointings = DISHES * SAMPLES

    async def consume_pointings(consumer):
        count = 0
        # expecting the default values at start of stream
        async for data in astream.take(consumer, DISHES + expected_pointings):
            if len(data.value) != 0:
                value = Pointing.decode(data.value, FORMAT)
                assert value.dtype == Pointing.DTYPE
                count += 1
        return count

    async with (
        aiokafka.AIOKafkaConsumer(
            COMMANDED_TOPIC, bootstrap_servers=KAFKA_HOST
        ) as commanded_consumer,
        aiokafka.AIOKafkaConsumer(ACTUAL_TOPIC, bootstrap_servers=KAFKA_HOST) as actual_consumer,
    ):
        manual_qc.Start()
        try:
            commanded_count, actual_count = await asyncio.wait_for(
                asyncio.gather(
                    consume_pointings(commanded_consumer),
                    consume_pointings(actual_consumer),
                ),
                TIMEOUT,
            )
        finally:
            manual_qc.Stop()

        assert commanded_count == expected_pointings
        assert actual_count == expected_pointings
        assert manual_qc.State() == DevState.OFF
