import asyncio
from io import BytesIO

import aiokafka
import numpy as np
import numpy.lib.recfunctions as rfn
import pytest
from tango import DevState

from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    InMemorySourceDescriptor,
    KafkaProducerSinkDescriptor,
    QueueConnectorDescriptor,
    TangoLocalAttributeSinkDescriptor,
)
from ska_sdp_lmc_queue_connector.dataqueue_sourcesink import DataQueueProducerSinkDescriptor
from ska_sdp_lmc_queue_connector.pipe.buffer_pipe import BufferWithTimePipeDescriptor
from ska_sdp_lmc_queue_connector.tango_pointing_source import (
    TangoPointingSubscriptionSourceDescriptor,
)

KAFKA_HOST = "localhost:9092"
COMMANDED_TOPIC = "commanded-pointings"
ACTUAL_TOPIC = "actual-pointings"

FORMAT = "npy"
SAMPLE_FREQ_HZ = 10.0
DISH_NAMES = ["ska001", "ska002", "ska003", "ska004"]  # Up to AA2 Mid

# number of pointings samples to mock for each attribute
SAMPLE_COUNT = 10

# allowed time in seconds for all data to appear in Kafka
TIMEOUT = 10.0


DTYPE = np.dtype(
    [
        ("antenna_name", "<U6"),
        ("ts", "datetime64[ms]"),
        ("az", "float64"),
        ("el", "float64"),
    ]
)
SHAPE = [-1]  # arbitrary sized array of structs
Y2000_TAI_EPOCH_UNIX_MS = 946684800000

S_TO_MS = 1000


@pytest.fixture
def device_properties(data_queue_producer_impl: type):
    return {
        "exchanges_json": QueueConnectorDescriptor(
            exchanges=[
                # Mock Data
                ExchangeDescriptor(
                    dtype=np.dtype(float),
                    shape=[3 * SAMPLE_COUNT],
                    source=InMemorySourceDescriptor(
                        data=[
                            np.array(
                                list([i / SAMPLE_FREQ_HZ, 0.5, 0.5] for i in range(SAMPLE_COUNT))
                            ).flatten(),
                        ],
                        delay=1.0 / SAMPLE_FREQ_HZ,
                    ),
                    sink=TangoLocalAttributeSinkDescriptor(
                        attribute_name=f"mock_pointing_{dish_name}",
                        default_value="0",
                    ),
                )
                for dish_name in DISH_NAMES
            ]
            + [
                # Commanded Pointings
                ExchangeDescriptor(
                    dtype=DTYPE,
                    shape=SHAPE,
                    source=[
                        TangoPointingSubscriptionSourceDescriptor(
                            device_name="test/sdp/1",
                            attribute_name=f"mock_pointing_{dish_name}",
                            antenna_name=f"{dish_name}",
                        )
                        for dish_name in DISH_NAMES
                    ],
                    pipe=BufferWithTimePipeDescriptor(timespan=1.0, flatten=True),
                    sink=data_queue_producer_impl(
                        servers=KAFKA_HOST,
                        topic=COMMANDED_TOPIC,
                        format=FORMAT,
                    ),
                ),
                # Actual Pointings
                ExchangeDescriptor(
                    dtype=DTYPE,
                    shape=SHAPE,
                    source=[
                        TangoPointingSubscriptionSourceDescriptor(
                            device_name="test/sdp/1",
                            attribute_name=f"mock_pointing_{dish_name}",
                            antenna_name=f"{dish_name}",
                        )
                        for dish_name in DISH_NAMES
                    ],
                    pipe=BufferWithTimePipeDescriptor(timespan=1.0, flatten=True),
                    sink=data_queue_producer_impl(
                        servers=KAFKA_HOST,
                        topic=ACTUAL_TOPIC,
                        format=FORMAT,
                    ),
                ),
            ]
        ).model_dump_json_nodb(),
    }


@pytest.fixture()
def expected_pointings() -> np.ndarray:
    num_dishes = len(DISH_NAMES)
    dishes = np.array([dish_name for dish_name in DISH_NAMES], dtype=[DTYPE.descr[0]])
    times = np.array(
        np.arange(
            Y2000_TAI_EPOCH_UNIX_MS,
            Y2000_TAI_EPOCH_UNIX_MS + (SAMPLE_COUNT / SAMPLE_FREQ_HZ * S_TO_MS),
            1.0 / SAMPLE_FREQ_HZ * S_TO_MS,
        ),
        dtype=[DTYPE.descr[1]],
    )
    dish_grid, time_grid = np.meshgrid(dishes, times, indexing="ij")
    return np.hstack(
        (
            # default pointing values
            rfn.merge_arrays(
                [
                    dish_grid.T.flatten(),
                    np.full(
                        shape=time_grid.T.flatten().shape,
                        fill_value=Y2000_TAI_EPOCH_UNIX_MS,
                        dtype=[DTYPE.descr[1]],
                    ),
                    np.array(np.zeros(num_dishes * SAMPLE_COUNT) * 0.5, dtype=[DTYPE.descr[2]]),
                    np.array(np.zeros(num_dishes * SAMPLE_COUNT) * 0.5, dtype=[DTYPE.descr[3]]),
                ]
            ),
            # pointing values per timestep
            rfn.merge_arrays(
                [
                    dish_grid.T.flatten(),
                    time_grid.T.flatten(),
                    np.array(np.ones(num_dishes * SAMPLE_COUNT) * 0.5, dtype=[DTYPE.descr[2]]),
                    np.array(np.ones(num_dishes * SAMPLE_COUNT) * 0.5, dtype=[DTYPE.descr[3]]),
                ]
            ),
        )
    )


async def _consume_pointings(consumer, expected_pointing_count: int):
    message_count = 0
    pointing_count = 0
    pointings = []

    # expecting the default values at start of stream
    async for record in consumer:
        if len(record.value) != 0:
            value = np.load(BytesIO(record.value))
            assert value.dtype == DTYPE
            message_count += 1
            pointings.append(value)
            pointing_count += len(value)
        if pointing_count >= expected_pointing_count:
            break

    return message_count, pointing_count, np.concatenate(pointings).ravel()


@pytest.mark.integration_test
@pytest.mark.asyncio
@pytest.mark.parametrize(
    "data_queue_producer_impl",
    [DataQueueProducerSinkDescriptor, KafkaProducerSinkDescriptor],
)
async def test_pointing_pipeline(manual_qc, expected_pointings):
    # default multiarray samples are also zeros
    expected_pointings_count = len(DISH_NAMES) * (SAMPLE_COUNT * 2)
    assert expected_pointings_count == len(expected_pointings)

    async with (
        aiokafka.AIOKafkaConsumer(
            COMMANDED_TOPIC, bootstrap_servers=KAFKA_HOST
        ) as commanded_consumer,
        aiokafka.AIOKafkaConsumer(ACTUAL_TOPIC, bootstrap_servers=KAFKA_HOST) as actual_consumer,
    ):
        manual_qc.Start()
        consume_tasks = asyncio.gather(
            _consume_pointings(commanded_consumer, expected_pointings_count),
            _consume_pointings(actual_consumer, expected_pointings_count),
        )
        commanded, actual = await asyncio.wait_for(
            consume_tasks,
            TIMEOUT,
        )
        manual_qc.Stop()

        for kind in [commanded, actual]:
            messages_count, pointings_count, pointings = kind
            assert messages_count < expected_pointings_count
            assert pointings_count == expected_pointings_count
            np.testing.assert_array_equal(
                np.sort(pointings), np.sort(expected_pointings), strict=True
            )

        assert manual_qc.State() == DevState.OFF
