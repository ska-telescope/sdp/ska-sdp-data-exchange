"""
Integration tests for a single beam jones matrix pipeline.
"""

import asyncio
from contextlib import ExitStack

import numpy as np
import pytest
import pytest_asyncio
from aiokafka.admin import AIOKafkaAdminClient
from tango import EventType

import ska_sdp_lmc_queue_connector.astream_utils as astream
from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    InMemorySourceDescriptor,
    KafkaConsumerSourceDescriptor,
    KafkaProducerSinkDescriptor,
    QueueConnectorDescriptor,
)
from ska_sdp_lmc_queue_connector.dataqueue_sourcesink import (
    DataQueueConsumerSourceDescriptor,
    DataQueueProducerSinkDescriptor,
)
from ska_sdp_lmc_queue_connector.tango_object_scatter_sink import (
    TangoAttributeDescriptor,
    TangoObjectScatterAttributeSinkDescriptor,
)
from tests.tango_helper import AsyncQueueIterator, SubscribeEventContext

KAFKA_HOST = "localhost:9092"
TEST_TOPIC = "qametrics"

DTYPE = np.dtype(object)
SHAPE = []


@pytest_asyncio.fixture(name="clear_topics", autouse=True)
async def clear_topics_fixture():
    admin_client = AIOKafkaAdminClient(bootstrap_servers=KAFKA_HOST)
    await admin_client.start()
    topics = await admin_client.list_topics()
    to_remove = list({TEST_TOPIC} & set(topics))
    await admin_client.delete_topics(to_remove)
    await admin_client.close()


@pytest.fixture
def device_properties(data_queue_producer_impl: type, data_queue_consumer_impl: type):
    return {
        "exchanges_json": QueueConnectorDescriptor(
            exchanges=[
                ExchangeDescriptor(
                    dtype=DTYPE,
                    source=InMemorySourceDescriptor(
                        data=[
                            {
                                "type": "update",
                                "state": "receiving",
                                "scan_id": 12,
                                "payloads_received": 3,
                                "data_count": 1,
                                "error_count": 0,
                                "time_since_last_payload": 2,
                                "time": 1679904337.9817705,
                                "processing_block_id": "pb-test-20230327-93787",
                                "streams": [{"id": "0", "heaps": 7}],
                            },
                            {
                                "type": "update",
                                "state": "receiving",
                                "scan_id": 13,
                                "payloads_received": 3,
                                "data_count": 1,
                                "error_count": 0,
                                "time_since_last_payload": 2,
                                "time": 1679904337.9817705,
                                "processing_block_id": "pb-test-20230327-93787",
                                "streams": [{"id": "1", "heaps": 11}],
                            },
                            {
                                "type": "log",
                                "message": "message",
                            },
                        ],
                        delay=0,
                    ),
                    sink=data_queue_producer_impl(
                        servers=KAFKA_HOST,
                        topic=TEST_TOPIC,
                        format="json",
                    ),
                ),
                ExchangeDescriptor(
                    dtype=DTYPE,
                    source=data_queue_consumer_impl(
                        servers=KAFKA_HOST,
                        topic=TEST_TOPIC,
                        format="json",
                    ),
                    sink=TangoObjectScatterAttributeSinkDescriptor(
                        attributes=[
                            TangoAttributeDescriptor(
                                attribute_name="receive_state",
                                filter="type=='update'",
                                path="state",
                                dtype=np.dtype(str),
                                default_value="",
                            ),
                            TangoAttributeDescriptor(
                                attribute_name="scan_id",
                                filter="type=='update'",
                                path="scan_id",
                                dtype=np.dtype(int),
                                default_value=0,
                            ),
                            TangoAttributeDescriptor(
                                attribute_name="stream_0_heaps",
                                filter=(
                                    "type=='update' && streams != undefined && "
                                    "contains(streams[].id, '0')"
                                ),
                                path="streams[].heaps | [0]",
                                dtype=np.dtype(float),
                                default_value=0,
                            ),
                        ]
                    ),
                ),
            ]
        ).model_dump_json_nodb(),
    }


@pytest.mark.integration_test
@pytest.mark.parametrize(
    "data_queue_producer_impl,data_queue_consumer_impl",
    [
        (DataQueueProducerSinkDescriptor, DataQueueConsumerSourceDescriptor),
        (KafkaProducerSinkDescriptor, KafkaConsumerSourceDescriptor),
    ],
)
def test_qametrics_defaults(manual_qc):
    assert manual_qc.receive_state == ""
    assert manual_qc.scan_id == 0
    assert manual_qc.stream_0_heaps == 0


@pytest.mark.integration_test
@pytest.mark.asyncio
@pytest.mark.parametrize(
    "data_queue_producer_impl,data_queue_consumer_impl",
    [
        (DataQueueProducerSinkDescriptor, DataQueueConsumerSourceDescriptor),
        (KafkaProducerSinkDescriptor, KafkaConsumerSourceDescriptor),
    ],
)
@pytest.mark.parametrize(
    "names,expected_events",
    [
        (["receive_state"], 3),
        (["scan_id"], 3),
        (["stream_0_heaps"], 2),
        (["receive_state", "scan_id", "stream_0_heaps"], 8),
    ],
)
async def test_qametrics_pipeline(manual_qc, names: list[str], expected_events: int):
    loop = asyncio.get_running_loop()
    queue = asyncio.Queue()

    def append_queue(value):
        asyncio.run_coroutine_threadsafe(queue.put(value), loop)

    with ExitStack() as stack:
        for name in names:
            stack.enter_context(
                SubscribeEventContext(manual_qc, name, EventType.CHANGE_EVENT, append_queue)
            )
        manual_qc.Start()
        actual_events = 0
        try:
            async for event in astream.timeout(AsyncQueueIterator(queue), 1.0):
                actual_events += 1
        except asyncio.TimeoutError:
            pass
        finally:
            manual_qc.Stop()

        assert expected_events == actual_events
