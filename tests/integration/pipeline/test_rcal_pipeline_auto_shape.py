"""
Integration tests for a single beam jones matrix pipeline.
"""

import asyncio
import logging
from contextlib import ExitStack
from functools import partial

import numpy as np
import pytest
from tango import EventType

from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    InMemorySourceDescriptor,
    KafkaConsumerSourceDescriptor,
    KafkaProducerSinkDescriptor,
    QueueConnectorDescriptor,
    TangoArrayScatterAttributeSinkDescriptor,
)
from tests.manual_sdp_queue_connector import ManualSDPQueueConnector
from tests.tango_helper import SubscribeEventTensorContext, range_split

KAFKA_HOST = "localhost:9092"

DTYPE = np.float32
FORMAT = "carray"
BEAM_ID = 0
MAX_MID_SHAPE = np.array([1, 4096, 197, 4, 2])
MAX_LOW_SHAPE = np.array([1, 4096, 512, 4, 2])


@pytest.fixture()
def devices_to_load(data: np.ndarray, shape: np.ndarray, max_channel_chunk_size: int):
    flat_shape = [int(shape.prod())]
    flat_data = [data.flatten()] if isinstance(data, np.ndarray) else data
    channel_ranges = _chunkify(shape[1], max_channel_chunk_size)

    return (
        {
            "class": ManualSDPQueueConnector,
            "devices": [
                {
                    "name": "test/sdp/1",
                    "properties": {
                        "exchanges_json": QueueConnectorDescriptor(
                            exchanges=[
                                ExchangeDescriptor(
                                    dtype=DTYPE,
                                    shape=shape.tolist(),
                                    source=InMemorySourceDescriptor(
                                        data=flat_data,
                                        delay=0,
                                    ),
                                    sink=KafkaProducerSinkDescriptor(
                                        servers=KAFKA_HOST,
                                        topic="jones",
                                        format=FORMAT,
                                        message_max_bytes=flat_shape[0] * np.dtype(DTYPE).itemsize,
                                    ),
                                )
                            ]
                        ).model_dump_json_nodb(),
                    },
                },
                {
                    "name": "test/sdp/2",
                    "properties": {
                        "exchanges_json": QueueConnectorDescriptor(
                            exchanges=[
                                ExchangeDescriptor(
                                    dtype=DTYPE,
                                    shape=shape.tolist(),
                                    source=KafkaConsumerSourceDescriptor(
                                        servers=KAFKA_HOST,
                                        topic="jones",
                                        format=FORMAT,
                                    ),
                                    sink=TangoArrayScatterAttributeSinkDescriptor(
                                        attribute_names=[
                                            f"jones_{BEAM_ID}_{c}"
                                            for c in range(len(channel_ranges))
                                        ],
                                        attribute_shape_names=[
                                            f"jones_shape_{BEAM_ID}_{c}"
                                            for c in range(len(channel_ranges))
                                        ],
                                        indices=[r.stop for r in channel_ranges[:-1]],
                                        axis=1,
                                        default_value=0.0,
                                    ),
                                ),
                            ]
                        ).model_dump_json_nodb(),
                    },
                },
            ],
        },
    )


@pytest.mark.parametrize(
    "data,shape",
    [
        ([1.0, 2.0, 3.0, 4.0], MAX_MID_SHAPE),
        # TODO(yan-1406)
        # ([1.0, 2.0, 3.0, 4.0], MAX_LOW_SHAPE),
    ],
)
@pytest.mark.parametrize(
    "max_channel_chunk_size",
    [
        # TODO(yan-1406)
        # 4096,
        # 512,
        256,
        # 200
    ],
)
@pytest.mark.integration_test
@pytest.mark.asyncio
async def test_rcal_pipeline(tango_context, data, shape, max_channel_chunk_size):
    logging.debug(tango_context)
    loop = asyncio.get_running_loop()
    final_value: np.ndarray = np.array([])
    dev1 = tango_context.get_device("test/sdp/1")
    dev2 = tango_context.get_device("test/sdp/2")

    channel_ranges = _chunkify(shape[1], max_channel_chunk_size)
    num_channel_chunks = len(channel_ranges)
    queues = [asyncio.Queue() for _ in range(num_channel_chunks)]

    def append_queue(value, chunk):
        asyncio.run_coroutine_threadsafe(queues[chunk].put(value), loop)

    with ExitStack() as stack:
        event_contexts = [
            SubscribeEventTensorContext(
                dev2,
                f"jones_{BEAM_ID}_{c}",
                f"jones_shape_{BEAM_ID}_{c}",
                EventType.CHANGE_EVENT,
                partial(append_queue, chunk=c),
            )
            for c in range(num_channel_chunks)
        ]
        for mgr in event_contexts:
            stack.enter_context(mgr)

        dev2.Start()
        dev1.Start()
        expected = 2 if isinstance(data, np.ndarray) else len(data) + 1
        chunk = 0
        for _ in range(expected):
            final_value = await asyncio.wait_for(queues[chunk].get(), timeout=5)
        dev1.Stop()
        dev2.Stop()

    chunk = 0
    if not isinstance(data, np.ndarray):
        channel_chunk_size = len(channel_ranges[0])
        chunked_shape = np.array([shape[0], channel_chunk_size, shape[2], shape[3], shape[4]])
        data = np.full(shape=chunked_shape, fill_value=data[-1], dtype=DTYPE)
    np.testing.assert_array_equal(final_value, data, strict=True)


def _chunkify(elements, max_chunk_size) -> list[range]:
    return range_split(range(elements), int(np.ceil(elements / max_chunk_size)))
