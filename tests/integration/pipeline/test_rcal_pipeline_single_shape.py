import asyncio
import logging

import numpy as np
import pytest
from tango import EventType

from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    InMemorySourceDescriptor,
    KafkaConsumerSourceDescriptor,
    KafkaProducerSinkDescriptor,
    QueueConnectorDescriptor,
    TangoLocalAttributeSinkDescriptor,
)
from tests.manual_sdp_queue_connector import ManualSDPQueueConnector
from tests.tango_helper import SubscribeEventTensorContext

KAFKA_HOST = "localhost:9092"

DTYPE = np.float32
FORMAT = "carray"
AA05_SHAPE = np.array([1, 512, 4, 4, 2])
AA2_SHAPE = np.array([1, 512, 16, 4, 2])
MID_SHAPE = np.array([1, 512, 197, 4, 2])
LOW_SHAPE = np.array([1, 512, 512, 4, 2])
MAX_LOW_SHAPE = np.array([1, 4096, 512, 4, 2])


@pytest.fixture()
def devices_to_load(data: np.ndarray, shape: np.ndarray):
    flat_shape = [int(shape.prod())]
    flat_data = [data.flatten()] if isinstance(data, np.ndarray) else data

    return (
        {
            "class": ManualSDPQueueConnector,
            "devices": [
                {
                    "name": "test/sdp/1",
                    "properties": {
                        "exchanges_json": QueueConnectorDescriptor(
                            exchanges=[
                                ExchangeDescriptor(
                                    dtype=DTYPE,
                                    shape=flat_shape,
                                    source=InMemorySourceDescriptor(
                                        data=flat_data,
                                        delay=0,
                                    ),
                                    sink=KafkaProducerSinkDescriptor(
                                        servers=KAFKA_HOST,
                                        topic="jones_0_0",
                                        format=FORMAT,
                                        message_max_bytes=flat_shape[0] * np.dtype(DTYPE).itemsize,
                                    ),
                                )
                            ]
                        ).model_dump_json_nodb(),
                    },
                },
                {
                    "name": "test/sdp/2",
                    "properties": {
                        "exchanges_json": QueueConnectorDescriptor(
                            exchanges=[
                                ExchangeDescriptor(
                                    dtype=np.uint32,
                                    shape=[len(shape)],
                                    source=InMemorySourceDescriptor(
                                        data=[],
                                        delay=0,
                                    ),
                                    sink=TangoLocalAttributeSinkDescriptor(
                                        attribute_name="jones_shape_0_0",
                                        default_value=shape,
                                    ),
                                ),
                                ExchangeDescriptor(
                                    dtype=DTYPE,
                                    shape=flat_shape,
                                    source=KafkaConsumerSourceDescriptor(
                                        servers=KAFKA_HOST,
                                        topic="jones_0_0",
                                        format=FORMAT,
                                    ),
                                    sink=TangoLocalAttributeSinkDescriptor(
                                        attribute_name="jones_0_0",
                                        default_value=0.0,
                                    ),
                                ),
                            ]
                        ).model_dump_json_nodb(),
                    },
                },
            ],
        },
    )


@pytest.mark.parametrize(
    "data,shape",
    [
        pytest.param(np.random.rand(*AA05_SHAPE).astype(DTYPE), AA05_SHAPE),
        pytest.param(np.random.rand(*AA2_SHAPE).astype(DTYPE), AA2_SHAPE),
        pytest.param([1.0, 2.0, 3.0, 4.0], MID_SHAPE),
        pytest.param([1.0, 2.0, 3.0, 4.0], LOW_SHAPE),
        pytest.param(
            [1.0, 2.0, 3.0, 4.0], MAX_LOW_SHAPE, marks=pytest.mark.skip("kafka crash on CI")
        ),
    ],
)
@pytest.mark.integration_test
@pytest.mark.asyncio
async def test_rcal_pipeline(tango_context, data, shape):
    logging.debug(tango_context)
    q = asyncio.Queue()
    loop = asyncio.get_running_loop()
    final_value: np.ndarray = np.array([])
    dev1 = tango_context.get_device("test/sdp/1")
    dev2 = tango_context.get_device("test/sdp/2")

    def append_queue(value):
        asyncio.run_coroutine_threadsafe(q.put(value), loop)

    with SubscribeEventTensorContext(
        dev2,
        "jones_0_0",
        "jones_shape_0_0",
        EventType.CHANGE_EVENT,
        append_queue,
    ):
        dev2.Start()
        dev1.Start()
        expected = 2 if isinstance(data, np.ndarray) else len(data) + 1
        for _ in range(expected):
            final_value = await asyncio.wait_for(q.get(), timeout=5)
        dev1.Stop()
        dev2.Stop()

    if not isinstance(data, np.ndarray):
        data = np.full(shape=shape, fill_value=data[-1], dtype=DTYPE)
    np.testing.assert_array_equal(final_value, data, strict=True)
