import asyncio

import numpy as np
import pytest

from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    InMemorySinkDescriptor,
    InMemorySourceDescriptor,
    KafkaConsumerSourceDescriptor,
    KafkaProducerSinkDescriptor,
    QueueConnectorDescriptor,
)
from ska_sdp_lmc_queue_connector.in_memory_sourcesink import InMemorySink

KAFKA_HOST = "localhost:9092"
TEST_TOPIC = "structured-data"
TIMEOUT = 5


@pytest.fixture
def device_properties(exchange_descriptors: list[ExchangeDescriptor]):
    descriptor = QueueConnectorDescriptor(exchanges=exchange_descriptors)
    return {"exchanges_json": descriptor.model_dump_json_nodb()}


VARIABLE_DTYPE = np.dtype(
    [
        ("antenna_id", "int32"),
        ("ts", "datetime64[ns]"),
        ("az", "float64"),
        ("el", "float64"),
    ]
)
VARIABLE_POINTINGS = [
    [
        (1, "2023-01-01T00:00Z", 0.25, 0.35),
    ],
    [
        (2, "2023-01-01T00:01Z", 0.26, 0.36),
        (3, "2023-01-01T00:02Z", 0.27, 0.37),
        (4, "2023-01-01T00:03Z", 0.28, 0.38),
    ],
]


@pytest.mark.integration_test
@pytest.mark.asyncio
@pytest.mark.parametrize(
    "exchange_descriptors",
    [
        [
            ExchangeDescriptor(
                dtype=VARIABLE_DTYPE,
                source=InMemorySourceDescriptor(
                    delay=0.1,
                    data=VARIABLE_POINTINGS,
                ),
                sink=KafkaProducerSinkDescriptor(
                    servers=KAFKA_HOST,
                    topic=TEST_TOPIC,
                    format="npy",
                ),
            ),
            ExchangeDescriptor(
                dtype=VARIABLE_DTYPE,
                source=KafkaConsumerSourceDescriptor(
                    servers=KAFKA_HOST,
                    topic=TEST_TOPIC,
                    format="npy",
                ),
                sink=InMemorySinkDescriptor(
                    key="variable",
                ),
            ),
        ]
    ],
)
async def test_variable_length_using_npy_encoding(manual_qc):
    manual_qc.Start()
    await asyncio.sleep(0.5)
    manual_qc.Stop()

    messages = InMemorySink.get_queue("variable")

    data: np.ndarray = messages.get_nowait()
    assert data.shape == (1,)
    assert data[0]["antenna_id"] == 1
    assert data[0]["az"] == 0.25

    data = messages.get_nowait()
    assert data.shape == (3,)
    assert data[2]["antenna_id"] == 4
    assert data[2]["el"] == 0.38

    with pytest.raises(asyncio.QueueEmpty):
        messages.get_nowait()


# DateTime isn't supported when serialising using carray
# https://github.com/numpy/numpy/issues/4983
FIXED_DTYPE = np.dtype(
    [
        ("antenna_id", "int32"),
        ("az", "float64"),
        ("el", "float64"),
    ]
)
FIXED_POINTINGS = [
    [(1, 0.25, 0.35), (2, 0.26, 0.36)],
    [(3, 0.27, 0.37), (4, 0.28, 0.38)],
]


@pytest.mark.integration_test
@pytest.mark.asyncio
@pytest.mark.parametrize(
    "exchange_descriptors",
    [
        [
            ExchangeDescriptor(
                dtype=FIXED_DTYPE,
                shape=[2],
                source=InMemorySourceDescriptor(
                    delay=0.1,
                    data=FIXED_POINTINGS,
                ),
                sink=KafkaProducerSinkDescriptor(
                    servers=KAFKA_HOST,
                    topic=TEST_TOPIC,
                    format="carray",
                ),
            ),
            ExchangeDescriptor(
                dtype=FIXED_DTYPE,
                shape=[2],
                source=KafkaConsumerSourceDescriptor(
                    servers=KAFKA_HOST,
                    topic=TEST_TOPIC,
                    format="carray",
                ),
                sink=InMemorySinkDescriptor(
                    key="fixed",
                ),
            ),
        ]
    ],
)
async def test_fixed_length_using_carray_encoding(manual_qc):
    manual_qc.Start()
    await asyncio.sleep(0.5)
    manual_qc.Stop()

    messages = InMemorySink.get_queue("fixed")

    data = messages.get_nowait()
    assert isinstance(data, np.ndarray)
    assert data.shape == (2,)
    assert data[0]["antenna_id"] == 1
    assert data[0]["az"] == 0.25

    data = messages.get_nowait()
    assert isinstance(data, np.ndarray)
    assert data.shape == (2,)
    assert data[1]["antenna_id"] == 4
    assert data[1]["el"] == 0.38

    with pytest.raises(asyncio.QueueEmpty):
        messages.get_nowait()
