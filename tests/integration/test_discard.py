import asyncio

import pytest
import pytest_asyncio
from aiokafka import AIOKafkaProducer
from aiokafka.admin import AIOKafkaAdminClient

from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    InMemorySourceDescriptor,
    KafkaProducerSinkDescriptor,
    QueueConnectorDescriptor,
    TangoLocalAttributeSinkDescriptor,
    TangoSubscriptionSourceDescriptor,
)

KAFKA_HOST = "localhost:9092"
SINK_TOPIC = "integration_sink1"


@pytest_asyncio.fixture(name="producer")
async def producer_fixture():
    producer = AIOKafkaProducer(bootstrap_servers=KAFKA_HOST)
    await producer.start()
    yield producer
    await producer.stop()


@pytest.fixture
def device_properties():
    return {
        "exchanges_json": QueueConnectorDescriptor(
            exchanges=[
                ExchangeDescriptor(
                    dtype="int16",
                    source=InMemorySourceDescriptor(
                        data=[1, 2, 3, 4, 5],
                        delay=0.1,
                    ),
                    sink=TangoLocalAttributeSinkDescriptor(
                        attribute_name="value",
                        default_value=0,
                    ),
                ),
                ExchangeDescriptor(
                    dtype="int16",
                    source=TangoSubscriptionSourceDescriptor(
                        device_name="test/sdp/1",
                        attribute_name="value",
                    ),
                    sink=KafkaProducerSinkDescriptor(servers=KAFKA_HOST, topic=SINK_TOPIC),
                ),
            ]
        ).model_dump_json_nodb(),
    }


@pytest_asyncio.fixture(name="clear_topics")
async def clear_topics_fixture():
    admin_client = AIOKafkaAdminClient(bootstrap_servers=KAFKA_HOST)
    await admin_client.start()
    topics = await admin_client.list_topics()
    to_remove = list({SINK_TOPIC} & set(topics))
    await admin_client.delete_topics(to_remove)
    await admin_client.close()


@pytest.mark.integration_test
@pytest.mark.asyncio
async def test_kafka_discard(
    clear_topics,  # pylint: disable=unused-argument
    manual_qc,
):
    manual_qc.Start()
    await asyncio.sleep(0.1)
    manual_qc.Stop()
