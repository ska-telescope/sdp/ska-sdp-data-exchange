import asyncio

import pytest
from aiokafka import AIOKafkaConsumer, AIOKafkaProducer

from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    InMemorySourceDescriptor,
    KafkaConsumerSourceDescriptor,
    KafkaProducerSinkDescriptor,
    QueueConnectorDescriptor,
    TangoLocalAttributeSinkDescriptor,
    TangoSubscriptionSourceDescriptor,
)

KAFKA_HOST = "localhost:9092"
SOURCE_TOPIC = "integration_source"
SINK_TOPIC = "integration_sink"


@pytest.fixture
def device_properties():
    return {
        "exchanges_json": QueueConnectorDescriptor(
            exchanges=[
                ExchangeDescriptor(
                    dtype="str",
                    source=InMemorySourceDescriptor(
                        data=["b", "c", "d", "e", "f"],
                        delay=0.1,
                    ),
                    sink=TangoLocalAttributeSinkDescriptor(
                        attribute_name="value",
                        default_value="a",
                    ),
                ),
                ExchangeDescriptor(
                    dtype="str",
                    source=TangoSubscriptionSourceDescriptor(
                        device_name="test/sdp/1",
                        attribute_name="value",
                    ),
                    sink=KafkaProducerSinkDescriptor(servers=KAFKA_HOST, topic=SINK_TOPIC),
                ),
                ExchangeDescriptor(
                    dtype="str",
                    source=KafkaConsumerSourceDescriptor(servers=KAFKA_HOST, topic=SOURCE_TOPIC),
                    sink=TangoLocalAttributeSinkDescriptor(
                        attribute_name="from_kafka",
                        default_value="a",
                    ),
                ),
            ]
        ).model_dump_json_nodb(),
    }


@pytest.mark.xfail(reason="intermittent segmentation fault")
@pytest.mark.integration_test
@pytest.mark.asyncio
async def test_kafka_topic_to_local_tango_attribute(manual_qc):
    attr_helper = _DeviceProxyAttributeTestHelper(manual_qc, "from_kafka")
    assert attr_helper.attr_value == "a"

    async with AIOKafkaProducer(bootstrap_servers=KAFKA_HOST) as producer:
        # Nothing will happen until start is called
        await producer.send_and_wait(SOURCE_TOPIC, b"'test message'")
        await attr_helper.assert_doesnt_change(2)

        manual_qc.Start()
        await producer.send_and_wait(SOURCE_TOPIC, b"'after start message'")
        await attr_helper.assert_becomes_value("after start message", timeout=2, poll_rate=0.5)

        # Once the device is stopped, sources won't be tracked any more
        manual_qc.Stop()
        await producer.send_and_wait(SOURCE_TOPIC, b"'after stop message'")
        await attr_helper.assert_doesnt_change(2)


@pytest.mark.integration_test
@pytest.mark.asyncio
async def test_tango_subscription_to_kafka_topic(manual_qc):
    assert manual_qc.is_attribute_polled("value") is False
    messages = []

    async with AIOKafkaConsumer(SINK_TOPIC, bootstrap_servers=KAFKA_HOST) as consumer:
        # Enque output messages in the background with asyncio
        async def append_messages():
            async for msg in consumer:
                messages.append(msg.value)

        task = asyncio.create_task(append_messages())
        await asyncio.sleep(0.1)

        # Nothing should happen until start is called
        assert messages == []

        manual_qc.Start()

        # Default value sent on subscription
        await asyncio.sleep(0.05)
        assert messages == [b"'a'"]

        await asyncio.sleep(0.1)
        assert messages == [b"'a'", b"'b'"]

        # Once the device is stopped sources are no longer tracked
        manual_qc.Stop()

        await consumer.stop()
        await task


class _DeviceProxyAttributeTestHelper:
    """
    A simple helper class for verifying the value of an attribute on a device
    proxy
    """

    def __init__(self, proxy, attribute: str) -> None:
        self._proxy = proxy
        self._attribute = attribute

    @property
    def attr_value(self):
        attr = self._proxy.read_attribute(self._attribute)
        return attr.value

    async def assert_doesnt_change(self, wait_time):
        before = self.attr_value
        await asyncio.sleep(wait_time)
        assert before == self.attr_value

    async def assert_becomes_value(self, new_value, timeout=10, poll_rate=0.5):
        try:
            await asyncio.wait_for(self._poll_until_is_value(new_value, poll_rate), timeout)
        except TimeoutError:
            pass
        finally:
            assert self.attr_value == new_value

    async def _poll_until_is_value(self, target_value: str, poll_rate: float):
        while self.attr_value != target_value:
            await asyncio.sleep(poll_rate)
