import asyncio

import numpy as np
import pytest

from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    InMemorySinkDescriptor,
    InMemorySourceDescriptor,
    QueueConnectorDescriptor,
    TangoLocalAttributeSinkDescriptor,
    TangoSubscriptionSourceDescriptor,
)
from ska_sdp_lmc_queue_connector.in_memory_sourcesink import InMemorySink


@pytest.fixture
def device_properties(dtype, shape, data):
    return {
        "exchanges_json": QueueConnectorDescriptor(
            exchanges=[
                ExchangeDescriptor(
                    dtype=dtype,
                    shape=shape,
                    source=InMemorySourceDescriptor(
                        data=data,
                        delay=0.02,
                    ),
                    sink=TangoLocalAttributeSinkDescriptor(
                        attribute_name="testattr",
                        default_value=DEFAULT_VALUES[dtype],
                    ),
                ),
                ExchangeDescriptor(
                    dtype=dtype,
                    shape=shape,
                    source=TangoSubscriptionSourceDescriptor(
                        device_name="test/sdp/1",
                        attribute_name="testattr",
                    ),
                    sink=InMemorySinkDescriptor(key="1"),
                ),
                ExchangeDescriptor(
                    dtype=dtype,
                    shape=shape,
                    source=TangoSubscriptionSourceDescriptor(
                        device_name="test/sdp/1",
                        attribute_name="testattr",
                    ),
                    sink=InMemorySinkDescriptor(key="2"),
                ),
            ]
        ).model_dump_json_nodb(),
    }


DEFAULT_VALUES = {
    str: "",
    bytes: ("utf-8", b""),
    np.int16: 0,
    np.uint16: 0,
    int: 0,
    float: 0.0,
}


def expected_value_type(dtype: type, shape: list) -> type:
    if dtype == bytes:
        # configuring with an encoded dtype in-turn results in
        # (encoding, bytearray) tuple on tango attributes
        return tuple
    if shape == []:
        # tango converts scalars to python native types
        if np.issubdtype(dtype, np.integer):
            return int
        if np.issubdtype(dtype, np.floating):
            return float
    return dtype


# fmt: off
# flake8: noqa
@pytest.mark.parametrize("dtype,shape,data", [
    (bytes,     [], [('utf-8', b'example'), ('utf-8', b'data')]),
    (str,       [], ["test", "strings"]),
    (str,       [2,2], [[["image", "of"],["test", "strings"]]]),
    (str,       [2,2], ["image", "test"]),
    (int,       [], [1,2,3],),
    (np.int16,  [], [-1,2,3],),
    (np.uint16, [], [1,2,3],),
    (float,     [], [1.,2.,3.],),
    (float,     [2], [[1., 1.], [2., 2.]],),
    (float,     [2,2], [1., 2.],),
])
# fmt: on
@pytest.mark.integration_test
@pytest.mark.asyncio
async def test_tango_subscription_types(manual_qc, dtype, shape, data):
    manual_qc.Start()
    await asyncio.sleep(0.1)
    manual_qc.Stop()

    def test_queue(q):
        actuals = []
        while not q.empty():
            actuals.append(q.get_nowait())

        for value, expected in zip(data, data):
            np.testing.assert_array_equal(value, expected, strict=True)

            # check types
            if dtype is bytes:
                assert isinstance(value[0], str)
                assert isinstance(value[1], bytes)
            else:
                assert np.issubdtype(
                    np.array(value).dtype,
                    expected_value_type(dtype, shape)
                )

    test_queue(InMemorySink.get_queue(key='1'))
    test_queue(InMemorySink.get_queue(key='2'))
