from overrides import override
from tango import ArgType
from tango.server import command, device_property

from ska_sdp_lmc_queue_connector import SDPQueueConnector
from tests.tango_helper import tango_command_guard


class ManualSDPQueueConnector(SDPQueueConnector):
    """
    SDPQueueConnector with added commands for manual monitoring
    and configuring outside of a configuration database context.
    """

    exchanges_json: str = device_property(
        dtype=ArgType.DevString, default_value=""
    )  # type: ignore
    """JSON configuration conforming to :class:`QueueConnectorDescriptor`"""

    @override
    async def _configure_from_property(self):
        if self.exchanges_json:
            await self.group_configure(self.exchanges_json)
        else:
            await super()._configure_from_property()

    def group_state_message(self, group_name: str):
        if group_name in self._group_states:
            message = (
                f"when group {group_name} is in the {self._group_states.get(group_name)} state"
            )
        else:
            message = f"when group {group_name} has not been registered"
        return message

    @command(fisallowed=SDPQueueConnector.is_Standby_allowed)
    async def Standby(self):
        """Commands the default exchange group to enter the STANDBY state.
        This state has no exchanges loaded for the given group name.
        """
        await self.group_standby(None)

    @command(dtype_in=ArgType.DevString)
    @tango_command_guard(
        condition=SDPQueueConnector.is_Standby_allowed,
        command_failed_desc=group_state_message,
    )
    async def GroupStandby(self, group_name: str):
        """Commands the given exchange group to enter the STANDBY state. This
        state has no exchanges loaded for the given group name.

        Args:
            group_name (str): group name to enter STANDBY.
        """
        await self.group_standby(group_name)

    @command(fisallowed=SDPQueueConnector.is_Abort_allowed)
    async def Abort(self):
        """Commands the default exchange group to cancel unresponsive
        streams, either when starting (OPEN state) or stopping (CLOSE state)
        and transition to the OFF state.
        """
        await self.group_abort(None)

    @command(dtype_in=ArgType.DevString)
    @tango_command_guard(
        condition=SDPQueueConnector.is_Abort_allowed,
        command_failed_desc=group_state_message,
    )
    async def GroupAbort(self, group_name: str | None):
        """Commands the given exchange group to cancel unresponsive
        streams, either when starting (OPEN state) or stopping (CLOSE state).

        Args:
            group_name (str): group name to enter OFF.
        """
        await self.group_abort(group_name)

    @command(fisallowed=SDPQueueConnector.is_Reset_allowed)
    async def Reset(self):
        """Commands the default exchange group to exit the FAULT state
        and transition to the OFF state."""
        await self.group_reset(None)

    @command(dtype_in=ArgType.DevString)
    @tango_command_guard(
        condition=SDPQueueConnector.is_Reset_allowed,
        command_failed_desc=group_state_message,
    )
    async def GroupReset(self, group_name: str):
        """Commands the given exchange group to exit the FAULT state
        and transition to the OFF state.

        Args:
            group_name (str): group name to enter OFF.
        """
        await self.group_reset(group_name)

    @command(dtype_in=ArgType.DevString)
    async def Monitor(self, config_path_prefix: str):
        """Commands the device to start or restart monitoring
        the given configuration database prefix path.

        Args:
            config_path_prefix (str): The SDP configuration database path
            prefix to monitor for queue connector group configs.
        """
        self.exchanges_config_path = config_path_prefix
        await self.start_db_monitor()

    @command(dtype_out=ArgType.DevBoolean)
    def IsMonitoringDB(self) -> bool:
        """
        Returns true if database monitoring is active and not
        in the FAULT state.
        """
        return self.db_monitor_task is not None and not self.db_monitor_task.done()

    @command(
        dtype_in=ArgType.DevString,
        fisallowed=SDPQueueConnector.is_Configure_allowed,
    )
    async def Configure(self, configuration: str):
        await self.group_configure(configuration, None)

    @command(fisallowed=SDPQueueConnector.is_Start_allowed)
    async def Start(self):
        """Commands the default exchange group to start loaded exchanges
        in the OPEN state then begin streaming in the ON state once
        connections have established.
        """
        await self.group_start(None)

    @command(dtype_in=ArgType.DevString)
    @tango_command_guard(
        condition=SDPQueueConnector.is_Start_allowed,
        command_failed_desc=group_state_message,
    )
    async def GroupStart(self, group_name: str):
        """Commands the given exchange group to start loaded exchanges
        in the OPEN state then begin streaming in the ON state once
        connections have established.

        Args:
            group_name (str): name of the exchange group to start.
        """
        await self.group_start(group_name)

    @command(fisallowed=SDPQueueConnector.is_Stop_allowed)
    async def Stop(self):
        """Commands the default exchange group to stop loaded exchanges in the
        CLOSE state then transition to the OFF state once streaming has gracefully ended.
        """
        await self.group_stop(None)

    @command(dtype_in=ArgType.DevString)
    @tango_command_guard(
        condition=SDPQueueConnector.is_Stop_allowed,
        command_failed_desc=group_state_message,
    )
    async def GroupStop(self, group_name: str):
        """Commands the given exchange group to stop loaded exchanges in the CLOSE state
        then transition to the OFF state once streaming has gracefully ended.

        Args:
            group_name (str): name of the exchange group to start.
        """
        await self.group_stop(group_name)
