import asyncio
import queue
import traceback
from contextlib import AbstractContextManager
from functools import wraps
from itertools import accumulate
from typing import Any, AsyncIterator, Callable

import tango
from overrides import override

from ska_sdp_lmc_queue_connector.tango_sourcesink import TangoSubscriptionSource


class SubscribeEventContext(AbstractContextManager):
    """
    Context manager for controlling the lifetime of a tango
    event subscription.
    """

    def __init__(self, proxy, attr_name, event_type, callback):
        self._proxy = proxy
        self._attr_name = attr_name
        self.__event_type = event_type
        self.__callback = callback

    @override
    def __enter__(self):
        self.__id = self._proxy.subscribe_event(
            self._attr_name, self.__event_type, self.__callback
        )
        return self

    @override
    def __exit__(self, exc_type, exc_value, tb):
        self._proxy.unsubscribe_event(self.__id)


class SubscribeEventConditionContext(SubscribeEventContext):
    """
    Context manager for a threadsafe asyncio listener of tango events.
    """

    def __init__(self, proxy, attr_name, event_type, loop=None):
        self.__condition = asyncio.Condition()
        self.__queue = queue.Queue()
        self.__queue.put_nowait(proxy.read_attribute(attr_name).value)
        loop = asyncio.get_running_loop() if loop is None else loop

        async def notify_condition(value):
            self.__queue.put_nowait(value)
            async with self.__condition:
                self.__condition.notify_all()

        def callback_threadsafe(event):
            value = event.attr_value.value
            asyncio.run_coroutine_threadsafe(notify_condition(value), loop)

        super().__init__(proxy, attr_name, event_type, callback_threadsafe)

    async def wait_for(self, value, timeout: float | None):
        """
        Waits for the context attribute to become a specified value.
        NOTE: simultaneous calls not suported.
        """

        def predicate():
            result = False
            while not result and not self.__queue.empty():
                result = value == self.__queue.get_nowait()
            return result

        async with self.__condition:
            try:
                await asyncio.wait_for(
                    self.__condition.wait_for(predicate),
                    timeout=timeout,
                )
            except asyncio.TimeoutError:
                current_value = self._proxy.read_attribute(self._attr_name).value
                raise asyncio.TimeoutError(
                    f"timeout waiting for {current_value} to become {value}"
                )


class SubscribeEventValueContext(SubscribeEventContext):
    """
    Context manager for controlling the lifetime of a tango
    event subscription with a callback that only processes
    event values.
    """

    def __init__(self, proxy, attr_name: str, event_type, callback: Callable):
        def conditional_callback(event):
            if TangoSubscriptionSource.is_valid_event(event):
                callback(event.attr_value.value)

        super().__init__(proxy, attr_name, event_type, conditional_callback)


class SubscribeEventTensorContext(SubscribeEventContext):
    """
    Context manager for controlling the lifetime of a tango
    event subscription with a callback that only processes
    event values.
    """

    def __init__(
        self,
        proxy,
        attr_name: str,
        attr_shape_name: str,
        event_type,
        callback: Callable,
    ):
        def conditional_callback(event):
            if TangoSubscriptionSource.is_valid_event(event):
                callback(
                    event.attr_value.value.reshape(proxy.read_attribute(attr_shape_name).value)
                )

        super().__init__(proxy, attr_name, event_type, conditional_callback)


def tango_command_guard(
    condition: Callable[(...), bool], command_failed_desc: Callable[(...), str]
):
    """
    A call guard decorator for Tango Commands that need an `fisallowed` callable
    with arguments and custom API_CommandNotAllowed message.

    Args:
        condition (Callable[P, bool]): Condition predicate for whether the
            command can be invoked.
        command_failed_desc (Callable[P, str]): Custom description for the
            CommandNotAllowed exception.
    """

    def decorator(func: Callable[(...), Any]):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if not condition(*args, **kwargs):
                tango.Except.throw_exception(
                    "API_CommandNotAllowed",  # reason
                    f"Command {func.__name__}"
                    f"({','.join(repr(i) for i in args[1:])}) not allowed "
                    + command_failed_desc(*args, **kwargs),  # desc
                    traceback.format_stack(limit=1)[0],  # origin
                )
            return func(*args, **kwargs)

        return wrapper

    return decorator


def range_split(r: range, sections: int) -> list[range]:
    """
    Splits an axis range into an evenly distributes sequence of ranges.
    e.g.
    range_split(range(10, 20), 3) -> [range(10, 14), range(14, 17), range(17, 20)]
    """
    if r.start > r.stop or r.step != 1:
        raise ValueError("only forward ranges of step 1 supported")
    if sections <= 0:
        raise ValueError("number of sections must be larger than 0")
    k, rem = divmod(len(r), sections)
    sizes = [k + 1] * rem + [k] * (sections - rem)
    indexes = accumulate([r.start] + sizes[:-1])
    return list(range(index, index + size) for index, size in zip(indexes, sizes))


class AsyncQueueIterator(AsyncIterator):
    """Utility for iterating an asyncio Queue"""

    def __init__(self, source_queue: asyncio.Queue):
        self.source_queue = source_queue

    async def __anext__(self):
        return await self.source_queue.get()

    def __next__(self):
        try:
            yield self.source_queue.get_nowait()
        except asyncio.QueueEmpty:
            raise StopIteration


class QueueIterator:
    """Utility for iterating a regular Queue"""

    def __init__(self, source_queue: queue.Queue, timeout: float | None = 0.0):
        self.source_queue = source_queue
        self.timeout = timeout

    def __iter__(self):
        while True:
            try:
                yield self.source_queue.get(timeout=self.timeout)
            except queue.Empty:
                break
