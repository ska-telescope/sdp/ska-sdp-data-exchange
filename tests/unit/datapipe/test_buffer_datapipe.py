from typing import Sequence

import numpy as np
import pytest

import ska_sdp_lmc_queue_connector.astream_utils as astream
from ska_sdp_lmc_queue_connector.pipe.buffer_pipe import (
    BufferWithTimePipe,
    BufferWithTimePipeDescriptor,
)


@pytest.mark.asyncio
async def test_buffer_with_time_pipe():
    pipe = BufferWithTimePipe(BufferWithTimePipeDescriptor(timespan=0.3), np.dtype(int), [])

    await assert_aiter_numpy(
        pipe(astream.spaceout(astream.arange(12), interval=0.13)),
        values=[[0, 1, 2], [3, 4], [5, 6], [7, 8, 9], [10, 11]],
    )
    assert pipe.output_dtype == int
    assert pipe.output_shape == [-1]


@pytest.mark.asyncio
async def test_buffer_with_time_pipe_exceptions():
    pipe = BufferWithTimePipe(BufferWithTimePipeDescriptor(timespan=0.3), np.dtype(int), [])

    with pytest.raises(TypeError):
        await assert_aiter_numpy(
            pipe(astream.spaceout(astream.arange("bad"), interval=0.13)),
            values=[[0, 1, 2], [3, 4], [5, 6], [7, 8, 9], [10, 11]],
        )


async def assert_aiter_numpy(source, values: Sequence[Sequence]):
    """Assert the results of an async stream against a synchronous sequence."""
    __tracebackhide__ = True  # pylint: disable=unused-variable
    results = [item async for item in source]
    assert len(results) == len(values)
    for result, value in zip(results, values, strict=True):
        np.testing.assert_array_equal(result, value, strict=True)
