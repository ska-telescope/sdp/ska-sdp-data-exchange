import re
from typing import Any

import numpy as np
import pytest
from pydantic import BaseModel
from pydantic_core import ValidationError

from ska_sdp_lmc_queue_connector.pydantic import DType


class DTypeModel(BaseModel):
    dtype: DType | None = None


@pytest.mark.parametrize(
    "dtype",
    [
        None,
        np.dtype(np.int64),
        np.dtype(np.str_),
        np.dtype(np.bytes_),
        np.dtype(np.object_),
        np.dtype(np.datetime64),
        np.dtype("datetime64[ns]"),
        np.dtype("datetime64[ms]"),
        np.dtype([]),
        np.dtype({}),
        np.dtype([("", "<i8")]),
        np.dtype([("a", "<i8")]),
    ],
)
def test_dtypes(dtype: Any):
    kwargs = {}
    if dtype is not None:
        kwargs["dtype"] = dtype

    model_expected = DTypeModel(**kwargs)
    ser = model_expected.model_dump_json()
    model_actual = DTypeModel.model_validate_json(ser)
    assert model_actual.dtype == dtype


@pytest.mark.parametrize(
    "dtypelike, dtype",
    [
        (None, None),
        (str, np.dtype(np.str_)),
        (np.str_, np.dtype(np.str_)),
        (bytes, np.dtype(np.bytes_)),
        (np.bytes_, np.dtype(np.bytes_)),
        (object, np.dtype(np.object_)),
        (np.object_, np.dtype(np.object_)),
        (dict, np.dtype(np.object_)),
        (list, np.dtype(np.object_)),
        (np.int32, np.dtype(np.int32)),
        (int, np.dtype(np.int64)),
        ("i8", np.dtype(np.int64)),
        ("int64", np.dtype(np.int64)),
        (np.float32, np.dtype(np.float32)),
        (float, np.dtype(np.float64)),
        ("f8", np.dtype(np.float64)),
        ("float64", np.dtype(np.float64)),
        (np.datetime64, np.dtype(np.datetime64)),
        ("datetime64", np.dtype("datetime64")),
        ("datetime64[ns]", np.dtype("datetime64[ns]")),
        ("datetime64[ms]", np.dtype("datetime64[ms]")),
        (np.void, np.dtype(np.void)),
        ([], np.dtype([])),
        ([("a", np.int64)], np.dtype([("a", np.int64)])),
    ],
)
def test_extended_dtypes(dtypelike: Any, dtype: np.dtype):
    kwargs = {}
    if dtypelike is not None:
        kwargs["dtype"] = dtypelike

    model_expected = DTypeModel(**kwargs)
    ser = model_expected.model_dump_json()
    model_actual = DTypeModel.model_validate_json(ser)
    assert model_expected.dtype == dtype
    assert isinstance(model_expected.dtype, np.dtype) or model_expected.dtype is None
    assert model_actual.dtype == dtype
    assert isinstance(model_actual.dtype, np.dtype) or model_actual.dtype is None


@pytest.mark.parametrize(
    "dtype, expected",
    [
        ('"str"', np.dtype(np.str_)),
        ('"str_"', np.dtype(np.str_)),
        ('"bytes"', np.dtype(np.bytes_)),
        ('"bytes_"', np.dtype(np.bytes_)),
        ('"object"', np.dtype(np.object_)),
        ('"object_"', np.dtype(np.object_)),
        ('"int32"', np.dtype(np.int32)),
        ('"int"', np.dtype(np.int64)),
        ('"i8"', np.dtype(np.int64)),
        ('"float32"', np.dtype(np.float32)),
        ('"float"', np.dtype(np.float64)),
        ('"f8"', np.dtype(np.float64)),
        ('"datetime64"', np.dtype("datetime64")),
        ('"datetime64[ns]"', np.dtype("datetime64[ns]")),
        ('"datetime64[ms]"', np.dtype("datetime64[ms]")),
        ('"void"', np.dtype(np.void)),
        (
            '[["a", "int"], ["b", "float"]]',
            np.dtype([("a", np.int64), ("b", np.float64)]),
        ),
    ],
)
def test_json_dtype(dtype: Any, expected: np.dtype):
    model_actual = DTypeModel.model_validate_json(f'{{ "dtype": {dtype} }}')
    assert model_actual.dtype == expected


@pytest.mark.parametrize(
    "dtype",
    [
        "",
        "[]",
        "{}",
        "invalid",
        "dict",
        "list",
        "i16",
        "i32",
        "i64",
    ],
)
def test_invalid_python_dtypes(dtype):
    with pytest.raises(ValidationError, match=f"data type '{re.escape(dtype)}' not understood"):
        _ = DTypeModel(dtype=dtype)


@pytest.mark.parametrize(
    "dtype",
    [
        "",
        "[]",
        "{}",
        "invalid",
        "dict",
        "list",
        "i16",
        "i32",
        "i64",
    ],
)
def test_invalid_json_dtypes(dtype: str):
    with pytest.raises(ValidationError, match=f"data type '{re.escape(dtype)}' not understood"):
        _ = DTypeModel.model_validate_json(f'{{"dtype": "{dtype}"}}')
