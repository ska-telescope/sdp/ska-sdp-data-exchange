import asyncio
import contextlib

import pytest
from ska_sdp_config import Config
from tango import DevState, EventType

from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    InMemorySinkDescriptor,
    InMemorySourceDescriptor,
    QueueConnectorDescriptor,
)
from ska_sdp_lmc_queue_connector.in_memory_sourcesink import InMemorySink
from tests.tango_helper import SubscribeEventConditionContext

PB_ID = "pb-0-00000000-pipeline1"
CONFIG_PATH = "/component/lmc-queueconnector-01/owner"
CONFIG_PATH1 = f"{CONFIG_PATH}/{PB_ID}"
EMPTY_DESCRIPTOR_JSON = "{}"


def _create_inmemory_descriptor(data: list[int]):
    return QueueConnectorDescriptor(
        exchanges={
            PB_ID: [
                ExchangeDescriptor(
                    dtype="int16",
                    source=InMemorySourceDescriptor(data=data, delay=0.01),
                    sink=InMemorySinkDescriptor(key="1"),
                )
            ],
        }
    )


EXAMPLE_DESCRIPTOR_JSON = _create_inmemory_descriptor([1, 2, 3, 4, 5]).model_dump_json_nodb()


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "config,error",
    [
        (EMPTY_DESCRIPTOR_JSON, None),
        (
            QueueConnectorDescriptor(exchanges=None).model_dump_json_nodb(),
            None,
        ),
        ('{"exchanges": []}', None),
        ('{"exchanges": null}', None),
        # BUG: Tango nodb test context currently cannot handle quoted strings
        # properly.
        # See https://gitlab.com/tango-controls/cppTango/-/merge_requests/1152
        ('{"exchanges":[]}', RuntimeError),
        ('{"exchanges":null}', RuntimeError),
    ],
)
async def test_configuration_empty(config, error: type, create_manual_qc):
    """
    Tests the empty configuration which should initialize
    the device into the STANDBY state.
    """
    if error is None:
        context = contextlib.nullcontext
    else:
        context = pytest.raises

    with context(error):
        with create_manual_qc({"exchanges_json": config, "experimental_flow": False}) as proxy:
            assert proxy.State() == DevState.STANDBY


@pytest.fixture(name="sdp_config")
def sdp_config_fixture():
    config = Config(backend="etcd3")
    config.backend.delete(CONFIG_PATH, recursive=True, must_exist=False)
    yield config
    config.backend.delete(CONFIG_PATH, recursive=True, must_exist=False)


@pytest.mark.asyncio
@pytest.mark.parametrize("method", ["property", "command"])
async def test_configuration_simple(sdp_config, method, create_manual_qc):
    sdp_config.backend.create(CONFIG_PATH1, EXAMPLE_DESCRIPTOR_JSON)
    properties = {"experimental_flow": False}
    if method == "property":
        properties["exchanges_json"] = EXAMPLE_DESCRIPTOR_JSON

    with create_manual_qc(properties) as proxy:
        if method == "command":
            proxy.Configure(EXAMPLE_DESCRIPTOR_JSON)

        assert proxy.IsMonitoringDB() is False

        proxy.GroupStart(PB_ID)
        await asyncio.sleep(0.5)
        proxy.GroupStop(PB_ID)

        __assert_sink_example_data()


@pytest.mark.asyncio
@pytest.mark.parametrize("method", ["property", "command"])
async def test_configuration_by_db_simple(sdp_config, method, create_manual_qc):
    sdp_config.backend.create(CONFIG_PATH1, EXAMPLE_DESCRIPTOR_JSON)
    properties = {"experimental_flow": False}
    if method == "property":
        properties["exchanges_config_path"] = CONFIG_PATH

    with create_manual_qc(properties) as proxy:
        if method == "command":
            proxy.Monitor(CONFIG_PATH)

        assert proxy.IsMonitoringDB() is True
        await asyncio.sleep(0.5)
        proxy.GroupStop(PB_ID)

        __assert_sink_example_data()


@pytest.mark.asyncio
@pytest.mark.parametrize("preconfig", [True, False])
async def test_configuration_by_db_repeat(sdp_config: Config, preconfig: bool, create_manual_qc):
    # config path method should handle case where config was set
    # before device is created.
    if preconfig:
        sdp_config.backend.create(CONFIG_PATH1, EXAMPLE_DESCRIPTOR_JSON)

    with (
        create_manual_qc(
            {"exchanges_config_path": CONFIG_PATH, "experimental_flow": False}
        ) as proxy,
        SubscribeEventConditionContext(proxy, "State", EventType.CHANGE_EVENT) as device_state,
    ):
        assert proxy.IsMonitoringDB() is True

        # change to standby
        if preconfig:
            await device_state.wait_for(DevState.ON, 10.0)
            await asyncio.sleep(0.2)
            sdp_config.backend.update(CONFIG_PATH1, EMPTY_DESCRIPTOR_JSON)
            await device_state.wait_for(DevState.STANDBY, 2.0)
            __assert_sink_example_data()
        else:
            sdp_config.backend.create(CONFIG_PATH1, EMPTY_DESCRIPTOR_JSON)
            await device_state.wait_for(DevState.STANDBY, 2.0)

        # start and stop n times
        for _ in range(2):
            sdp_config.backend.update(CONFIG_PATH1, EXAMPLE_DESCRIPTOR_JSON)
            await device_state.wait_for(DevState.ON, 2.0)
            await asyncio.sleep(0.2)
            sdp_config.backend.update(CONFIG_PATH1, EMPTY_DESCRIPTOR_JSON)
            await device_state.wait_for(DevState.STANDBY, 2.0)
            __assert_sink_example_data()


def __assert_sink_example_data():
    q = InMemorySink.get_queue("1")
    for expected in range(1, 6):
        assert expected == q.get_nowait()
