import json
import os

import pytest
from tango import EventType

from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    KafkaConsumerSourceDescriptor,
    KafkaProducerSinkDescriptor,
    QueueConnectorDescriptor,
    TangoLocalAttributeSinkDescriptor,
    TangoSubscriptionSourceDescriptor,
)

KAFKA_HOST = "localhost:9092"
TEST_TOPIC = "test-events"


@pytest.mark.parametrize(
    "raw_json,expected_descriptor",
    [
        ("{}", QueueConnectorDescriptor(exchanges=None)),
        ('{"exchanges": null}', QueueConnectorDescriptor(exchanges=None)),
        ('{"exchanges": []}', QueueConnectorDescriptor(exchanges=[])),
    ],
)
def test_empty_deserialize(raw_json, expected_descriptor):
    output_descriptor = QueueConnectorDescriptor.model_validate_json(raw_json)
    assert output_descriptor == expected_descriptor


def test_serialize_deserialize():
    input_descriptor = QueueConnectorDescriptor(
        exchanges=[
            ExchangeDescriptor(
                source=KafkaConsumerSourceDescriptor(servers=KAFKA_HOST, topic=TEST_TOPIC),
                sink=TangoLocalAttributeSinkDescriptor(
                    attribute_name="calibration", default_value=""
                ),
            ),
            ExchangeDescriptor(
                source=TangoSubscriptionSourceDescriptor(
                    device_name="subarray/1",
                    attribute_name="pointings",
                    etype=EventType.CHANGE_EVENT,
                ),
                sink=KafkaProducerSinkDescriptor(servers=KAFKA_HOST, topic=TEST_TOPIC),
            ),
        ]
    )

    exchanges_str = input_descriptor.model_dump_json()
    assert isinstance(exchanges_str, str)

    output_descriptor = QueueConnectorDescriptor.model_validate_json(exchanges_str)
    assert input_descriptor == output_descriptor


def test_schema():
    schema = QueueConnectorDescriptor.model_json_schema()

    SCHEMA_FILE = "schemas/queue_connector_descriptor.json"
    with open(SCHEMA_FILE, encoding="utf-8") as f:
        doc_schema = json.loads(f.read())

    try:
        assert doc_schema == schema
    except AssertionError:
        # If git diff.tool is set use interactive tool to update schema
        check_tool_command = (
            "bash -c command -v git && bash -c '[[ ! -z $(git config --get diff.tool) ]]'"
        )
        if os.system(check_tool_command) == 0:
            import tempfile

            with tempfile.NamedTemporaryFile("w", encoding="utf-8", suffix=".json") as f:
                f.write(json.dumps(schema, indent=2))
                os.system(f"git difftool --no-prompt --no-index {SCHEMA_FILE} {f.name}")
        raise
