import asyncio

import aiokafka
import numpy as np
import pytest
from tango import DevFailed

from ska_sdp_lmc_queue_connector import ExchangeDescriptor, QueueConnectorDescriptor
from ska_sdp_lmc_queue_connector.in_memory_sourcesink import InMemorySourceDescriptor
from ska_sdp_lmc_queue_connector.kafka_sourcesink import KafkaProducerSinkDescriptor

KAFKA_HOST = "localhost:9092"
TEST_TOPIC = "test-topic"
FORMAT = "python"


def __create_dummy_descriptor(value: int):
    return ExchangeDescriptor(
        dtype=np.dtype(int),
        shape=[],
        source=InMemorySourceDescriptor(data=[value]),
        sink=KafkaProducerSinkDescriptor(
            servers=KAFKA_HOST,
            topic=TEST_TOPIC,
            format=FORMAT,
        ),
    )


@pytest.fixture
def device_properties():
    return {
        "exchanges_json": QueueConnectorDescriptor(
            exchanges={
                "1": [__create_dummy_descriptor(1)],
                "2": [
                    __create_dummy_descriptor(2),
                    __create_dummy_descriptor(3),
                ],
            }
        ).model_dump_json_nodb(),
    }


@pytest.mark.asyncio
async def test_multiple_groups(manual_qc):
    async with (aiokafka.AIOKafkaConsumer(TEST_TOPIC, bootstrap_servers=KAFKA_HOST)) as consumer:
        manual_qc.GroupStart("1")
        manual_qc.GroupStart("2")

        messages = []

        async def collect_messages():
            async for message in consumer:
                messages.append(message.value)

        with pytest.raises(asyncio.TimeoutError):
            await asyncio.wait_for(collect_messages(), 3)

        manual_qc.GroupStop("1")
        manual_qc.GroupStop("2")

    assert set(messages) == {b"1", b"2", b"3"}


@pytest.mark.asyncio
async def test_debug_group_exceptions(manual_qc):
    async with (aiokafka.AIOKafkaConsumer(TEST_TOPIC, bootstrap_servers=KAFKA_HOST)) as consumer:
        with pytest.raises(
            DevFailed,
            match="Command GroupStart\\('a'\\) not allowed when group a has not been registered",
        ):
            manual_qc.GroupStart("a")

        manual_qc.GroupStart("1")
        with pytest.raises(
            DevFailed,
            match="Command GroupAbort\\('1'\\) not allowed when group 1 is in the ON state",
        ):
            manual_qc.GroupAbort("1")
        with pytest.raises(
            DevFailed,
            match="Command GroupReset\\('1'\\) not allowed when group 1 is in the ON state",
        ):
            manual_qc.GroupReset("1")
        with pytest.raises(
            DevFailed,
            match="Command GroupStart\\('1'\\) not allowed when group 1 is in the ON state",
        ):
            manual_qc.GroupStart("1")
        with pytest.raises(
            DevFailed,
            match="Command GroupStandby\\('1'\\) not allowed when group 1 is in the ON state",
        ):
            manual_qc.GroupStandby("1")

        manual_qc.GroupStart("2")

        messages = []

        async def collect_messages():
            async for message in consumer:
                messages.append(message.value)

        with pytest.raises(asyncio.TimeoutError):
            await asyncio.wait_for(collect_messages(), 3)

        manual_qc.GroupStop("1")
        manual_qc.GroupStop("2")

    assert set(messages) == {b"1", b"2", b"3"}
