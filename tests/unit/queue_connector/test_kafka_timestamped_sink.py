# flake8: noqa

import asyncio

import numpy as np
import pytest
from aiokafka import AIOKafkaConsumer, ConsumerRecord

from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    InMemorySourceDescriptor,
    QueueConnectorDescriptor,
)
from ska_sdp_lmc_queue_connector.kafka_sourcesink import (
    KafkaProducerSink,
    KafkaProducerSinkDescriptor,
)
from ska_sdp_lmc_queue_connector.sourcesink import DataType

KAFKA_HOST = "localhost:9092"
TEST_TOPIC = "kafka-data-test-events"


@pytest.fixture
def device_properties(
    dtype: type,
    encoding: str,
    shape: list,
    data: DataType,
    slices: tuple,
    key: str,
    reducer: str,
):
    return {
        "exchanges_json": QueueConnectorDescriptor(
            exchanges=[
                ExchangeDescriptor(
                    dtype=dtype,
                    shape=shape,
                    source=InMemorySourceDescriptor(data=data, delay=0.02),
                    sink=KafkaProducerSinkDescriptor(
                        servers=KAFKA_HOST,
                        topic=TEST_TOPIC,
                        format=encoding,
                        timestamp_options=KafkaProducerSinkDescriptor.TimestampOptions(
                            slices=slices, key=key, reducer=reducer
                        ),
                    ),
                ),
            ]
        ).model_dump_json_nodb(),
    }


POINTING_DTYPE = [
    ("antenna_id", "int32"),
    ("ts", "datetime64[ns]"),
    ("az", "float64"),
    ("el", "float64"),
]

# fmt: off
@pytest.mark.asyncio
@pytest.mark.parametrize("dtype, encoding, shape, data, slices, key, reducer", [
    # types
    (dict,            'utf-8', [],    [{'a': 7777}],                     tuple(), 'a',  None),
    (int,             'utf-8', [],    [7777],                            tuple(), None, None),
    (dict,            'json',  [],    [{'a': 7777}],                     tuple(), 'a',  None),
    (int,             'json',  [],    [7777],                            tuple(), None, None),
    (int,             'json',  [2],   [[300,7777]],                      (1,),    None, None),
    (int,             'npy',   [2],   [[300,7777]],                      (1,),    None, None),
    (np.uint64,       'npy',   [2],   [[300,7777]],                      (1,),    None, None),
    (np.int64,        'npy',   [2,2], [[[1,7777],[2,3]]],                (0,1),   None, None),
    (POINTING_DTYPE,  'npy',   [1],   [[0, 7777000000, 0., 0.]],         (0,),    'ts', None),
    # reduce
    (int,             'npy',   [2,2],    [[[300,6666],[0,8888]]],        (slice(None),1), None, "mean"),
    (int,             'npy',   [3,2],    [[[1,8888],[2,7777],[3,9999]]], (slice(None),1), None, "min"),
    (int,             'npy',   [3,2],    [[[2,6666],[3,0],[0,7777]]],    (slice(None),1), None, "max"),
])
# fmt: on
async def test_kafka_timestamp(manual_qc):
    async with AIOKafkaConsumer(TEST_TOPIC, bootstrap_servers=KAFKA_HOST) as consumer:
        manual_qc.Start()
        try:
            v: ConsumerRecord = await asyncio.wait_for(anext(consumer), 1.0)
            assert v.timestamp == 7777
        finally:
            manual_qc.Stop()


# fmt: off
@pytest.mark.asyncio
@pytest.mark.parametrize("dtype, encoding, shape, slices, key, error, message", [
    (dict,            'utf-8',  [],  tuple(), None, RuntimeError, "timestamp key is required"),
    (POINTING_DTYPE,  'utf-8',  [],  tuple(), None, RuntimeError, "timestamp key is required"),
    (int,             'utf-8',  [],  (1,),    None, RuntimeError, "slices dims mismatch"),
    (int,             'utf-8',  [1], tuple(), None, RuntimeError, "slices dims mismatch"),
    (int,             'utf-8',  [],  tuple(), 'a',  RuntimeWarning, "timestamp key is not used"),
])
# fmt: on
async def test_kafka_timestamp_configure_fault(
    dtype: type,
    encoding: str,
    shape: list,
    slices: tuple,
    key: str,
    error: type,
    message: str,
):
    if issubclass(error, Warning):
        context = pytest.warns
    else:
        context = pytest.raises

    with context(error, match=message):
        sink = KafkaProducerSink(
            KafkaProducerSinkDescriptor(
                servers=KAFKA_HOST,
                topic=TEST_TOPIC,
                format=encoding,
                timestamp_options=KafkaProducerSinkDescriptor.TimestampOptions(
                    slices=slices,
                    key=key,
                ),
            ),
            np.dtype(dtype),
            shape,
        )
        await sink.start()
        await sink.stop()
