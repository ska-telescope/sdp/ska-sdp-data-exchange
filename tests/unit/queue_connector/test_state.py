import asyncio

import pytest
from tango import DevFailed, DevState, EventType, GreenMode

from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    InMemorySinkDescriptor,
    InMemorySourceDescriptor,
    QueueConnectorDescriptor,
    TangoLocalAttributeSinkDescriptor,
)
from ska_sdp_lmc_queue_connector.in_memory_sourcesink import InMemorySink
from tests.tango_helper import SubscribeEventConditionContext

CONFIG_PATH_PREFIX = "/component/lmc-queueconnector-01/owner"

exchange_json = QueueConnectorDescriptor(
    exchanges=[
        ExchangeDescriptor(
            dtype=str,
            source=InMemorySourceDescriptor(data=["1", "2", "3"], delay=0.01),
            sink=TangoLocalAttributeSinkDescriptor(
                attribute_name="test_attribute0",
                default_value="",
            ),
        ),
        ExchangeDescriptor(
            source=InMemorySourceDescriptor(data=["4", "5", "6"], delay=0.01),
            sink=InMemorySinkDescriptor(key="1"),
        ),
    ]
).model_dump_json_nodb()


@pytest.fixture(name="default_proxy")
def default_proxy_fixture(create_manual_qc):
    with create_manual_qc({}) as proxy:
        yield proxy


@pytest.fixture(name="monitoring_proxy")
def monitoring_proxy_fixture(create_manual_qc):
    with create_manual_qc({"exchanges_config_path": CONFIG_PATH_PREFIX}) as proxy:
        yield proxy


@pytest.fixture(name="proxy")
def proxy_fixture(create_manual_qc):
    with create_manual_qc({"exchanges_json": exchange_json}) as proxy:
        yield proxy


def test_default_state_monitoring_off(default_proxy):
    assert default_proxy.State() == DevState.STANDBY


def test_default_state_monitoring_on(monitoring_proxy):
    assert monitoring_proxy.State() == DevState.STANDBY


@pytest.mark.asyncio
async def test_common_states(proxy):
    assert proxy.State() == DevState.OFF
    assert proxy.isMonitoringDB() is False

    proxy.Standby()
    assert proxy.State() == DevState.STANDBY

    proxy.Configure(exchange_json)
    assert proxy.State() == DevState.OFF

    # Sinks should be in default state until started
    assert proxy.State() == DevState.OFF
    assert proxy.test_attribute0 == ""
    with pytest.raises(asyncio.QueueEmpty):
        InMemorySink.get_queue("1").get_nowait()

    # Immediately stopping after start won't allow
    # enough time for streaming any data
    proxy.Start()
    assert proxy.State() == DevState.ON
    proxy.Stop()
    assert proxy.State() == DevState.OFF
    assert proxy.test_attribute0 == ""
    with pytest.raises(asyncio.QueueEmpty):
        InMemorySink.get_queue("1").get_nowait()

    # Stream remaining data
    proxy.Start()
    await asyncio.sleep(0.1)
    assert proxy.State() == DevState.ON
    proxy.Stop()
    assert proxy.State() == DevState.OFF
    assert proxy.test_attribute0 == "3"
    assert InMemorySink.get_queue("1").get_nowait() == "4"
    assert InMemorySink.get_queue("1").get_nowait() == "5"
    assert InMemorySink.get_queue("1").get_nowait() == "6"


@pytest.mark.asyncio
async def test_state_exceptions(proxy):
    assert proxy.isMonitoringDB() is False

    assert proxy.State() == DevState.OFF
    with pytest.raises(DevFailed):
        proxy.Configure(exchange_json)
    with pytest.raises(DevFailed):
        proxy.Stop()

    proxy.Start()
    assert proxy.State() == DevState.ON
    with pytest.raises(DevFailed):
        proxy.Start()
    with pytest.raises(DevFailed):
        proxy.Configure(exchange_json)
    with pytest.raises(DevFailed):
        proxy.Standby()

    proxy.Stop()
    assert proxy.State() == DevState.OFF

    proxy.Standby()
    assert proxy.State() == DevState.STANDBY
    with pytest.raises(DevFailed):
        proxy.Standby()
    with pytest.raises(DevFailed):
        proxy.Start()
    with pytest.raises(DevFailed):
        proxy.Stop()


@pytest.mark.asyncio
async def test_common_state_events(proxy):
    with SubscribeEventConditionContext(proxy, "State", EventType.CHANGE_EVENT) as device_state:
        await device_state.wait_for(DevState.OFF, timeout=1)
        proxy.command_inout_asynch("Start")
        await device_state.wait_for(DevState.OPEN, timeout=1)
        await device_state.wait_for(DevState.ON, timeout=1)

        proxy.command_inout_asynch("Stop")
        await device_state.wait_for(DevState.CLOSE, timeout=1)
        await device_state.wait_for(DevState.OFF, timeout=1)


def test_configure_fault(default_proxy):
    assert default_proxy.State() == DevState.STANDBY

    with pytest.raises(DevFailed):
        default_proxy.Configure('{"exchanges": [1,2,3]}')
    assert default_proxy.State() == DevState.STANDBY

    with pytest.raises(DevFailed):
        default_proxy.Configure("")
    assert default_proxy.State() == DevState.STANDBY

    default_proxy.Configure("{}")
    assert default_proxy.State() == DevState.STANDBY


@pytest.mark.asyncio
async def test_start_fault(proxy):

    # Cause a sink exception
    InMemorySink.set_queue("1", None)  # type: ignore

    assert proxy.State() == DevState.OFF
    proxy.Start()
    await asyncio.sleep(0.03)
    assert proxy.State() == DevState.FAULT

    InMemorySink.set_queue("1", asyncio.Queue())
    proxy.Reset()
    assert proxy.State() == DevState.OFF
    proxy.Start()
    await asyncio.sleep(0.05)
    assert proxy.State() == DevState.ON
    proxy.Stop()
    assert proxy.State() == DevState.OFF
    assert proxy.test_attribute0 == "3"

    # NOTE: source data gets fully buffered and lost on sink exception
    with pytest.raises(asyncio.QueueEmpty):
        InMemorySink.get_queue("1").get_nowait()
