import numpy as np
import pytest

from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    InMemorySourceDescriptor,
    QueueConnectorDescriptor,
    TangoLocalAttributeSinkDescriptor,
)
from ska_sdp_lmc_queue_connector.sourcesink import DataType
from ska_sdp_lmc_queue_connector.tango_dtype_helper import tango_format_to_shape, tango_to_dtypes


@pytest.fixture
def device_properties(dtype: type, shape: list, default_value: DataType):
    return {
        "exchanges_json": QueueConnectorDescriptor(
            exchanges=[
                ExchangeDescriptor(
                    dtype=dtype,
                    shape=shape,
                    source=InMemorySourceDescriptor(data=[]),
                    sink=TangoLocalAttributeSinkDescriptor(
                        attribute_name="test_attr",
                        default_value=default_value,
                    ),
                ),
            ]
        ).model_dump_json_nodb(),
    }


# fmt: off
# flake8: noqa
@pytest.mark.parametrize("dtype, shape, default_value", [
    (object,        [],     {}),
    (dict,          [],     {}),
    (bytes,         [],     ('utf-8', b'testvalue')),
    (str,           [],     'testvalue'),
    (str,           [4],    'testvalue'),
    (str,           [3,2],  'testvalue'),
    (str,           [3,2],  [['a', 'variable'], ['length', 'str'], ['test', '']]),
    (bool,          [],     True),
    (bool,          [4],    True),
    (bool,          [3,2],  True),
    (np.uint8,      [],     3),
    (np.uint8,      [4],    3),
    (np.uint8,      [3,2],  3),
    (np.uint16,     [],     3),
    (np.uint16,     [4],    3),
    (np.uint16,     [3,2],  3),
    (np.uint32,     [],     3),
    (np.uint32,     [4],    3),
    (np.uint32,     [3,2],  3),
    (np.uint64,     [],     3),
    (np.uint64,     [4],    3),
    (np.uint64,     [3,2],  3),
    (np.int16,      [],     3),
    (np.int16,      [4],    3),
    (np.int16,      [3,2],  3),
    (np.int32,      [],     3),
    (np.int32,      [4],    3),
    (np.int32,      [3,2],  3),
    (np.int64,      [],     3),
    (np.int64,      [4],    3),
    (np.int64,      [2,2],  3),
    (int,           [],     3),
    (int,           [4],    3),
    (int,           [3,2],  3),
    (np.float32,    [],     3),
    (np.float32,    [4],    3),
    (np.float32,    [3,2],  [[0,1],[2,3],[4,5]]),
    (np.float32,    [],     float("NaN")),
    (np.float32,    [2],    np.array([float("NaN"), float("NaN")]).tolist()),
    (np.float32,    [3,2],  float("NaN")),
    (np.float32,    [],     float("Inf")),
    (np.float32,    [2],    float("Inf")),
    (np.float32,    [3,2],  float("Inf")),
    (np.float32,    [],     float("-Inf")),
    (np.float32,    [2],    float("-Inf")),
    (np.float32,    [3,2],  float("-Inf")),
    (np.float64,    [],     3),
    (np.float64,    [4],    [1,2,3,4]),
    (np.float64,    [3,2],  3),
    (np.float64,    [3,2],  [[0,1],[2,3],[4,5]]),
    (float,         [],     3),
    (float,         [4],    3),
    (float,         [3,2],  3),
])
# fmt: on
def test_read_attribute_type(manual_qc, dtype: type, shape: list, default_value: DataType):
    attr = manual_qc.get_attribute_config_ex("test_attr")[0]
    assert np.dtype(dtype) in tango_to_dtypes(attr.data_type)
    assert tango_format_to_shape(attr.data_format, attr.max_dim_x, attr.max_dim_y) == shape

    if dtype in (dict, object):
        # for types unsupported by tango
        assert type(manual_qc.test_attr) == str
        assert manual_qc.test_attr == str(default_value)
    else:
        # ensure type supported by tango and numpy
        np.testing.assert_array_equal(manual_qc.test_attr, default_value)

    # To readers from the future:
    #
    # From 6bc7f0a The queue connector stopped supporting READWRITE attributes created by sinks.
    # This file contained a section below where which ensured the dtypes and shapes above work in both
    # reading and writing directions. If writeable attributes are needed at some point (which would be
    # created by Sources instead of Sinks), the removed code would be useful again. Go and grab it!
