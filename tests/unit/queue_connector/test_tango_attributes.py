import asyncio
import logging
import math
import threading

import numpy as np
import pytest
from tango import ArgType, AttrDataFormat, DevFailed, EventType

from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    InMemorySourceDescriptor,
    QueueConnectorDescriptor,
    TangoLocalAttributeSinkDescriptor,
)
from tests.tango_helper import SubscribeEventContext

SPECTRUM_ATTR_SHAPE = [2]
IMAGE_ATTR_SHAPE = [3, 5]


@pytest.fixture
def device_properties():
    return {
        "exchanges_json": QueueConnectorDescriptor(
            exchanges=[
                ExchangeDescriptor(
                    dtype=str,
                    source=InMemorySourceDescriptor(data=["1", "2", "3"], delay=0.02),
                    sink=TangoLocalAttributeSinkDescriptor(
                        attribute_name="test_attribute0",
                        default_value="0",
                    ),
                ),
                ExchangeDescriptor(
                    dtype=np.float64,
                    source=InMemorySourceDescriptor(
                        data=np.arange(0, 10, 0.5).tolist(), delay=0.5
                    ),
                    sink=TangoLocalAttributeSinkDescriptor(
                        attribute_name="test_attribute1",
                        default_value=0.0,
                    ),
                ),
                ExchangeDescriptor(
                    dtype=np.int64,
                    shape=SPECTRUM_ATTR_SHAPE,
                    source=InMemorySourceDescriptor(
                        data=np.arange(0, math.prod(SPECTRUM_ATTR_SHAPE) * 10, 0.5).tolist(),
                        delay=0.5,
                    ),
                    sink=TangoLocalAttributeSinkDescriptor(
                        attribute_name="test_attribute2",
                        default_value=-3,
                    ),
                ),
                ExchangeDescriptor(
                    dtype=np.int32,
                    shape=IMAGE_ATTR_SHAPE,
                    source=InMemorySourceDescriptor(
                        data=np.arange(0, math.prod(IMAGE_ATTR_SHAPE) * 10, 0.5).tolist(),
                        delay=0.5,
                    ),
                    sink=TangoLocalAttributeSinkDescriptor(
                        attribute_name="test_attribute3",
                        default_value=-11,
                    ),
                ),
            ]
        ).model_dump_json_nodb(),
    }


@pytest.mark.parametrize(
    "attr_name, data_type, data_format, max_dim_x, max_dim_y",
    (
        ("test_attribute0", ArgType.DevString, AttrDataFormat.SCALAR, 1, 0),
        ("test_attribute1", ArgType.DevDouble, AttrDataFormat.SCALAR, 1, 0),
        ("test_attribute2", ArgType.DevLong64, AttrDataFormat.SPECTRUM, SPECTRUM_ATTR_SHAPE[0], 0),
        (
            "test_attribute3",
            ArgType.DevLong,
            AttrDataFormat.IMAGE,
            IMAGE_ATTR_SHAPE[1],
            IMAGE_ATTR_SHAPE[0],
        ),
    ),
)
@pytest.mark.asyncio
async def test_attribute_configs(
    manual_qc,
    attr_name: str,
    data_type: ArgType,
    data_format: AttrDataFormat,
    max_dim_x: int,
    max_dim_y: int,
):
    attr = manual_qc.get_attribute_config_ex(attr_name)
    assert manual_qc.get_attribute_poll_period(attr_name) == 0
    assert len(attr) == 1
    assert attr[0].data_type == data_type
    assert attr[0].data_format == data_format
    assert attr[0].max_dim_x == max_dim_x
    assert attr[0].max_dim_y == max_dim_y
    # NOTE: periodic event period not configurable
    assert attr[0].events.per_event.period == "1000"
    assert attr[0].events.ch_event.abs_change == "Not specified"
    assert attr[0].events.ch_event.rel_change == "Not specified"
    assert attr[0].events.arch_event.archive_abs_change == "Not specified"
    assert attr[0].events.arch_event.archive_rel_change == "Not specified"
    assert attr[0].events.arch_event.archive_period == "Not specified"


@pytest.mark.asyncio
async def test_attribute_read(manual_qc):
    assert manual_qc.test_attribute0 == "0"
    assert manual_qc.test_attribute1 == 0.0
    assert np.all(manual_qc.test_attribute2 == np.full(SPECTRUM_ATTR_SHAPE, -3, dtype=np.int64))
    assert np.all(manual_qc.test_attribute3 == np.full(IMAGE_ATTR_SHAPE, -11, dtype=np.int32))
    manual_qc.Start()
    await asyncio.sleep(0.1)
    manual_qc.Stop()
    assert manual_qc.test_attribute0 == "3"
    assert manual_qc.test_attribute1 == 0.0


@pytest.mark.asyncio
async def test_attribute_cant_write(manual_qc):
    assert manual_qc.test_attribute0 == "0"
    assert manual_qc.test_attribute1 == 0.0
    manual_qc.Start()
    await asyncio.sleep(0.1)
    with pytest.raises(DevFailed):
        manual_qc.test_attribute0 = "4"
    with pytest.raises(DevFailed):
        manual_qc.test_attribute1 = 10.0
    assert manual_qc.test_attribute0 == "3"
    assert manual_qc.test_attribute1 == 0.0


@pytest.mark.asyncio
async def test_attribute_removal(manual_qc):
    attributes = {"test_attribute0", "test_attribute1"}
    assert attributes.intersection(manual_qc.get_attribute_list()) == attributes
    manual_qc.Standby()
    assert attributes.intersection(manual_qc.get_attribute_list()) == set()


@pytest.mark.asyncio
async def test_attribute_changed_events(manual_qc):
    events = []

    def append_event_value(event):
        # On the client side, the first callback is run on the current
        # thread which in several cases is separate to the tango thread.
        # This is one way to filter out the default value event.
        if threading.current_thread() is not threading.main_thread():
            events.append(event.attr_value.value)

    with SubscribeEventContext(
        manual_qc, "test_attribute0", EventType.CHANGE_EVENT, append_event_value
    ):
        assert manual_qc.test_attribute0 == "0"
        await asyncio.sleep(0.2)
        manual_qc.Start()
        await asyncio.sleep(0.5)
        manual_qc.Stop()

    assert manual_qc.test_attribute0 == "3"
    assert events == ["1", "2", "3"]


@pytest.mark.asyncio
async def test_attribute_changed_events_with_duplicate_first_event(manual_qc):
    events = []

    def append_event_value(event):
        # Two default events will appear here:
        # One from the main thread
        # One from the attribute polling thread
        events.append(event.attr_value.value)

    with SubscribeEventContext(
        manual_qc,
        "test_attribute1",
        EventType.CHANGE_EVENT,
        append_event_value,
    ):
        assert manual_qc.test_attribute1 == 0.0
        await asyncio.sleep(0.6)
        assert events == [0.0]

        manual_qc.Start()
        await asyncio.sleep(4.2)
        manual_qc.Stop()

    assert manual_qc.test_attribute1 == 3.5
    assert _prepend_missing_event(events) == [0.0, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5]


def _prepend_missing_event(events: list) -> list:
    """
    Prepends a duplicate event at the start of an event list. Tango
    subscriptions on creation typically fetches the current attribute
    value first on the main thread, and then again on the polling thread
    if it hasn't already changed.
    """
    if len(events) == 1 or events[0] != events[1]:
        events = [events[0]] + events
        logging.warning("duplicate first event missing")
    return events
