import asyncio
import json

import aiokafka
import numpy as np
import pytest
from tango import DevFailed

from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    KafkaConsumerSourceDescriptor,
    QueueConnectorDescriptor,
)
from ska_sdp_lmc_queue_connector.serialization import create_serializer
from ska_sdp_lmc_queue_connector.sourcesink import DataType
from ska_sdp_lmc_queue_connector.tango_dtype_helper import tango_format_to_shape, tango_to_dtypes
from ska_sdp_lmc_queue_connector.tango_object_scatter_sink import (
    TangoAttributeDescriptor,
    TangoObjectScatterAttributeSinkDescriptor,
)

KAFKA_HOST = "localhost:9092"
KAFKA_TOPIC = "kafka-topic"
FORMAT = "msgpack_numpy"


@pytest.fixture
def device_properties(
    dtype: np.dtype,
    shape: list[int],
    path: str,
    jmespath_filter: str,
    default_value: DataType,
):
    return {
        "exchanges_json": QueueConnectorDescriptor(
            exchanges=[
                ExchangeDescriptor(
                    dtype=object,
                    shape=[],
                    source=KafkaConsumerSourceDescriptor(
                        servers=KAFKA_HOST,
                        topic=KAFKA_TOPIC,
                        format=FORMAT,
                    ),
                    sink=TangoObjectScatterAttributeSinkDescriptor(
                        attributes=[
                            TangoAttributeDescriptor(
                                attribute_name="test_attr",
                                dtype=dtype,
                                shape=shape,
                                path=path,
                                filter=jmespath_filter,
                                default_value=default_value,
                            )
                        ]
                    ),
                ),
            ]
        ).model_dump_json_nodb(),
    }


# fmt: off
# flake8: noqa
@pytest.mark.parametrize("path", ["a"])
@pytest.mark.parametrize("jmespath_filter", [None, "a != undefined"])
@pytest.mark.parametrize("dtype, shape, default_value, value", [
    # scalars
    (dict,          [],     {}, {"key": "value"}),
    (str,           [],     'testvalue', ''),
    (bool,          [],     True, False),
    (np.uint8,      [],     3, 0),
    (np.uint16,     [],     3, 0),
    (np.uint32,     [],     3, 0),
    (np.uint64,     [],     3, 0),
    (np.int16,      [],     3, 0),
    (np.int32,      [],     3, 0),
    (np.int64,      [],     3, 0),
    (int,           [],     3, 0),
    (np.float32,    [],     3, 0),
    (np.float64,    [],     3, 0),
    (float,         [],     3, 0),
    # arrays
    (np.int64,      [3],    0, [1,2,3]),
    (np.int64,      [3],    0, np.array([1,2,3])),
    (np.int64,      [2,2],  0, [[1,2],[3,4]]),
    (np.int64,      [2,2],  0, np.array([[1,2],[3,4]])),
])
# fmt: on
@pytest.mark.asyncio
async def test_readwrite_attribute_type_from_dict(
    manual_qc, dtype: type, shape: list, default_value: DataType, value: DataType
):
    assert_attribute_read(manual_qc, dtype, shape, default_value)

    manual_qc.Start()
    async with aiokafka.AIOKafkaProducer(
            bootstrap_servers=KAFKA_HOST
        ) as producer:
        await producer.send_and_wait(
            KAFKA_TOPIC,
            create_serializer(object, shape, FORMAT)(
                {"a": value}
            ))
    await asyncio.sleep(0.5)
    manual_qc.Stop()

    assert_attribute_read(manual_qc, dtype, shape, value)
    with pytest.raises(DevFailed):
        assert_attribute_write(manual_qc, dtype, shape, default_value)

    # Test removal
    manual_qc.Standby()
    assert {"test_attr"}.intersection(manual_qc.get_attribute_list()) == set()


_INPUT_DTYPE = "U6,f8,f8,f8,f8"
_ANT1_DATA = np.array([("SKA001", 1., 2., 3., 4.)], dtype=_INPUT_DTYPE)
_ANT1_NAN_DATA = np.array([("SKA001", np.nan, np.nan, np.nan, np.nan)], dtype=_INPUT_DTYPE)
_ANT2_DATA = np.array([("SKA002", 0., 0., 0., 0.)], dtype=_INPUT_DTYPE)
_3_ANT_DATA = np.array([
    ("SKA001", 1., 2., 3., 4.),
    ("SKA002", 0., 0., 0., 0.),
    ("SKA003", -1., -2., -3., -4.),
], dtype=_INPUT_DTYPE)

@pytest.mark.parametrize("value, dtype, shape, default_value, jmespath_filter, path, expected", [
    pytest.param(*args[1:], id=args[0]) for args in [
    ("single element array", _ANT1_DATA, np.float64, [3], np.repeat(1, 3), "[?[0] == 'SKA001']", "[?[0] == 'SKA001'][[1], [4], [2]][]", np.array([1, 4, 2])),
    ("single element array, nan data", _ANT1_NAN_DATA, np.float64, [3], np.repeat(1, 3), "[?[0] == 'SKA001']", "[?[0] == 'SKA001'][[1], [4], [2]][]", np.repeat(np.nan, 3)),
    ("single element array, nan defaults", _ANT1_DATA, np.float64, [3], np.repeat(np.nan, 3), "[?[0] == 'SKA001']", "[?[0] == 'SKA001'][[1], [4], [2]][]", np.array([1, 4, 2])),
    ("single element array, no match", _ANT2_DATA, np.float64, [3], np.repeat(1, 3), "[?[0] == 'SKA001']" , "[?[0] == 'SKA001'][[1], [1], [1]][]", np.repeat(1, 3)),
    ("single element array, nan defaults, no match", _ANT1_DATA, np.float64, [3], np.repeat(np.nan, 3), "[?[0] == 'SKA001']", "[?[0] == 'SKA001'][[1], [4], [2]][]", np.array([1, 4, 2])),
    ("multi element array, matches element 1", _3_ANT_DATA, np.float64, [3], np.repeat(1, 3), "[?[0] == 'SKA001']" , "[?[0] == 'SKA001'][[1], [4], [2]][]", np.array([1, 4, 2])),
    ("multi element array, matches element 2", _3_ANT_DATA, np.float64, [3], np.repeat(1, 3), "[?[0] == 'SKA002']" , "[?[0] == 'SKA002'][[2], [2], [1]][]", np.array([0, 0, 0])),
    ("multi element array, matches element 3", _3_ANT_DATA, np.float64, [3], np.repeat(1, 3), "[?[0] == 'SKA003']" , "[?[0] == 'SKA003'][[1], [4], [2]][]", np.array([-1, -4, -2])),
    ]
])
@pytest.mark.asyncio
async def test_readwrite_attribute_type_from_structured_array(
    value: np.ndarray, manual_qc, dtype: type, shape: list, default_value: DataType, expected: DataType
):
    assert_attribute_read(manual_qc, dtype, shape, default_value)

    manual_qc.Start()
    async with aiokafka.AIOKafkaProducer(
            bootstrap_servers=KAFKA_HOST
        ) as producer:
        await producer.send_and_wait(
            KAFKA_TOPIC,
            create_serializer(object, shape, FORMAT)(
                value
            ))
    await asyncio.sleep(0.5)
    manual_qc.Stop()

    assert_attribute_read(manual_qc, dtype, shape, expected)
    with pytest.raises(DevFailed):
        assert_attribute_write(manual_qc, dtype, shape, default_value)

    # Test removal
    manual_qc.Standby()
    assert {"test_attr"}.intersection(manual_qc.get_attribute_list()) == set()


def assert_attribute_read(proxy, dtype, shape, value):
    # test read
    attr = proxy.get_attribute_config_ex("test_attr")[0]
    assert np.dtype(dtype) in tango_to_dtypes(attr.data_type)
    assert (
        tango_format_to_shape(attr.data_format, attr.max_dim_x, attr.max_dim_y)
        == shape
    )

    if dtype == dict:
        # type unsupported by tango
        assert type(proxy.test_attr) == str
        assert proxy.test_attr == json.dumps(value)
    else:
        # type supported by tango and numpy
        np.testing.assert_array_equal(proxy.test_attr, value)


def assert_attribute_write(proxy, dtype, shape, value):
    # test write
    if dtype == str:
        write_data = np.full(shape=shape, dtype=None, fill_value=value)
        # NOTE: write using ndarray is not supported for string
        # write using list
        proxy.test_attr = write_data.tolist()
        np.testing.assert_array_equal(proxy.test_attr, write_data)
    elif isinstance(proxy.test_attr, np.ndarray):
        write_data = np.full(
            shape=shape, dtype=proxy.test_attr.dtype, fill_value=value
        )
        # write using ndarray
        proxy.test_attr = write_data
        np.testing.assert_array_equal(proxy.test_attr, write_data)
        # write using list
        proxy.test_attr = write_data.tolist()
        np.testing.assert_array_equal(proxy.test_attr, write_data)
    elif dtype == dict:
        # write using JSON string
        write_data = json.dumps(value)
        proxy.test_attr = write_data
        assert proxy.test_attr == write_data
    else:
        # write using scalar or encoded
        write_data = value
        proxy.test_attr = write_data
        assert proxy.test_attr == write_data
        # write using list
        proxy.test_attr = np.array(value).tolist()
        assert proxy.test_attr == value
