import asyncio
import warnings
from io import BytesIO

import numpy as np
import pytest
from aiokafka import AIOKafkaConsumer
from astropy.time import Time

import ska_sdp_lmc_queue_connector.astream_utils as astream
from ska_sdp_lmc_queue_connector import (
    ExchangeDescriptor,
    InMemorySourceDescriptor,
    QueueConnectorDescriptor,
    TangoLocalAttributeSinkDescriptor,
)
from ska_sdp_lmc_queue_connector.kafka_sourcesink import KafkaProducerSinkDescriptor
from ska_sdp_lmc_queue_connector.tango_pointing_source import (
    TangoPointingSubscriptionSourceDescriptor,
)

KAFKA_HOST = "localhost:9092"
KAFKA_TOPIC = "pointing"
FORMAT = "npy"
TIME_PRECISON = {"ns": 1e-9, "us": 1e-6, "ms": 1e-3, "s": 1}


@pytest.fixture
def device_properties(units: str):
    return {
        "exchanges_json": QueueConnectorDescriptor(
            exchanges=[
                ExchangeDescriptor(
                    dtype=np.dtype(float),
                    shape=[3],
                    source=InMemorySourceDescriptor(
                        data=[
                            536544035.0,
                            536544036.0,  # leap second
                            536544037.0,
                            8276687237.0,  # beyond datetime64[ns]
                        ],
                        delay=0.02,
                    ),
                    sink=TangoLocalAttributeSinkDescriptor(
                        attribute_name="epoch_pointing",
                        default_value="0",  # epoch
                    ),
                ),
                ExchangeDescriptor(
                    dtype=np.dtype(
                        [
                            ("antenna_id", "int64"),
                            ("ts", f"datetime64[{units}]"),
                            ("az", "float64"),
                            ("el", "float64"),
                        ]
                    ),
                    shape=[-1],
                    source=TangoPointingSubscriptionSourceDescriptor(
                        device_name="test/sdp/1",
                        attribute_name="epoch_pointing",
                    ),
                    sink=KafkaProducerSinkDescriptor(
                        servers=KAFKA_HOST,
                        topic=KAFKA_TOPIC,
                        format=FORMAT,
                    ),
                ),
            ]
        ).model_dump_json_nodb(),
    }


@pytest.mark.asyncio
@pytest.mark.parametrize("units", ["ns"])
async def test_antenna_id(manual_qc):
    async with AIOKafkaConsumer(KAFKA_TOPIC, bootstrap_servers=KAFKA_HOST) as consumer:
        manual_qc.Start()
        try:
            message = await asyncio.wait_for(anext(consumer), 1.0)
            antenna_id = np.load(BytesIO(message.value))[0][0]
            assert antenna_id == 1
        finally:
            manual_qc.Stop()


@pytest.mark.asyncio
@pytest.mark.parametrize("units", ["ns", "us", "ms", "s"])
async def test_timestamp_leapsecond(manual_qc, units):
    precision: int = TIME_PRECISON[units]

    async with AIOKafkaConsumer(KAFKA_TOPIC, bootstrap_servers=KAFKA_HOST) as consumer:
        manual_qc.Start()
        try:
            # Default value is epoch
            assert await _next_pointing_time(consumer, precision) == (
                "1999-12-31T23:59:28.000",
                946684800.0,
                units,
            )

            # Check before and after leap second
            assert await _next_pointing_time(consumer, precision) == (
                "2016-12-31T23:59:59.000",
                1483228835.0,
                units,
            )
            assert await _next_pointing_time(consumer, precision) == (
                "2016-12-31T23:59:60.000",
                1483228836.0,
                units,
            )
            assert await _next_pointing_time(consumer, precision) == (
                "2017-01-01T00:00:00.000",
                1483228837.0,
                units,
            )
        finally:
            manual_qc.Stop()


@pytest.mark.asyncio
@pytest.mark.parametrize("units", ["ns", "us", "ms", "s"])
async def test_timestamp_limit(manual_qc, units):
    precision: int = TIME_PRECISON[units]

    with warnings.catch_warnings():
        # ignore astropy dubious year warning
        warnings.filterwarnings("ignore", category=UserWarning)
        async with AIOKafkaConsumer(KAFKA_TOPIC, bootstrap_servers=KAFKA_HOST) as consumer:
            manual_qc.Start()
            try:

                async def take():
                    return [i async for i in astream.take(consumer, 4)]

                await asyncio.wait_for(take(), 1.0)

                # check time past datetime64[ns] limit
                assert await _next_pointing_time(consumer, precision) == (
                    "2262-04-11T23:46:40.000",
                    9223372037.0,
                    units,
                ), pytest.xfail(reason="datetime64[ns] should throw on integer overflow")
            finally:
                manual_qc.Stop()


async def _next_pointing_time(
    consumer: AIOKafkaConsumer, precision: int
) -> tuple[str, float, str]:
    """
    Awaits the next pointing value and extracts both the utc timestamp and
    offset from Y2000 in seconds.

    Args:
        consumer (AIOKafkaConsumer): consumer pre-populated with pointings.

    Returns:
        tuple[str, float, str]: utc timestamp, Y2000 seconds offset, time units.
    """
    message = await asyncio.wait_for(anext(consumer), 1.0)
    datetime = np.load(BytesIO(message.value))[0][1]
    utc_timestamp = Time(str(datetime), format="isot", scale="tai").utc.isot
    unix_offset = datetime.astype(np.int64) * precision
    units = np.datetime_data(datetime)[0]
    return utc_timestamp, unix_offset, units
