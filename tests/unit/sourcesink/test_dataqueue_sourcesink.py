import asyncio
import enum

import numpy as np
import pytest
import pytest_asyncio
from aiokafka import AIOKafkaConsumer, AIOKafkaProducer, ConsumerStoppedError
from aiokafka.admin import AIOKafkaAdminClient

from ska_sdp_lmc_queue_connector.dataqueue_sourcesink import (
    DataQueueConsumerSource,
    DataQueueConsumerSourceDescriptor,
    DataQueueProducerSink,
    DataQueueProducerSinkDescriptor,
)
from ska_sdp_lmc_queue_connector.kafka_sourcesink import (
    KafkaConsumerSource,
    KafkaConsumerSourceDescriptor,
    KafkaProducerSink,
    KafkaProducerSinkDescriptor,
)
from ska_sdp_lmc_queue_connector.serialization import Format
from ska_sdp_lmc_queue_connector.sourcesink import DataSink, DataSource

KAFKA_HOST = "[::1]:9092"
TEST_TOPIC = "kafka-data-test-events"


@pytest_asyncio.fixture(name="clear_topics", autouse=True)
async def clear_topics_fixture():
    admin_client = AIOKafkaAdminClient(bootstrap_servers=KAFKA_HOST)
    await admin_client.start()
    topics = await admin_client.list_topics()
    to_remove = list({TEST_TOPIC} & set(topics))
    await admin_client.delete_topics(to_remove)
    await admin_client.close()


class DataQueueImpl(enum.Enum):
    DATAQUEUE = enum.auto()
    KAFKA = enum.auto()


@pytest_asyncio.fixture(name="unmanaged_sink")
async def unmanaged_sink_fixture(
    impl: DataQueueImpl, ser_format: Format, dtype: type, shape: list[int]
) -> DataSink:
    match impl:
        case DataQueueImpl.KAFKA:
            descriptor = KafkaProducerSinkDescriptor(
                servers=KAFKA_HOST, topic=TEST_TOPIC, format=ser_format
            )
            sink = KafkaProducerSink(descriptor, np.dtype(dtype), shape)
        case DataQueueImpl.DATAQUEUE:
            descriptor = DataQueueProducerSinkDescriptor(
                servers=KAFKA_HOST, topic=TEST_TOPIC, format=ser_format
            )
            sink = DataQueueProducerSink(descriptor, np.dtype(dtype), shape)
    return sink


@pytest_asyncio.fixture(name="unmanaged_source")
async def unmanaged_source_fixture(
    impl: DataQueueImpl, ser_format: Format, dtype: type, shape: list[int]
) -> DataSource:
    match impl:
        case DataQueueImpl.KAFKA:
            descriptor = KafkaConsumerSourceDescriptor(
                servers=KAFKA_HOST, topic=TEST_TOPIC, format=ser_format
            )
            sink = KafkaConsumerSource(descriptor, np.dtype(dtype), shape)
        case DataQueueImpl.DATAQUEUE:
            descriptor = DataQueueConsumerSourceDescriptor(
                servers=KAFKA_HOST, topic=TEST_TOPIC, format=ser_format
            )
            sink = DataQueueConsumerSource(descriptor, np.dtype(dtype), shape)
    return sink


@pytest_asyncio.fixture(name="data_sink")
async def sink_fixture(unmanaged_sink: DataSink):
    await unmanaged_sink.start()
    yield unmanaged_sink
    await unmanaged_sink.stop()


@pytest_asyncio.fixture(name="data_source")
async def source_fixture(unmanaged_source: DataSource):
    await unmanaged_source.start()
    yield unmanaged_source
    await unmanaged_source.stop()


@pytest_asyncio.fixture(name="kafka_consumer")
async def kafka_consumer_fixture():
    consumer = AIOKafkaConsumer(TEST_TOPIC, bootstrap_servers=KAFKA_HOST)
    await consumer.start()
    yield consumer
    await consumer.stop()


@pytest.mark.parametrize("ser_format,shape,dtype", [("utf-8", [], str)])
@pytest.mark.parametrize("impl", [DataQueueImpl.KAFKA, DataQueueImpl.DATAQUEUE])
@pytest.mark.asyncio
async def test_dataqueue_source_incorrect_starting_and_stopping_exception(
    unmanaged_source: DataSource,
):
    with pytest.raises(AssertionError):
        await unmanaged_source.stop()

    await unmanaged_source.start()
    with pytest.raises(AssertionError):
        await unmanaged_source.start()

    await unmanaged_source.stop()


@pytest.mark.parametrize("ser_format,shape,dtype", [("utf-8", [], str)])
@pytest.mark.parametrize("impl", [DataQueueImpl.KAFKA, DataQueueImpl.DATAQUEUE])
@pytest.mark.asyncio
async def test_dataqueue_source_read_while_stopped_exception(unmanaged_source: DataSource):
    with pytest.raises(StopAsyncIteration):
        await anext(unmanaged_source)

    await unmanaged_source.start()
    await unmanaged_source.stop()

    with pytest.raises(StopAsyncIteration):
        await anext(unmanaged_source)


@pytest.mark.parametrize(
    "impl",
    [
        pytest.param(DataQueueImpl.KAFKA),
        pytest.param(DataQueueImpl.DATAQUEUE, marks=pytest.mark.xfail),
    ],
)
@pytest.mark.parametrize(
    "ser_format,dtype,shape,data",
    [
        ("utf-8", str, [], "a"),
        ("utf-8", bytes, [], ("utf-8", b"a")),
        ("utf-8", float, [], 1e15),
        ("ascii", str, [], "a"),
        ("ascii", bytes, [], ("ascii", b"a")),
        ("ascii", float, [], 1e15),
        ("python", str, [], "a"),
        ("python", str, [2, 2], [["a", "a"], ["a", "a"]]),
        ("python", dict, [], {"a": 0}),
        ("python", float, [], 1e15),
        ("python", float, [2], [1e15, 1e15]),
        ("python", str, [2, 2], [["a", "a"], ["a", "a"]]),
        ("python", dict, [], {"a": 0}),
        ("json", str, [], "a"),
        ("json", float, [], 1e15),
        ("json", float, [2], [1e15, 1e15]),
        ("json", dict, [], {"a": 0}),
        ("msgpack_numpy", str, [], "message"),
        ("msgpack_numpy", str, [2, 2], [["a", "grid"], ["of", "str"]]),
        ("msgpack_numpy", np.float32, [], np.array(1e15, dtype=np.float32)),
        ("msgpack_numpy", float, [2], np.array([1e15, 1e15])),
        ("msgpack_numpy", dict, [], {"a": 0}),
        ("npy", str, [], "a"),
        ("npy", str, [2, 2], [["a", "a"], ["a", "a"]]),
        ("npy", int, [3], np.array([1, 2, 3])),
        ("npy", int, [2, 2], np.array([[1, 2], [3, 4]])),
        ("npy", float, [2, 2], np.array([[1.0, 2.0], [3.0, 4.0]])),
        ("carray", int, [3], np.array([1, 2, 3])),
        ("carray", int, [2, 2], np.array([[1, 2], [3, 4]])),
        ("carray", float, [2, 2], np.array([[1.0, 2.0], [3.0, 4.0]])),
    ],
)
@pytest.mark.asyncio
async def test_dataqueue_types(data_sink: DataSink, data_source: DataSource, data, dtype):
    dtype = np.dtype(dtype)
    await data_sink.awrite(data)
    actual = await anext(data_source)
    np.testing.assert_array_equal(actual, data, strict=True)

    if dtype == np.bytes_:
        assert isinstance(actual[0], str)
        assert isinstance(actual[1], bytes)
    else:
        assert np.issubdtype(actual.dtype, dtype)


@pytest.mark.parametrize("ser_format,dtype,shape", [("utf-8", str, [])])
@pytest.mark.parametrize("impl", [DataQueueImpl.KAFKA, DataQueueImpl.DATAQUEUE])
@pytest.mark.asyncio
async def test_dataqueue_source_is_iterable(
    data_sink: KafkaProducerSink, data_source: KafkaConsumerSource
):
    await data_sink.awrite("a")
    await data_sink.awrite("b")
    await data_sink.awrite("c")

    msgs: list[str] = []
    async for msg in data_source:
        msgs.append(msg)
        if len(msgs) == 3:
            break

    assert ["a", "b", "c"] == msgs


@pytest.mark.parametrize("ser_format,shape,dtype", [("utf-8", [], str)])
@pytest.mark.parametrize(
    "impl",
    [
        pytest.param(DataQueueImpl.KAFKA),
        pytest.param(DataQueueImpl.DATAQUEUE, marks=pytest.mark.xfail),
    ],
)
@pytest.mark.asyncio
async def test_dataqueue_source_stop_while_blocked_is_handled(unmanaged_source: DataSource):
    await unmanaged_source.start()

    # aread() should yield until we stop the consumer (task will be pending)
    task = asyncio.create_task(anext(unmanaged_source))

    # This should interrupt our blocked read
    await unmanaged_source.stop()

    done, pending = await asyncio.wait([task], timeout=3)
    assert len(done) == 1
    assert len(pending) == 0
    assert isinstance(task.exception(), StopAsyncIteration)


@pytest.mark.parametrize("ser_format,shape,dtype", [("utf-8", [], str)])
@pytest.mark.parametrize("impl", [DataQueueImpl.KAFKA, DataQueueImpl.DATAQUEUE])
@pytest.mark.asyncio
async def test_dataqueue_source_stop_while_iterating_is_handled(unmanaged_source: DataSource):
    await unmanaged_source.start()

    finished_iterating = False
    messages: list[str] = []

    async def iterate():
        # This should yield until we send a message, and then when we stop
        # the consumer
        async for msg in unmanaged_source:
            messages.append(msg)

        nonlocal finished_iterating
        finished_iterating = True

    task = asyncio.create_task(iterate())

    # Write a single message to the topic
    producer = AIOKafkaProducer(bootstrap_servers=KAFKA_HOST)
    await producer.start()
    await producer.send_and_wait(TEST_TOPIC, b"test")
    await producer.stop()

    # Give the iterator a chance to run on the event loop, otherwise the below
    # stop will occur before we've had a chance to read the message
    await asyncio.sleep(0.1)

    # This should make the iterator raise a StopAsyncIteration
    await unmanaged_source.stop()

    done, pending = await asyncio.wait([task], timeout=3)
    assert len(done) == 1
    assert len(pending) == 0
    assert not task.cancelled()
    assert not isinstance(task.exception(), ConsumerStoppedError)
    assert messages == ["test"]
    assert finished_iterating


@pytest.mark.parametrize("ser_format,shape,dtype", [("utf-8", [], str)])
@pytest.mark.parametrize("impl", [DataQueueImpl.KAFKA, DataQueueImpl.DATAQUEUE])
@pytest.mark.asyncio
async def test_dataqueue_sink_incorrectly_starting_and_stopping(unmanaged_sink: DataSink):
    with pytest.raises(AssertionError):
        await unmanaged_sink.stop()

    await unmanaged_sink.start()
    with pytest.raises(AssertionError):
        await unmanaged_sink.start()

    await unmanaged_sink.stop()


@pytest.mark.parametrize("ser_format,shape,dtype", [("utf-8", [], str)])
@pytest.mark.parametrize("impl", [DataQueueImpl.KAFKA, DataQueueImpl.DATAQUEUE])
@pytest.mark.asyncio
@pytest.mark.parametrize("before_start", [False, True])
async def test_dataqueue_sink_awrite_while_stopped(
    caplog, before_start: bool, unmanaged_sink: DataSink
):
    if not before_start:
        await unmanaged_sink.start()
        await unmanaged_sink.stop()
    await unmanaged_sink.awrite("test message")

    assert "WARNING" in caplog.text
    for record in caplog.records:
        assert record.levelname == "WARNING"
        assert "Producer to %s discarding data" in record.msg


@pytest.mark.parametrize("ser_format,shape,dtype", [("utf-8", [], str)])
@pytest.mark.parametrize("impl", [DataQueueImpl.KAFKA, DataQueueImpl.DATAQUEUE])
@pytest.mark.asyncio
async def test_dataqueue_sink_stop_while_send_in_flight_waits_for_send_to_complete(
    unmanaged_sink: DataSink,
    kafka_consumer: AIOKafkaConsumer,
):
    await unmanaged_sink.start()

    in_flight_task = asyncio.create_task(unmanaged_sink.awrite("in-flight"))
    stop_task = asyncio.create_task(unmanaged_sink.stop())
    await in_flight_task
    await stop_task

    msg = await kafka_consumer.getone()
    assert b"in-flight" == msg.value


@pytest.mark.parametrize("ser_format,shape,dtype", [("utf-8", [], str)])
@pytest.mark.parametrize("impl", [DataQueueImpl.KAFKA, DataQueueImpl.DATAQUEUE])
@pytest.mark.asyncio
async def test_dataqueue_sink_awrite_while_stopping_logs_and_drops(
    unmanaged_sink: DataSink, kafka_consumer: AIOKafkaConsumer, caplog: pytest.LogCaptureFixture
):
    await unmanaged_sink.start()

    stop_task = asyncio.create_task(unmanaged_sink.stop())
    in_flight_task = asyncio.create_task(unmanaged_sink.awrite("in-flight"))
    await in_flight_task
    await stop_task

    assert "discarding data due to closed sink" in caplog.text
    msgs = await kafka_consumer.getmany(timeout_ms=200)
    assert len(msgs) == 0
