import asyncio

import pytest

from ska_sdp_lmc_queue_connector.in_memory_sourcesink import (
    InMemorySink,
    InMemorySinkDescriptor,
    InMemorySource,
    InMemorySourceDescriptor,
)

DATA_IN = [i for i in range(4)]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "delay,expected",
    [
        (0.0, 4),
        (0.2, 2),
        (0.25, 1),
        (0.4, 1),
    ],
)
async def test_source(delay, expected):
    source = InMemorySource(InMemorySourceDescriptor(data=DATA_IN, delay=delay), int)
    data_out = []

    await source.start()

    async def stream_data():
        async for value in source:
            data_out.append(value)

    # stream all data
    done, pending = await asyncio.wait([asyncio.create_task(stream_data())], timeout=0.5)
    await asyncio.gather(*done)  # get exceptions
    assert len(done) == 0
    assert len(pending) == 1
    assert data_out == DATA_IN[:expected]

    # finalize tasks
    await source.stop()
    await asyncio.gather(*pending)

    assert data_out == DATA_IN[:expected]


@pytest.mark.asyncio
async def test_source_delay():
    source = InMemorySource(InMemorySourceDescriptor(data=["0"], delay=0.15), int)
    data_out = []

    async def stream_data():
        async for value in source:
            data_out.append(value)

    # restarting stream restarts timer
    await source.start()
    with pytest.raises(asyncio.TimeoutError):
        await asyncio.wait_for(stream_data(), timeout=0.1)
    assert len(data_out) == 0
    with pytest.raises(asyncio.TimeoutError):
        await asyncio.wait_for(stream_data(), timeout=0.1)
    assert len(data_out) == 0

    # resuming stream fetches data
    _, pending = await asyncio.wait([asyncio.create_task(stream_data())], timeout=0.1)
    assert len(pending) == 1
    assert len(data_out) == 0
    _, pending = await asyncio.wait(pending, timeout=0.1)
    assert len(pending) == 1
    assert len(data_out) == 1

    # streaming after stop awaits quickly
    await source.stop()
    p = list(pending)[0]
    await asyncio.wait_for(p, timeout=0.1)
    await asyncio.wait_for(stream_data(), timeout=0.1)


@pytest.mark.asyncio
async def test_source_exceptions():
    source = InMemorySource(InMemorySourceDescriptor(data=DATA_IN), int)
    data_out = []

    async def stream_data():
        # start not called before reading
        with pytest.raises(StopAsyncIteration):
            await anext(source)

        # stream restart
        await source.start()
        await source.stop()

        # start not called before reading
        with pytest.raises(StopAsyncIteration):
            await anext(source)

        await source.start()
        async for value in source:
            data_out.append(value)
            if len(data_out) == len(DATA_IN):
                await source.stop()
        await source.stop()

    # stream all data
    done, pending = await asyncio.wait([asyncio.create_task(stream_data())], timeout=3)
    assert len(done) == 1
    assert len(pending) == 0

    # finalize tasks
    await asyncio.gather(*pending)
    await asyncio.gather(*done)  # get exceptions
    assert all(task.cancel() for task in pending)  # cancel unfinished

    assert data_out == DATA_IN


@pytest.mark.asyncio
async def test_sink():
    sink = InMemorySink(InMemorySinkDescriptor(key="1"))
    data_out = []

    await sink.start()
    for value in DATA_IN:
        await sink.awrite(value)
    await sink.stop()

    while not sink.queue.empty():
        data_out.append(sink.queue.get_nowait())
    assert data_out == DATA_IN
