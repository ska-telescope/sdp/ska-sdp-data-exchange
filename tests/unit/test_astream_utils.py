import asyncio
from typing import AsyncGenerator

import pytest

import ska_sdp_lmc_queue_connector.astream_utils as astream


async def async_generator(values) -> AsyncGenerator[int, None]:
    for value in values:
        await asyncio.sleep(0.1)  # Simulate async delay
        yield value


@pytest.mark.asyncio
async def test_astream_merge():
    iter1 = async_generator([1, 4, 7])
    iter2 = async_generator([2, 5, 8])
    iter3 = async_generator([3, 6, 9])

    outputs = [item async for item in astream.merge(iter1, iter2, iter3)]
    assert outputs == [1, 2, 3, 4, 5, 6, 7, 8, 9]


@pytest.mark.asyncio
async def test_astream_range():

    outputs = [item async for item in astream.arange(3)]
    assert outputs == [0, 1, 2]


@pytest.mark.asyncio
async def test_astream_spaceout():

    loop = asyncio.get_event_loop()
    spaceout_interval = 0.1
    yield_interval = 0.05
    # total interval is 0.15s

    target_time = loop.time()
    async for _ in astream.spaceout(astream.arange(20), interval=spaceout_interval):
        target_delta = loop.time() - target_time
        assert target_delta > 0.0
        assert target_delta < 0.01
        await asyncio.sleep(yield_interval)
        target_time = loop.time() + spaceout_interval
