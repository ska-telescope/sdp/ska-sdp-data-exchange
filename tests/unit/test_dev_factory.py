import asyncio
import logging

import pytest
from tango import GreenMode
from tango.server import Device, attribute, command
from tango.test_context import MultiDeviceTestContext

from ska_sdp_lmc_queue_connector.dev_factory import DevFactory

logger = logging.getLogger(__name__)


class AsyncDevice(Device):
    green_mode = GreenMode.Asyncio

    def init_device(self):
        self._value = 0

    @attribute
    def value(self):
        return self._value

    @command(dtype_out="DevShort")
    async def increment(self):
        asyncio.sleep(0.1)
        self._value += 1
        return self._value


@pytest.fixture()
def devices_to_load():
    return ({"class": AsyncDevice, "devices": [{"name": "test/device/1"}]},)


@pytest.mark.asyncio
async def test_async_proxy_throws(tango_context: MultiDeviceTestContext):
    logger.debug(tango_context)
    dev_factory = DevFactory()

    # Async proxy in tests is unsupported. For more details see
    # https://gitlab.com/tango-controls/pytango/-/issues/509
    with pytest.raises(RuntimeError):
        await dev_factory.get_async_proxy("test/device/1")


def test_sync_proxy_works(tango_context: MultiDeviceTestContext):
    logger.debug(tango_context)
    dev_factory = DevFactory()

    proxy = dev_factory.get_sync_proxy("test/device/1")

    assert proxy.get_green_mode() == GreenMode.Synchronous

    value = proxy.increment()
    assert value == 1

    attr = proxy.read_attribute("value")
    assert attr.value == 1

    assert proxy.value == 1


def test_proxies_are_cached(tango_context: MultiDeviceTestContext):
    logger.debug(tango_context)
    dev_factory = DevFactory()

    proxy1 = dev_factory.get_sync_proxy("test/device/1")
    proxy2 = dev_factory.get_sync_proxy("test/device/1")
    assert proxy1 == proxy2
