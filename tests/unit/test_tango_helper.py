import numpy as np
import pytest

from tests.tango_helper import range_split


@pytest.mark.parametrize(
    "value,sections,expected",
    [
        (
            # Single split
            range(0, 10),
            1,
            [
                range(0, 10),
            ],
        ),
        (
            # Even split
            range(0, 16),
            4,
            [
                range(0, 4),
                range(4, 8),
                range(8, 12),
                range(12, 16),
            ],
        ),
        (
            # Uneven split
            range(10, 20),
            3,
            [
                range(10, 14),
                range(14, 17),
                range(17, 20),
            ],
        ),
        (
            # Large split
            range(0, 4),
            10,
            [
                range(0, 1),
                range(1, 2),
                range(2, 3),
                range(3, 4),
                range(4, 4),
                range(4, 4),
                range(4, 4),
                range(4, 4),
                range(4, 4),
                range(4, 4),
            ],
        ),
    ],
)
def test_range_split(value, sections, expected):
    # check expected values comply with array_split
    a = np.array_split(np.zeros(len(value)), sections)
    for a, b in zip(expected, a):
        assert len(a) == len(b)

    actual = range_split(value, sections)
    assert len(actual) == len(expected)
    assert actual == expected


def test_range_split_no_sections():
    with pytest.raises(ValueError):
        range_split(range(10), -1)

    with pytest.raises(ValueError):
        range_split(range(10), -1)


def test_range_split_backward():
    with pytest.raises(ValueError):
        range_split(range(10, 0), 2)

    with pytest.raises(ValueError):
        range_split(range(10, 0, -1), 2)
