"""Package versioning tests."""

from importlib.metadata import version

PACKAGE_VER = version("ska-sdp-lmc-queue-connector")


def test_cicd_version():
    """Test CICD version matches package version."""
    with open(".release", encoding="utf-8") as f:
        lines = f.readlines()
        assert f"release={PACKAGE_VER}\n" in lines
        assert f"tag={PACKAGE_VER}\n" in lines
