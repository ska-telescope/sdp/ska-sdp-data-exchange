import asyncio

import pytest
from ska_sdp_config import Config

from ska_sdp_lmc_queue_connector.watcher.descriptor.descriptor_watcher_factory import (
    create_descriptor_watcher,
)

PB_ID1 = "pipeline1"
PB_ID2 = "pipeline2"
PB_ID3 = "pipeline3"
CONFIG_PATH = "/component/lmc-queueconnector-01/owner"
CONFIG_PATH1 = f"/component/lmc-queueconnector-01/owner/{PB_ID1}"
CONFIG_PATH2 = f"/component/lmc-queueconnector-01/owner/{PB_ID2}"
CONFIG_PATH3 = f"/component/lmc-queueconnector-01/owner/{PB_ID3}"


def clear_configs(config):
    config.backend.delete(CONFIG_PATH, recursive=True, must_exist=False)


@pytest.fixture(name="config")
def sdp_config_fixture():
    config = Config(backend="etcd3")
    clear_configs(config)
    yield config
    clear_configs(config)


@pytest.mark.asyncio
@pytest.mark.parametrize("use_etcd3", [False, True])
async def test_single_watch_create(config: Config, use_etcd3: bool):
    outputs = []

    async def run_watcher(c):
        async with create_descriptor_watcher(c, pb_id=PB_ID1, use_etcd3=use_etcd3) as watcher:
            async for config in watcher:
                outputs.append(config)

    task = asyncio.create_task(run_watcher(config))
    await asyncio.sleep(0.1)

    config.backend.create(CONFIG_PATH1, "")
    if not use_etcd3:
        await asyncio.sleep(0.1)
    config.backend.update(CONFIG_PATH1, "1")

    with pytest.raises(asyncio.TimeoutError):
        await asyncio.wait_for(task, timeout=1)

    assert outputs == [
        (CONFIG_PATH1, None),
        (CONFIG_PATH1, ""),
        (CONFIG_PATH1, "1"),
    ]


@pytest.mark.asyncio
@pytest.mark.parametrize("use_etcd3", [False, True])
async def test_multi_watch_create(config: Config, use_etcd3: bool):
    outputs = []

    async def run_watcher(c: Config):
        async with create_descriptor_watcher(c, pb_id=None, use_etcd3=use_etcd3) as watcher:
            async for kv in watcher:
                outputs.append(kv)

    task = asyncio.create_task(run_watcher(config))
    await asyncio.sleep(1)

    for txn in config.txn():
        txn.raw.create(CONFIG_PATH1, "1")
    for txn in config.txn():
        txn.raw.create(CONFIG_PATH2, "2")
    for txn in config.txn():
        txn.raw.create(CONFIG_PATH3, "3")
    for txn in config.txn():
        txn.raw.update(CONFIG_PATH1, "4")
    for txn in config.txn():
        txn.raw.update(CONFIG_PATH2, "5")
    for txn in config.txn():
        txn.raw.update(CONFIG_PATH3, "6")

    with pytest.raises(asyncio.TimeoutError):
        await asyncio.wait_for(task, timeout=1)

    assert outputs == [
        (CONFIG_PATH1, "1"),
        (CONFIG_PATH2, "2"),
        (CONFIG_PATH3, "3"),
        (CONFIG_PATH1, "4"),
        (CONFIG_PATH2, "5"),
        (CONFIG_PATH3, "6"),
    ]


@pytest.mark.asyncio
@pytest.mark.parametrize("use_etcd3", [False, True])
async def test_single_watch_update(config: Config, use_etcd3: bool):
    config.backend.create(CONFIG_PATH1, "")
    outputs = []

    async def run_watcher(c):
        async with create_descriptor_watcher(c, pb_id=PB_ID1, use_etcd3=use_etcd3) as watcher:
            async for config in watcher:
                outputs.append(config)

    task = asyncio.create_task(run_watcher(config))
    await asyncio.sleep(0.1)

    config.backend.update(CONFIG_PATH1, "1")
    if not use_etcd3:
        await asyncio.sleep(0.1)
    config.backend.update(CONFIG_PATH1, "2")
    if not use_etcd3:
        await asyncio.sleep(0.1)
    config.backend.update(CONFIG_PATH1, "3")

    with pytest.raises(asyncio.TimeoutError):
        await asyncio.wait_for(task, timeout=1)

    assert outputs == [
        (CONFIG_PATH1, ""),
        (CONFIG_PATH1, "1"),
        (CONFIG_PATH1, "2"),
        (CONFIG_PATH1, "3"),
    ]


@pytest.mark.asyncio
@pytest.mark.parametrize("use_etcd3", [False, True])
async def test_multi_watch_update(config: Config, use_etcd3: bool):
    outputs = []
    config.backend.create(CONFIG_PATH1, "")
    config.backend.create(CONFIG_PATH2, "")
    config.backend.create(CONFIG_PATH3, "")

    async def run_watcher(c: Config):
        async with create_descriptor_watcher(c, pb_id=None, use_etcd3=use_etcd3) as watcher:
            async for kv in watcher:
                outputs.append(kv)

    task = asyncio.create_task(run_watcher(config))
    await asyncio.sleep(1)

    config.backend.update(CONFIG_PATH1, "1")
    config.backend.update(CONFIG_PATH2, "2")
    config.backend.update(CONFIG_PATH3, "3")
    if not use_etcd3:
        await asyncio.sleep(0.1)

    config.backend.update(CONFIG_PATH1, "4")
    config.backend.update(CONFIG_PATH2, "5")
    config.backend.update(CONFIG_PATH3, "6")

    with pytest.raises(asyncio.TimeoutError):
        await asyncio.wait_for(task, timeout=1)

    assert outputs == [
        (CONFIG_PATH1, ""),
        (CONFIG_PATH2, ""),
        (CONFIG_PATH3, ""),
        (CONFIG_PATH1, "1"),
        (CONFIG_PATH2, "2"),
        (CONFIG_PATH3, "3"),
        (CONFIG_PATH1, "4"),
        (CONFIG_PATH2, "5"),
        (CONFIG_PATH3, "6"),
    ]


@pytest.mark.asyncio
@pytest.mark.parametrize("use_etcd3", [False, True])
async def test_single_watch_delete(config: Config, use_etcd3: bool):
    config.backend.create(CONFIG_PATH1, "")
    outputs = []

    async def run_watcher(c):
        async with create_descriptor_watcher(c, pb_id=PB_ID1, use_etcd3=use_etcd3) as watcher:
            async for config in watcher:
                outputs.append(config)

    task = asyncio.create_task(run_watcher(config))
    await asyncio.sleep(0.1)

    config.backend.delete(CONFIG_PATH1)

    with pytest.raises(asyncio.TimeoutError):
        await asyncio.wait_for(task, timeout=1)

    assert outputs == [
        (CONFIG_PATH1, ""),
        (CONFIG_PATH1, None),
    ]


@pytest.mark.asyncio
@pytest.mark.parametrize("use_etcd3", [False, True])
async def test_multi_watch_delete(config: Config, use_etcd3: bool):
    outputs = []
    config.backend.create(CONFIG_PATH1, "")
    config.backend.create(CONFIG_PATH2, "")
    config.backend.create(CONFIG_PATH3, "")

    async def run_watcher(c: Config):
        async with create_descriptor_watcher(c, pb_id=None, use_etcd3=use_etcd3) as watcher:
            async for kv in watcher:
                outputs.append(kv)

    task = asyncio.create_task(run_watcher(config))
    await asyncio.sleep(1)

    config.backend.delete(CONFIG_PATH1)
    config.backend.delete(CONFIG_PATH2)
    config.backend.delete(CONFIG_PATH3)

    with pytest.raises(asyncio.TimeoutError):
        await asyncio.wait_for(task, timeout=1)

    assert outputs == [
        (CONFIG_PATH1, ""),
        (CONFIG_PATH2, ""),
        (CONFIG_PATH3, ""),
        (CONFIG_PATH1, None),
        (CONFIG_PATH2, None),
        (CONFIG_PATH3, None),
    ]
