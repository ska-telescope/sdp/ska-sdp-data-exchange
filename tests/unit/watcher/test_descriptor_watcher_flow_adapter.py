import asyncio
import logging

import numpy as np
import pytest
from ska_sdp_config import Config
from ska_sdp_config.entity.flow import DataQueue, Flow, FlowSource, TangoAttributeUrl

from ska_sdp_lmc_queue_connector.kafka_sourcesink import KafkaProducerSinkDescriptor
from ska_sdp_lmc_queue_connector.sdp_queue_connector_descriptor import (
    ExchangeDescriptor,
    QueueConnectorDescriptor,
)
from ska_sdp_lmc_queue_connector.tango_sourcesink import TangoSubscriptionSourceDescriptor
from ska_sdp_lmc_queue_connector.watcher import create_descriptor_watcher

PIPELINES = [
    Flow.Key(pb_id="pb-test-00000000-0", kind="data-queue", name="flow1"),
    Flow.Key(pb_id="pb-test-00000000-1", kind="data-queue", name="flow2"),
    Flow.Key(pb_id="pb-test-00000000-2", kind="data-queue", name="flow3"),
]


def create_test_flow(flow_idx: int, format_name: str):
    return Flow(
        key=Flow.Key(
            pb_id=PIPELINES[flow_idx].pb_id,
            name=PIPELINES[flow_idx].name,
        ),
        data_model="Metrics",
        sink=DataQueue(topics="example-topic", host="kafka://[::1]:9092", format=format_name),
        sources=[
            FlowSource(
                uri=TangoAttributeUrl("tango://test/sdp/1/test_attr"),
                function="ska-sdp-lmc-queue-connector:a",
            )
        ],
    )


def create_expected_descriptor(format_name: str):
    return QueueConnectorDescriptor(
        exchanges=[
            ExchangeDescriptor(
                dtype=np.dtype(object),
                shape=[],
                sink=KafkaProducerSinkDescriptor(
                    servers="[::1]:9092", topic="example-topic", format=format_name
                ),
                source=[
                    TangoSubscriptionSourceDescriptor(
                        device_name="test/sdp/1",
                        attribute_name="test_attr",
                    )
                ],
            ),
        ]
    ).model_dump_json()


def create_empty_descriptor():
    return QueueConnectorDescriptor(exchanges=[]).model_dump_json()


@pytest.fixture(name="config")
def sdp_config_fixture():
    config = Config(backend="etcd3")
    # must delete both entities and subentities
    for txn in config.txn():
        for path in txn.raw.list_keys("/flow", recurse=2):
            logging.warning("deleting: %s", path)
            txn.raw.delete(path, must_exist=False)
    yield config
    for txn in config.txn():
        for path in txn.raw.list_keys("/flow", recurse=2):
            logging.warning("deleting: %s", path)
            txn.raw.delete(path, must_exist=False)


@pytest.mark.asyncio
async def test_multi_watch_create_and_update(config: Config):
    outputs = []

    async def run_watcher():
        async with create_descriptor_watcher(config, use_flow=True) as watcher:
            async for key_value in watcher:
                outputs.append(key_value)

    task = asyncio.create_task(run_watcher())
    await asyncio.sleep(1)

    flow0a = create_test_flow(0, "msgpack_numpy")
    flow1a = create_test_flow(1, "msgpack_numpy")
    for txn in config.txn():
        txn.flow.create(flow0a)
        txn.flow.create(flow1a)

    flow0b = create_test_flow(0, "npy")
    flow1b = create_test_flow(1, "npy")
    flow2b = create_test_flow(2, "npy")
    for txn in config.txn():
        txn.flow.update(flow0b)
        txn.flow.update(flow1b)
        txn.flow.create(flow2b)

    with pytest.raises(asyncio.TimeoutError):
        await asyncio.wait_for(task, timeout=2)

    expected = [
        (PIPELINES[0].pb_id, create_expected_descriptor("msgpack_numpy")),
        (PIPELINES[1].pb_id, create_expected_descriptor("msgpack_numpy")),
        (PIPELINES[0].pb_id, create_expected_descriptor("npy")),
        (PIPELINES[1].pb_id, create_expected_descriptor("npy")),
        (PIPELINES[2].pb_id, create_expected_descriptor("npy")),
    ]

    assert set(outputs) == set(expected)
    await asyncio.get_running_loop().shutdown_default_executor()


@pytest.mark.asyncio
async def test_multi_watch_delete(config: Config):

    flow0a = create_test_flow(0, "msgpack_numpy")
    flow1a = create_test_flow(1, "msgpack_numpy")
    flow2a = create_test_flow(2, "msgpack_numpy")
    for txn in config.txn():
        txn.flow.create_or_update(flow0a)
        txn.flow.create_or_update(flow1a)
        txn.flow.create_or_update(flow2a)
    outputs = []

    async def run_watcher():
        async with create_descriptor_watcher(config, use_flow=True) as watcher:
            async for key_value in watcher:
                outputs.append(key_value)

    task = asyncio.create_task(run_watcher())
    await asyncio.sleep(1)

    for txn in config.txn():
        txn.flow.delete(flow2a.key)
    for txn in config.txn():
        txn.flow.delete(flow1a.key)
        txn.flow.delete(flow0a.key)

    with pytest.raises(asyncio.TimeoutError):
        await asyncio.wait_for(task, timeout=3)

    expected = [
        (PIPELINES[0].pb_id, create_expected_descriptor("msgpack_numpy")),
        (PIPELINES[1].pb_id, create_expected_descriptor("msgpack_numpy")),
        (PIPELINES[2].pb_id, create_expected_descriptor("msgpack_numpy")),
        (PIPELINES[2].pb_id, create_empty_descriptor()),
        (PIPELINES[0].pb_id, create_empty_descriptor()),
        (PIPELINES[1].pb_id, create_empty_descriptor()),
    ]
    assert set(outputs) == set(expected)
    await asyncio.get_running_loop().shutdown_default_executor()
