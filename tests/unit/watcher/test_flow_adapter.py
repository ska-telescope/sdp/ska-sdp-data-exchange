import logging

import numpy as np
import pytest
from ska_sdp_config import Config
from ska_sdp_config.entity.flow import (
    DataQueue,
    Flow,
    FlowSource,
    TangoAttribute,
    TangoAttributeMap,
    TangoAttributeUrl,
)

from ska_sdp_lmc_queue_connector.dataqueue_sourcesink import (
    DataQueueConsumerSourceDescriptor,
    DataQueueProducerSinkDescriptor,
)
from ska_sdp_lmc_queue_connector.exchange import ExchangeDescriptor
from ska_sdp_lmc_queue_connector.kafka_sourcesink import (
    KafkaConsumerSourceDescriptor,
    KafkaProducerSinkDescriptor,
)
from ska_sdp_lmc_queue_connector.pipe.buffer_pipe import BufferWithTimePipeDescriptor
from ska_sdp_lmc_queue_connector.tango_object_scatter_sink import (
    TangoAttributeDescriptor,
    TangoObjectScatterAttributeSinkDescriptor,
)
from ska_sdp_lmc_queue_connector.tango_pointing_source import (
    TangoPointingSubscriptionSourceDescriptor,
)
from ska_sdp_lmc_queue_connector.tango_sourcesink import TangoSubscriptionSourceDescriptor
from ska_sdp_lmc_queue_connector.watcher.descriptor.flow_adapter import (
    try_adapt_flow_and_take_ownership,
)


@pytest.fixture(name="config")
def sdp_config_fixture():
    config = Config(backend="etcd3")
    # must delete both entities and subentities
    for txn in config.txn():
        for path in txn.raw.list_keys("/flow", recurse=2):
            logging.warning("deleting: %s", path)
            txn.raw.delete(path, must_exist=False)
    yield config
    for txn in config.txn():
        for path in txn.raw.list_keys("/flow", recurse=2):
            logging.warning("deleting: %s", path)
            txn.raw.delete(path, must_exist=False)


@pytest.mark.parametrize("use_kafka", [True, False])
def test_adapt_tango(config: Config, use_kafka: bool):
    flow = Flow(
        key=Flow.Key(
            pb_id="pb-test-00000000-0",
            name="flow0",
        ),
        data_model="Metrics",
        sink=DataQueue(topics="example-topic", host="kafka://host:9092", format="msgpack_numpy"),
        sources=[
            FlowSource(
                uri=TangoAttributeUrl("tango://test/sdp/1/test_attr"),
                function="ska-sdp-lmc-queue-connector:a",
            )
        ],
    )
    ProducerType = KafkaProducerSinkDescriptor if use_kafka else DataQueueProducerSinkDescriptor
    assert ExchangeDescriptor(
        dtype=np.dtype(object),
        shape=[],
        sink=ProducerType(servers="host:9092", topic="example-topic", format="msgpack_numpy"),
        source=[
            TangoSubscriptionSourceDescriptor(device_name="test/sdp/1", attribute_name="test_attr")
        ],
    ) == try_adapt_flow_and_take_ownership(config, flow, use_kafka=use_kafka)


POINTING_DTYPE = np.dtype(
    [
        ("antenna_name", "<U6"),
        ("ts", "datetime64[ns]"),
        ("az", "float64"),
        ("el", "float64"),
    ]
)


@pytest.mark.parametrize("use_kafka", [True, False])
def test_adapt_pointingtable(config: Config, use_kafka: bool):
    flow = Flow(
        key=Flow.Key(
            pb_id="pb-test-00000000-0",
            name="flow0",
        ),
        data_model="PointingTable",
        sink=DataQueue(topics="example-topic", host="kafka://host:9092", format="npy"),
        sources=[
            FlowSource(
                uri=TangoAttributeUrl("tango://test-sdp/dishmaster/ska001/achievedPointing"),
                function="ska-sdp-lmc-queue-connector:a",
            )
        ],
    )
    ProducerType = KafkaProducerSinkDescriptor if use_kafka else DataQueueProducerSinkDescriptor
    expected = ExchangeDescriptor(
        dtype=np.dtype(
            [
                ("antenna_name", "<U6"),
                ("ts", "datetime64[ns]"),
                ("az", "float64"),
                ("el", "float64"),
            ]
        ),
        shape=[-1],
        sink=ProducerType(servers="host:9092", topic="example-topic", format="npy"),
        pipe=BufferWithTimePipeDescriptor(timespan=1.0, flatten=True),
        source=[
            TangoPointingSubscriptionSourceDescriptor(
                device_name="test-sdp/dishmaster/ska001",
                attribute_name="achievedPointing",
                antenna_name="ska001",
            )
        ],
    )
    actual = try_adapt_flow_and_take_ownership(config, flow, use_kafka=use_kafka)

    assert expected == actual


@pytest.mark.parametrize("use_kafka", [True, False])
def test_adapt_qametrics(config: Config, use_kafka: bool):

    for txn in config.txn():
        txn.flow.create(
            Flow(
                key=Flow.Key(
                    pb_id="pb-test-00000000-0",
                    name="flow0",
                ),
                data_model="object[]",
                sink=DataQueue(topics="qametrics", host="kafka://host:9092", format="json"),
                sources=[],
            )
        )
        flow = Flow(
            key=Flow.Key(
                pb_id="pb-test-00000000-0",
                name="flow1",
            ),
            data_model="object[]",
            sink=TangoAttributeMap(
                attributes=[
                    (
                        TangoAttribute(
                            attribute_url="tango://test-sdp/queueconnector/01/receive_state",
                            dtype="DevString",
                            default_value="",
                        ),
                        TangoAttributeMap.DataQuery(when="type=='update'", select="state"),
                    ),
                    (
                        TangoAttribute(
                            attribute_url="tango://test-sdp/queueconnector/01/scan_id",
                            dtype="DevLong64",
                            default_value=0,
                        ),
                        TangoAttributeMap.DataQuery(when="type=='update'", select="scan_id"),
                    ),
                    (
                        TangoAttribute(
                            attribute_url="tango://test-sdp/queueconnector/01/stream_0_heaps",
                            dtype="DevDouble",
                            default_value=0,
                        ),
                        TangoAttributeMap.DataQuery(
                            when="type=='update' && streams != undefined && "
                            "contains(streams[].id, '0')",
                            select="streams[].heaps | [0]",
                        ),
                    ),
                ]
            ),
            sources=[
                FlowSource(
                    uri=Flow.Key(pb_id="pb-test-00000000-0", kind="data-queue", name="flow0"),
                    function="ska-sdp-lmc-queue-connector:exchange",
                )
            ],
        )
        txn.flow.create(flow)

    ConsumerType = (
        KafkaConsumerSourceDescriptor if use_kafka else DataQueueConsumerSourceDescriptor
    )
    expected = ExchangeDescriptor(
        dtype=np.dtype(object),
        source=[
            ConsumerType(
                servers="host:9092",
                topic="qametrics",
                format="json",
            )
        ],
        sink=TangoObjectScatterAttributeSinkDescriptor(
            attributes=[
                TangoAttributeDescriptor(
                    attribute_name="receive_state",
                    filter="type=='update'",
                    path="state",
                    dtype=np.dtype(str),
                    default_value="",
                ),
                TangoAttributeDescriptor(
                    attribute_name="scan_id",
                    filter="type=='update'",
                    path="scan_id",
                    dtype=np.dtype(int),
                    default_value=0,
                ),
                TangoAttributeDescriptor(
                    attribute_name="stream_0_heaps",
                    filter=(
                        "type=='update' && streams != undefined && " "contains(streams[].id, '0')"
                    ),
                    path="streams[].heaps | [0]",
                    dtype=np.dtype(float),
                    default_value=0,
                ),
            ]
        ),
    )
    actual = try_adapt_flow_and_take_ownership(config, flow, use_kafka=use_kafka)
    assert expected == actual


@pytest.mark.parametrize("use_kafka", [True, False])
def test_adapt_pointing_offset(config: Config, use_kafka: bool):
    for txn in config.txn():
        data_queue_flow = Flow(
            key=Flow.Key(
                pb_id="pb-test-00000000-0",
                name="flow0",
            ),
            data_model="PointingNumpyArray",
            sink=DataQueue(topics="qametrics", host="kafka://host:9092", format="msgpack_numpy"),
            sources=[],
        )
        txn.flow.create(data_queue_flow)
        flow = Flow(
            key=Flow.Key(
                pb_id="pb-test-00000000-0",
                name="flow1",
            ),
            data_model="PointingNumpyArray",
            sink=TangoAttributeMap(
                attributes=[
                    (
                        TangoAttribute(
                            attribute_url=TangoAttributeUrl(
                                f"tango://mid-sdp/queueconnector/01/pointing_offset_{antenna}"
                            ),
                            dtype="DevDouble",
                            max_dim_x=3,
                            default_value=float("0"),
                        ),
                        TangoAttributeMap.DataQuery(
                            select=f"[?[0]=='{antenna}'][[1], [2], [4]][]"
                        ),
                    )
                    for antenna in ["SKA001", "SKA002"]
                ]
            ),
            sources=[
                FlowSource(
                    uri=data_queue_flow.key,
                    function="ska-sdp-lmc-queue-connector:exchange",
                )
            ],
        )
        txn.flow.create(flow)

    actual = try_adapt_flow_and_take_ownership(config, flow, use_kafka=use_kafka)

    ConsumerType = (
        KafkaConsumerSourceDescriptor if use_kafka else DataQueueConsumerSourceDescriptor
    )
    expected = ExchangeDescriptor(
        dtype=np.dtype("O"),
        shape=[],
        source=[
            ConsumerType(
                servers="host:9092",
                topic="qametrics",
                format="msgpack_numpy",
            )
        ],
        sink=TangoObjectScatterAttributeSinkDescriptor(
            attributes=[
                TangoAttributeDescriptor(
                    attribute_name="pointing_offset_SKA001",
                    dtype=np.dtype("float64"),
                    shape=[3],
                    path="[?[0]=='SKA001'][[1], [2], [4]][]",
                    filter=None,
                    default_value=float("0"),
                ),
                TangoAttributeDescriptor(
                    attribute_name="pointing_offset_SKA002",
                    dtype=np.dtype("float64"),
                    shape=[3],
                    path="[?[0]=='SKA002'][[1], [2], [4]][]",
                    filter=None,
                    default_value=float("0"),
                ),
            ],
        ),
    )
    assert expected == actual
