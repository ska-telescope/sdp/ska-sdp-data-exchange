import asyncio
from contextlib import suppress

import pytest
from ska_sdp_config import Config
from ska_sdp_config.entity.flow import DataQueue, Flow, FlowSource, TangoAttributeUrl

from ska_sdp_lmc_queue_connector.watcher.flow.db_flow_watcher import (
    DbFlowWatcher,
    _dict_symmetric_difference_deep,
)


def test_symmetric_difference_deep():
    assert _dict_symmetric_difference_deep({}, {}) == []
    assert _dict_symmetric_difference_deep({"a": 1}, {}) == ["a"]
    assert _dict_symmetric_difference_deep({}, {"a": 1}) == ["a"]
    assert _dict_symmetric_difference_deep({"a": 1, "b": 2}, {"a": 1, "b": 1}) == ["b"]


PIPELINES = [
    Flow.Key(pb_id="pb-test-00000000-0", kind="data-queue", name="flow0"),
    Flow.Key(pb_id="pb-test-00000000-1", kind="data-queue", name="flow1"),
    Flow.Key(pb_id="pb-test-00000000-2", kind="data-queue", name="flow2"),
]


def create_test_flow(n: int, format_name: str):
    return Flow(
        key=Flow.Key(
            pb_id=PIPELINES[n].pb_id,
            kind=PIPELINES[n].kind,
            name=PIPELINES[n].name,
        ),
        data_model="Metrics",
        sink=DataQueue(
            topics=[(0, "example-topic")], host="kafka://localhost", format=format_name
        ),
        sources=[
            FlowSource(
                uri=TangoAttributeUrl("tango://test/sdp/1/test_attr"),
                function="ska-sdp-lmc-queue-connector:a",
            )
        ],
    )


def clear_flows(config: Config):
    for txn in config.txn():
        flow_keys = txn.flow.list_keys()
        for flow_key in flow_keys:
            txn.flow.delete(flow_key)


@pytest.fixture(name="config")
def sdp_config_fixture():
    config = Config(backend="etcd3")
    clear_flows(config)
    yield config
    clear_flows(config)


@pytest.mark.asyncio
async def test_single_watch_create_and_update(config: Config):
    outputs = []

    async def run_watcher(c: Config):
        async with DbFlowWatcher(c, **PIPELINES[0].model_dump()) as watcher:
            async for config in watcher:
                outputs.append(config)

    task = asyncio.create_task(run_watcher(config))
    await asyncio.sleep(0.1)

    flow0a = create_test_flow(0, "msgpack_numpy")
    flow0b = create_test_flow(0, "npy")
    for txn in config.txn():
        txn.flow.create(flow0a)
        path1 = txn.flow.path(flow0a.key)
    for txn in config.txn():
        txn.flow.update(flow0b)

    with pytest.raises(asyncio.TimeoutError):
        await asyncio.wait_for(task, timeout=2)

    assert path1 == f"/flow/{PIPELINES[0].pb_id}:{PIPELINES[0].kind}:{PIPELINES[0].name}"

    assert outputs == [
        (PIPELINES[0].pb_id, [flow0a]),
        (PIPELINES[0].pb_id, [flow0b]),
    ]

    task.cancel()
    with suppress(asyncio.CancelledError):
        await task


@pytest.mark.asyncio
async def test_multi_watch_create_and_update(config: Config):
    outputs = []

    async def run_watcher(c: Config):
        async with DbFlowWatcher(c) as watcher:
            async for kv in watcher:
                outputs.append(kv)

    task = asyncio.create_task(run_watcher(config))
    await asyncio.sleep(1)

    flow0a = create_test_flow(0, "msgpack_numpy")
    flow1a = create_test_flow(1, "msgpack_numpy")
    for txn in config.txn():
        txn.flow.create(flow0a)
        txn.flow.create(flow1a)

    flow0b = create_test_flow(0, "npy")
    flow1b = create_test_flow(1, "npy")
    flow2b = create_test_flow(2, "npy")
    for txn in config.txn():
        txn.flow.update(flow0b)
        txn.flow.update(flow1b)
        txn.flow.create(flow2b)

    with pytest.raises(asyncio.TimeoutError):
        await asyncio.wait_for(task, timeout=2)

    expected = [
        (PIPELINES[0].pb_id, [flow0a]),
        (PIPELINES[0].pb_id, [flow0b]),
        (PIPELINES[1].pb_id, [flow1a]),
        (PIPELINES[1].pb_id, [flow1b]),
        (PIPELINES[2].pb_id, [flow2b]),
    ]

    # NOTE: no guarentee on the watch key order. Lists
    # and Flow models are not hashable but a single processing
    # block should receive results chronologically
    outputs.sort(key=lambda t: t[0])

    assert outputs == expected
    await asyncio.get_running_loop().shutdown_default_executor()


@pytest.mark.asyncio
async def test_single_watch_delete(config: Config):

    flow0a = create_test_flow(0, "msgpack_numpy")
    for txn in config.txn():
        txn.flow.create(flow0a)
    outputs = []

    async def run_watcher(c):
        async with DbFlowWatcher(c, **PIPELINES[0].model_dump()) as watcher:
            async for config in watcher:
                outputs.append(config)

    task = asyncio.create_task(run_watcher(config))
    await asyncio.sleep(0.1)

    for txn in config.txn():
        txn.flow.delete(flow0a.key)

    with pytest.raises(asyncio.TimeoutError):
        await asyncio.wait_for(task, timeout=1)

    assert outputs == [
        (PIPELINES[0].pb_id, [flow0a]),
        (PIPELINES[0].pb_id, []),
    ]
    await asyncio.get_running_loop().shutdown_default_executor()


@pytest.mark.asyncio
async def test_multi_watch_delete(config: Config):

    flow0a = create_test_flow(0, "msgpack_numpy")
    flow1a = create_test_flow(1, "msgpack_numpy")
    flow2a = create_test_flow(2, "msgpack_numpy")
    for txn in config.txn():
        txn.flow.create_or_update(flow0a)
        txn.flow.create_or_update(flow1a)
        txn.flow.create_or_update(flow2a)
    outputs = []

    async def run_watcher(c: Config):
        async with DbFlowWatcher(c) as watcher:
            async for kv in watcher:
                outputs.append(kv)

    task = asyncio.create_task(run_watcher(config))
    await asyncio.sleep(1)

    for txn in config.txn():
        txn.flow.delete(flow2a.key)
    for txn in config.txn():
        txn.flow.delete(flow1a.key)
        txn.flow.delete(flow0a.key)

    with pytest.raises(asyncio.TimeoutError):
        await asyncio.wait_for(task, timeout=3)

    expected = [
        (PIPELINES[1].pb_id, [flow1a]),
        (PIPELINES[0].pb_id, [flow0a]),
        (PIPELINES[2].pb_id, [flow2a]),
        (PIPELINES[2].pb_id, []),
        (PIPELINES[0].pb_id, []),
        (PIPELINES[1].pb_id, []),
    ]
    outputs.sort(key=lambda t: t[0])
    expected.sort(key=lambda t: t[0])
    assert outputs == expected

    # ensure threaded watcher is wrapped correctly by a coroutine
    await asyncio.get_running_loop().shutdown_default_executor()
